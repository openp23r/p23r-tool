
all:	dats
	go build -v
	mv p23r-tool p23r

mac:	all
	cp p23r distribution/macosx/dist

linux: all
	docker run --rm -v "$PWD":/usr/src/p23r -v "$GOPATH"/src/github.com:/usr/src/go/src/github.com -v "$GOPATH"/src/gitlab.com:/usr/src/go/src/gitlab.com -v "$GOPATH"/src/gopkg.in:/usr/src/go/src/gopkg.in -w /usr/src/p23r dregistry.fokus.fraunhofer.de:5000/p23r/go-compiler go build --ldflags '-extldflags "-static"' -v ...
	mv p23r distribution/linux

dist: mac linux

docker: Dockerfile
	docker build -tmy-golang:latest .

install: all
	sudo cp p23r /usr/local/bin

get:
	go get -u github.com/jteeuwen/go-pkg-xmlx
	go get -u github.com/jbussdieker/golibxml
	go get -u gopkg.in/yaml.v2
	go get -u github.com/odeke-em/go-uuid
	go get -u github.com/codegangsta/cli
	go get -u github.com/samalba/dockerclient
	go get -u github.com/jhoonb/archivex
	go get -u github.com/krolaw/xsd

include dats/Make.inc
