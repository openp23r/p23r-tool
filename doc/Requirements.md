# Requirements for the p23r tool

The p23r tool should support developer of notification rules, notification rule
groups and pivot data models.

## Goals

Therefor the following goals should be implemented:

* Check if notification rules, rule groups, pivot data models, tests and test sets are valid
* Create templates for notification rules, rule groups, pivot data models, tests and test sets
* Upload notification rules, rule groups, pivot data models, tests and test sets to a project control centra via git
* Sign notification rules, rule groups, pivot data models, tests and test sets to asure the integrity
* Publish notification rules, rule groups, pivot data models, tests and test sets to the public control centre
* Create documentation of notification rules, rule groups, pivot data models, tests and test sets as an epub book

## Checker tool

Before new or updated notification rules, rule groups, pivot data models, tests
and test sets are uploaded a p23r control centre they must be validated to
avoid complex failure searches. To ease the work of developers the failures
should be identified as early as possible. The p23r tool should check for the
following issues:

* Validate the manifest files using the corresponding XML schema definitions (XSD)
* Approve the file structure and check if all required files including the files,
  which are defined in the manifest files
* Check the valid syntax of each source file
* Rate the amount of text in the documentation fields

## Init tool

The init tool should provide all required and recommended files. The files should
contain an initial default text, which could be adapted by the developer on demand.
Therefor the following tasks should be implemented:

* Create an empty notification rule, rule group, pivot data model, test or test set
* Fill in default values from a local profile created by the developer
* Create a profile from the personal information to be entered by the developer
* The template must be valid and must be checkable by the checker tool
* The notification rule, rule group, pivot data model, test or test set should be
  complete and executable

## Deploy tool

The deploy tool will commit a notification rule, rule group, pivot data model,
test or test set to the branch *p23rdp*, so that the content will update all
related project control centre, which publishes that content. The content must
not be signed.

## Sign tool

The sign tool allows to create a hash for a notification rule, rule group, pivot
data model, test or test set and signing the hash with the registered private key.
This signature cold be used by the public control centre to validate the integrity
of the submitted content, before it will be published.

The hash will only include files, which are part of a official distribution and
not other working files, which are part of the development.

## Publish tool

The publish tool will commit a notification rule, rule group, pivot data model,
test or test set to the branch *p23rcc*, which is the input to publish content
in the public control centre. The content must be signed and the signature must
be valid. The public key for the signature must be registered at the public
control centre. The content will only be published when it is further part of the
official release list for the related release of the public control centre.

## Doc tool

The doc tool will generate an epub book for the notification rules, rule groups,
pivot data models, tests and test sets including all documentation parts of the
source code and additional documentation files.
