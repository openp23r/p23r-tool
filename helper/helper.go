package helper

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// CopyFile copies a file
func CopyFile(source string, dest string) (err error) {
	sourcefile, err := os.Open(source)
	if err != nil {
		return err
	}

	defer sourcefile.Close()

	destfile, err := os.Create(dest)
	if err != nil {
		return err
	}

	defer destfile.Close()

	_, err = io.Copy(destfile, sourcefile)
	if err == nil {
		sourceinfo, err := os.Stat(source)
		if err != nil {
			err = os.Chmod(dest, sourceinfo.Mode())
		}

	}

	return
}

// CopyDir copy a directory recursively
func CopyDir(source string, dest string) (err error) {
	// get properties of source dir
	sourceinfo, err := os.Stat(source)
	if err != nil {
		return err
	}

	// create dest dir

	err = os.MkdirAll(dest, sourceinfo.Mode())
	if err != nil {
		return err
	}

	directory, _ := os.Open(source)

	objects, err := directory.Readdir(-1)

	for _, obj := range objects {
		sourcefilepointer := filepath.Join(source, obj.Name())

		destinationfilepointer := filepath.Join(dest, obj.Name())
		// log.Println("file to copy:", obj.Name())
		if !strings.HasPrefix(obj.Name(), ".") {

			if obj.IsDir() {
				// create sub-directories - recursively
				err = CopyDir(sourcefilepointer, destinationfilepointer)
				if err != nil {
					fmt.Println(err)
				}
			} else {
				// perform copy

				err = CopyFile(sourcefilepointer, destinationfilepointer)
				if err != nil {
					fmt.Println(err)
				}
			}
		} else {
			// log.Println("INFO: obj starts with dot:", obj.Name())
		}

	}
	return
}

// CheckoutGit checkout a git in path
func CheckoutGit(gitToCheckout, path, branch string) {
	// docker build current directory
	cmdName := "git"
	cmdArgs := []string{"checkout", gitToCheckout, branch}

	cmd := exec.Command(cmdName, cmdArgs...)
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf("git checkout | %s\n", scanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		os.Exit(1)
	}

	err = cmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
		os.Exit(1)
	}

}
