// TODO: Prüfen, ob es eine neuere Version vom P23R-Tool gibt -> Version.txt
// TODO: Help verbessern
// TODO: Error messages should start with an uppercase letter
// TODO: Support for all formats (XML, YAML, JSON) everywhere
// TODO: Logging concept to be improved, especially regarding use cli context in logging class

package main

import (
	"C"
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"

	"github.com/codegangsta/cli"
	"gopkg.in/yaml.v2"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/protocol"
	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/model"
	"gitlab.com/openp23r/p23r-tool/packager"
	"gitlab.com/openp23r/p23r-tool/profile"
	"gitlab.com/openp23r/p23r-tool/rule"
	"gitlab.com/openp23r/p23r-tool/rulegroup"
	"gitlab.com/openp23r/p23r-tool/test"
	"gitlab.com/openp23r/p23r-tool/testPackage"
	"gitlab.com/openp23r/p23r-tool/testcase"
	"gitlab.com/openp23r/p23r-tool/testset"
)
import "gitlab.com/openp23r/p23r-tool/validate"

func buildDir(dir string, c *cli.Context) {
	if c.GlobalBool("verbose") {
		log.Info.Printf("Build directory %s", dir)
	}
	manifestType := manifest.GetManifestType(dir, c.Bool("verbose"))
	var dirs []string
	if manifestType != manifest.NoManifestID {
		dirs = []string{dir}
	} else {
		log.Warning.Printf("could not find manifest file (Manifest.xml) in '%s'\n", dir)
		xs, err := ioutil.ReadDir(dir)
		dirs = []string{}
		if err == nil {
			for _, x := range xs {
				if x.IsDir() {
					dirs = append(dirs, x.Name())
				}
			}
		}
	}
	for _, x := range dirs {
		if x == "." || (x[:1] != "." && x[:1] != "_") {
			manifestType = manifest.GetManifestType(x, c.Bool("verbose"))
			switch manifestType {
			case manifest.RuleManifestID:
				rule.Build(x, c.GlobalBool("verbose"))
			case manifest.RuleGroupManifestID:
				rulegroup.Build(x, c.GlobalBool("verbose"))
			case manifest.ModelManifestID:
				model.Build(x, c.GlobalBool("verbose"))
			case manifest.TestSetManifestID:
				testset.Build(x, c.GlobalBool("verbose"))
			case manifest.TestCaseManifestID:
				testcase.Build(x, c.GlobalBool("verbose"))
			case manifest.NoManifestID:
				buildDir(filepath.Join(dir, x), c)
			default:
				log.Warning.Printf("unknown kind of manifest in '%s'\n", x)
			}
		}
	}
}

func checkSubDir(dir string, c *cli.Context) int {
	if c.GlobalBool("verbose") {
		log.Info.Printf("checking directoy '%s'\n", dir)
	}
	foundErrorNb := 0
	manifestType := manifest.GetManifestType(dir, c.Bool("verbose"))
	var dirs []string
	if manifestType != manifest.NoManifestID {
		dirs = []string{dir}
	} else {
		// log.Warning.Printf("could not find manifest file (Manifest.xml) in '%s'\n", dir)
		xs, err := ioutil.ReadDir(dir)
		dirs = []string{}
		if err == nil {
			for _, x := range xs {
				if x.IsDir() {
					dirs = append(dirs, filepath.Join(dir, x.Name()))
				}
			}
		} else {
			log.Error.Printf("Could not read directory %s: %s\n", dir, err.Error())
		}
	}
	for _, x := range dirs {
		if x == "." || (x[:1] != "." && x[:1] != "_") {
			manifestType = manifest.GetManifestType(x, c.Bool("verbose"))
			switch manifestType {
			case manifest.ModelManifestID:
				foundErrorNb += model.Validate(x, c.GlobalBool("verbose"))
			case manifest.RuleManifestID:
				foundErrorNb += rule.Validate(x, c.GlobalBool("verbose"))
			case manifest.RuleGroupManifestID:
				foundErrorNb += rulegroup.Validate(x, c.GlobalBool("verbose"))
			case manifest.TestCaseManifestID:
				foundErrorNb += testcase.Validate(x, c.GlobalBool("verbose"))
			case manifest.TestSetManifestID:
				foundErrorNb += testset.Validate(x, c.GlobalBool("verbose"))
			case manifest.NoManifestID:
				foundErrorNb += checkSubDir(x, c)
			default:
				log.Warning.Printf("unknown kind of manifest in '%s'\n", x)
			}
		}
	}
	return foundErrorNb
}

func checkDir(dir string, c *cli.Context) {
	foundErrorNb := checkSubDir(dir, c)
	if foundErrorNb == 1 {
		log.Error.Fatal("Found 1 error")
	} else if foundErrorNb > 0 {
		log.Error.Fatalf("Found %d errors", foundErrorNb)
	}
}

func main() {
	app := cli.NewApp()
	app.Name = "p23r"
	app.Usage = "tools to support p23r rule developers"
	app.Version = "0.6"
	app.Authors = []cli.Author{
		cli.Author{
			Name:  "Jan Gottschick",
			Email: "jan.gottschick@fokus.fraunhofer.de",
		},
		cli.Author{
			Name:  "Wilhelm Vortisch",
			Email: "wilhelm.vortisch@fokus.fraunhofer.de",
		},
	}

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "verbose, V",
			Usage: "show verbose logging",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:  "build",
			Usage: "build compiles all files from the sources directories to their target files. To build XSD schema files the modelc compiler must be pressent! (npm install p23r-model-compiler)",
			Action: func(c *cli.Context) {
				if len(c.Args()) == 0 {
					buildDir(".", c)
				} else {
					for _, name := range c.Args() {
						buildDir(name, c)
					}
				}
			},
		},
		{
			Name:  "check",
			Usage: "check if the content of the rules, ... are valid and complete",
			Action: func(c *cli.Context) {
				if len(c.Args()) == 0 {
					checkDir(".", c)
				} else {
					for _, name := range c.Args() {
						checkDir(name, c)
					}
				}
			},
		},
		{
			Name:  "package",
			Usage: "create a project depot",
			Subcommands: []cli.Command{
				{
					Name:  "defaults",
					Usage: "Enter default values for packaging process",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "global, g",
							Usage: "set defaults globally",
						},
					},
					Action: func(c *cli.Context) {
						println("Set default values ")
						println()
						if err := profile.UpdateValues(c.Bool("global")); err != nil {
							log.Error.Println("Could not store updated profile file")
						}
						println()
					},
				},
				{
					Name:  "local",
					Usage: "package local artifacts",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "no-delete, l",
							Usage: "Do not delete the temporary content",
						},
						cli.StringFlag{
							Name:  "git-path, g",
							Value: "",
							Usage: "search the gits in path <path> (default: current working path)",
						},
						cli.StringFlag{
							Name:  "package-name, n",
							Value: "Package",
							Usage: "set the package name to <parameter> (default: 'Package')",
						},
						cli.StringFlag{
							Name:  "email-address, e",
							Value: "developer@p23r.de",
							Usage: "set the email in packaging process to <parameter> (default: 'developer@p23r.de')",
						},
						cli.BoolFlag{
							Name:  "localhost, L",
							Usage: "set the depot host in packaging process to the name of the localhost",
						},
						cli.StringFlag{
							Name:  "depot-host, d",
							Usage: "set the depot host in packaging process to <parameter> (no default!')",
						},
						cli.BoolFlag{
							Name:  "http",
							Usage: "start http server (default port: '80')",
						},
						cli.IntFlag{
							Name:  "port",
							Value: 80,
							Usage: "port for starting http server (ignored if --http is not set)",
						},
					},
					Action: func(c *cli.Context) {
						packager.InterpretCli(c)
					},
				},
				{
					Name:  "docker",
					Usage: "package local artifacts and start docker container in machine p23r-tool",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "no-delete, l",
							Usage: "Do not delete the temporary content",
						},
						cli.StringFlag{
							Name:  "registry, r",
							Value: "",
							Usage: "registry in which the docker build is pushed",
						},
						cli.BoolFlag{
							Name:  "insecure",
							Usage: "use insecure registry",
						},
						cli.StringFlag{
							Name:  "git-path, g",
							Value: "",
							Usage: "search the gits in path <path> (default: current working path)",
						},
						cli.StringFlag{
							Name:  "package-name, n",
							Value: "Package",
							Usage: "set the package name to <parameter> (default: 'Package')",
						},
						cli.StringFlag{
							Name:  "email-address, e",
							Value: "developer@p23r.de",
							Usage: "set the email in packaging process to <parameter> (default: 'developer@p23r.de')",
						},
						cli.StringFlag{
							Name:  "depot-host, d",
							Usage: "set the depot host in packaging process to <parameter> (no default!)",
						},
						cli.StringFlag{
							Name:  "depot-user, u",
							Usage: "set the depot host in packaging process to <parameter> (no default!)",
						},
						cli.StringFlag{
							Name:  "depot-password, p",
							Usage: "set the depot host in packaging process to <parameter> (no default!)",
						},
					},
					Action: func(c *cli.Context) {
						packager.InterpretCli(c)
					},
				},
			},
		},
		{
			Name:  "create",
			Usage: "create default templates for rules, ... in p23r-profile.yaml or globally in ~/.p23r-profile.yaml",
			Subcommands: []cli.Command{
				{
					Name:  "defaults",
					Usage: "Enter default values for the templates",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "global, g",
							Usage: "set defaults globally",
						},
					},
					Action: func(c *cli.Context) {
						println("Set default values ")
						println()
						if err := profile.UpdateValues(c.Bool("global")); err != nil {
							log.Error.Println("Could not store updated profile file")
						}
						println()
					},
				},
				{
					Name:  "model",
					Usage: "Create an empty model from templates",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "subproject, s",
							Usage: "Don't create model as subproject for a multi project repository",
						},
					},
					Action: func(c *cli.Context) {
						for _, name := range c.Args() {
							if matched, _ := regexp.MatchString("^[a-zA-Z][a-zA-Z0-9]*$", name); !matched {
								log.Error.Fatalf("wrong model name for template: %v -> [a-zA-Z][a-zA-Z0-9]*", name)
							}
							if c.GlobalBool("verbose") {
								print("Creating model ", name, " : ")
							}
							model.GenerateTemplate(name, !c.Bool("subproject"), !c.Bool("sources"))
						}
					},
				},
				{
					Name:  "rule",
					Usage: "Create an empty notification rule from templates",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "sources, S",
							Usage: "Don't create specific templates in the source directory",
						},
					},
					Action: func(c *cli.Context) {
						for _, name := range c.Args() {
							if matched, _ := regexp.MatchString("^[a-zA-Z][a-zA-Z0-9]*$", name); !matched {
								log.Error.Fatalf("wrong rule name for template: %v -> [a-zA-Z][a-zA-Z0-9]*", name)
							}
							if c.GlobalBool("verbose") {
								print("Creating rule ", name, " : ")
							}
							rule.GenerateTemplate(name, !c.Bool("sources"))
						}
					},
				},
				{
					Name:  "rulegroup",
					Usage: "Create an empty notification rule group from templates",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "subproject, s",
							Usage: "Don't create rule group as subproject for a multi project repository",
						},
						cli.BoolFlag{
							Name:  "sources, S",
							Usage: "Don't create specific templates in the source directory",
						},
					},
					Action: func(c *cli.Context) {
						for _, name := range c.Args() {
							if matched, _ := regexp.MatchString("^[a-zA-Z][a-zA-Z0-9]*$", name); !matched {
								log.Error.Fatalf("wrong rule group name for template: %v -> [a-zA-Z][a-zA-Z0-9]*", name)
							}
							if c.GlobalBool("verbose") {
								print("Creating rule group ", name, " : ")
							}
							rulegroup.GenerateTemplate(name, !c.Bool("subproject"), !c.Bool("sources"))
						}
					},
				},
				{
					Name:    "test",
					Aliases: []string{"testcase"},
					Usage:   "Create an empty test case from templates",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "sources, S",
							Usage: "Don't create specific templates in the source directory",
						},
						cli.StringFlag{
							Name:  "rule",
							Usage: "Name of the rule to test",
						},
						cli.StringFlag{
							Name:  "rulegroup",
							Usage: "Name of the rule group to test",
						},
					},
					Action: func(c *cli.Context) {
						for _, name := range c.Args() {
							if matched, _ := regexp.MatchString("^[a-zA-Z][a-zA-Z0-9]*$", name); !matched {
								log.Error.Fatalf("wrong test case name for template: %v -> [a-zA-Z][a-zA-Z0-9]*", name)
							}
							if c.GlobalBool("verbose") {
								print("Creating test case ", name, " : ")
							}
							testcase.GenerateTemplate(name, c, !c.Bool("sources"))
						}
					},
				},
				{
					Name:  "testset",
					Usage: "Create an empty test set from templates",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "subproject, s",
							Usage: "Don't create test set as subproject for a multi project repository",
						},
						cli.BoolFlag{
							Name:  "sources, S",
							Usage: "Don't create specific templates in the source directory",
						},
					},
					Action: func(c *cli.Context) {
						for _, name := range c.Args() {
							if matched, _ := regexp.MatchString("^[a-zA-Z][a-zA-Z0-9]*$", name); !matched {
								log.Error.Fatalf("wrong test set name for template: %v -> [a-zA-Z][a-zA-Z0-9]*", name)
							}
							if c.GlobalBool("verbose") {
								print("Creating test set ", name, " : ")
							}
							testset.GenerateTemplate(name, !c.Bool("subproject"), !c.Bool("sources"))
						}
					},
				},
			},
		},
		// {
		// 	Name:  "publish",
		// 	Usage: "publish a rule group, test set or model to a local or project specific P23R depot via GIT",
		// 	Action: func(c *cli.Context) {
		// 		if len(c.Args()) == 0 {
		// 			buildDir(".", c)
		// 		} else {
		// 			for _, name := range c.Args() {
		// 				buildDir(name, c)
		// 			}
		// 		}
		// 	},
		// },
		// {
		// 	Name:  "release",
		// 	Usage: "release provides a rule group, test set or model to be publish on a public P23R depot",
		// 	Action: func(c *cli.Context) {
		// 		if len(c.Args()) == 0 {
		// 			buildDir(".", c)
		// 		} else {
		// 			for _, name := range c.Args() {
		// 				buildDir(name, c)
		// 			}
		// 		}
		// 	},
		// },
		{
			Name:  "test",
			Usage: "execute test cases part of the project depot",
			Subcommands: []cli.Command{
				{
					Name:  "run",
					Usage: "Run one or more test sets with the packages in the configured project depot",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "server, s",
							Usage: "URL of the server which will run the tests",
							Value: "https://localhost/processor/test",
						},
						cli.StringFlag{
							Name:  "depot-host, d",
							Usage: "URL of the server which will provide the depot",
							Value: "http://localhost:8980/",
						},
					},
					Action: func(c *cli.Context) {
						ids := []string{}
						if len(c.Args()) == 0 {
							ids = testPackage.GetTestSetIDs(".", c.Bool("verbose"))
						} else {
							for _, name := range c.Args() {
								ids = append(ids, testPackage.GetTestSetIDs(name, c.Bool("verbose"))...)
							}
						}
						test.Run(ids, c)
					},
				},
				{
					Name:  "view",
					Usage: "View the result from the last test run as xml output",
					Flags: []cli.Flag{
						cli.BoolFlag{
							Name:  "http",
							Usage: "Start a local http server to visualize the current result ('_test/testlog.xml')",
						},
						cli.IntFlag{
							Name:  "port",
							Usage: "The port number of the local http server",
							Value: 7777,
						},
						cli.BoolFlag{
							Name:  "json",
							Usage: "The output will shown in the json format",
						},
						cli.BoolFlag{
							Name:  "yaml",
							Usage: "The output will shown in the yaml format",
						},
					},
					Action: func(c *cli.Context) {
						// start local http server
						if c.Bool("http") {
							if len(c.Args()) == 0 {
								test.HTTPService(".", c)
							} else if len(c.Args()) == 1 {
								for _, name := range c.Args() {
									test.HTTPService(name, c)
								}
							} else {
								log.Error.Fatalln("Too many arguments")
							}
						}

						// Read test result file
						data, err := ioutil.ReadFile(filepath.Join("_test", "testlog.xml"))
						if err != nil {
							log.Error.Fatalln("Could not read test result file", filepath.Join("_test", "testlog.xml"))
						}

						content := protocol.ParseXML(data)
						// TODO output in json format
						if c.Bool("json") {
							out, err := json.MarshalIndent(content, "", "  ")
							if err != nil {
								log.Error.Fatalf("Could not convert result to JSON\n")
							}
							println(string(out))
							return
						}

						// TODO output in yaml format
						if c.Bool("yaml") {
							out, err := yaml.Marshal(content)
							if err != nil {
								log.Error.Fatalf("Could not convert result to YAML\n")
							}
							println(string(out))
							return
						}

						// output in xml format
						out, err := xml.MarshalIndent(content, "", "  ")
						if err != nil {
							log.Error.Fatalf("Could not convert result to XML\n")
						}
						println(xml.Header + string(out))
					},
				},
				{
					Name:  "deliver",
					Usage: "Deliver only the message of a test case(s) or a given message(s) file to the p23r processor to generate a notification(s)",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "server",
							Usage: "Deliver the message to the p23r processor with the given URL",
							Value: "http://localhost:8080",
						},
						cli.StringFlag{
							Name:  "tenant",
							Usage: "Id of the tenant to be used",
							Value: "default",
						},
						cli.StringFlag{
							Name:  "context",
							Usage: "In which context the message will be delivered: private, public, scheduler, test, internal",
							Value: "public",
						},
					},
					Action: func(c *cli.Context) {
						if len(c.Args()) == 0 {
							test.Deliver(".", c)
						} else {
							for _, name := range c.Args() {
								test.Deliver(name, c)
							}
						}
					},
				},
			},
		},
		{
			Name:  "validate",
			Usage: "validate file formats",
			Subcommands: []cli.Command{
				{
					Name:  "notification",
					Usage: "validate a notification send over the transferNotification interface",
					Action: func(c *cli.Context) {
						if len(c.Args()) > 0 {
							for _, name := range c.Args() {
								validate.NotificationTransfer(name, c)
							}
						}
					},
				},
			},
		},
	}

	app.Run(os.Args)
}
