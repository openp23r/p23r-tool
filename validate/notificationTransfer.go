package validate

import (
	"io/ioutil"

	"github.com/codegangsta/cli"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/notificationTransfer"
)

// NotificationTransfer validation
func NotificationTransfer(filename string, c *cli.Context) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Error.Printf("Could not read file %s: %s", filename, err.Error())
	}
	dataJSON, err := notificationTransfer.ParseJSON(string(data))
	if err != nil {
		log.Error.Printf("Could not parse notification: %s", err.Error())
	} else {
		log.Info.Printf("Parsed: %#v", dataJSON)
	}
}
