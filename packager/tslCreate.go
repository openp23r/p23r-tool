package packager

import (
	"os"
	"path/filepath"
	"text/template"
	"time"

	"github.com/odeke-em/go-uuid"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/profile"
)

// GenerateTsl generate a local p23r.tsl
func GenerateTsl(prof *profile.Profile, dir string) (err error) {
	// .DepotHost = localhost:8011 e.g.
	// .P23R = https://leitstelle.p23r.de

	type Mapping struct {
		UUID, DepotHost, P23R, ActualDate string
	}
	var mapping *Mapping
	mapping = &Mapping{
		UUID:       uuid.New(),
		DepotHost:  prof.DepotHost,
		P23R:       "https://leitstelle.p23r.de",
		ActualDate: time.Now().Format(time.RFC3339),
	}

	t := template.Must(template.New("file").Parse(tsl))
	var f *os.File
	f, err = os.Create(filepath.Join(dir, "p23r.tsl"))
	defer f.Close()
	if err != nil {
		log.Error.Fatal(err)
	}
	err = t.Execute(f, mapping)
	if err != nil {
		log.Error.Fatal(err)
	}
	// generate package.lst

	return err

}

const tsl = `<?xml version="1.0" encoding="UTF-8"?>
<TrustServiceStatusList xmlns="http://uri.etsi.org/02231/v2#"
    xmlns:ns2="http://www.w3.org/2000/09/xmldsig#"
    xmlns:ns3="http://uri.etsi.org/TrstSvc/SvcInfoExt/eSigDir-1999-93-EC-TrustedList/#"
    xmlns:ns4="http://uri.etsi.org/01903/v1.3.2#"
    xmlns:ns5="http://uri.etsi.org/02231/v2/additionaltypes#"
    Id="{{.UUID}}" TSLTag="http://uri.etsi.org/02231/TSLTag">
    <SchemeInformation>
        <TSLVersionIdentifier>3</TSLVersionIdentifier>
        <TSLSequenceNumber>1</TSLSequenceNumber>
        <TSLType>http://uri.p23r.de/TrstSvc/TSLType/generic</TSLType>
        <SchemeOperatorName>
            <Name xml:lang="de">CN=P23R Control Centre, OU=Control Centre, O=P23R Consortium,
                C=DE</Name>
        </SchemeOperatorName>
        <SchemeOperatorAddress>
            <PostalAddresses>
                <PostalAddress xml:lang="de">
                    <StreetAddress>Kaiserin-Augusta-Allee 31</StreetAddress>
                    <Locality>Berlin</Locality>
                    <StateOrProvince>Berlin</StateOrProvince>
                    <PostalCode>10589</PostalCode>
                    <CountryName>DE</CountryName>
                </PostalAddress>
            </PostalAddresses>
            <ElectronicAddress>
                <URI>http://leitstelle.p23r.de</URI>
            </ElectronicAddress>
        </SchemeOperatorAddress>
        <SchemeName>
            <Name xml:lang="de">CTRLCTR-External</Name>
        </SchemeName>
        <SchemeInformationURI>
            <URI xml:lang="de">http://www.p23r.de/specification/SecurityInfrastructure.pdf</URI>
        </SchemeInformationURI>
        <StatusDeterminationApproach>http://uri.etsi.org/TrstSvc/TSLType/StatusDetn/passive</StatusDeterminationApproach>
        <SchemeTypeCommunityRules>
            <URI>http://leitstelle.p23r.de</URI>
        </SchemeTypeCommunityRules>
        <SchemeTerritory>DE</SchemeTerritory>
        <HistoricalInformationPeriod>0</HistoricalInformationPeriod>
        <ListIssueDateTime>{{.ActualDate}}</ListIssueDateTime>
        <NextUpdate />
        <DistributionPoints>
            <URI>{{.DepotHost}}/p23r.tsl</URI>
        </DistributionPoints>
    </SchemeInformation>
    <TrustServiceProviderList>
        <TrustServiceProvider>
            <TSPInformation>
                <TSPName>
                    <Name xml:lang="en">CTRLCTR-External</Name>
                </TSPName>
                <TSPTradeName>
                    <Name xml:lang="en">CTRLCTR-External</Name>
                </TSPTradeName>
                <TSPAddress>
                    <PostalAddresses>
                        <PostalAddress xml:lang="de">
                            <StreetAddress>Kaiserin-Augusta-Allee 31</StreetAddress>
                            <Locality>Berlin</Locality>
                            <StateOrProvince>Berlin</StateOrProvince>
                            <PostalCode>10589</PostalCode>
                            <CountryName>Deutschland</CountryName>
                        </PostalAddress>
                    </PostalAddresses>
                    <ElectronicAddress>
                        <URI>http://leitstelle.p23r.de</URI>
                    </ElectronicAddress>
                </TSPAddress>
                <TSPInformationURI>
                    <URI xml:lang="de">http://www.p23r.de</URI>
                </TSPInformationURI>
            </TSPInformation>
            <TSPServices>
                <TSPService>
                    <ServiceInformation>
                        <ServiceTypeIdentifier>http://www.p23r.de/Svc/Svctype/REST</ServiceTypeIdentifier>
                        <ServiceName>
                            <Name xml:lang="de">ModelAndRuleDepot</Name>
                        </ServiceName>
                        <ServiceStatus>http://uri.etsi.org/TrstSvc/Svcstatus/inaccord</ServiceStatus>
                        <StatusStartingTime>{{.ActualDate}}</StatusStartingTime>
                        <ServiceSupplyPoints>
                            <ServiceSupplyPoint>{{.DepotHost}}</ServiceSupplyPoint>
                        </ServiceSupplyPoints>
                    </ServiceInformation>
                </TSPService>
                <TSPService>
                    <ServiceInformation>
                        <ServiceTypeIdentifier>http://www.p23r.de/Svc/Svctype/SOAP</ServiceTypeIdentifier>
                        <ServiceName>
                            <Name xml:lang="de">NotificationReceiverQuery</Name>
                        </ServiceName>
                        <ServiceStatus>http://uri.etsi.org/TrstSvc/Svcstatus/inaccord</ServiceStatus>
                        <StatusStartingTime>{{.ActualDate}}</StatusStartingTime>
                        <ServiceSupplyPoints>
                            <ServiceSupplyPoint>{{.P23R}}/inrquery/services/INotificationReceiverQueryService</ServiceSupplyPoint>
                        </ServiceSupplyPoints>
                    </ServiceInformation>
                </TSPService>
                <TSPService>
                    <ServiceInformation>
                        <ServiceTypeIdentifier>http://www.p23r.de/Svc/Svctype/SOAP</ServiceTypeIdentifier>
                        <ServiceName>
                            <Name xml:lang="de">AddressQuery</Name>
                        </ServiceName>
                        <ServiceStatus>http://uri.etsi.org/TrstSvc/Svcstatus/inaccord</ServiceStatus>
                        <StatusStartingTime>{{.ActualDate}}</StatusStartingTime>
                        <ServiceSupplyPoints>
                            <ServiceSupplyPoint>{{.P23R}}/iadquery/services/IAddressQueryService</ServiceSupplyPoint>
                        </ServiceSupplyPoints>
                    </ServiceInformation>
                </TSPService>
            </TSPServices>
        </TrustServiceProvider>
    </TrustServiceProviderList>
</TrustServiceStatusList>
`
