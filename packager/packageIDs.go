package packager

import (
	"io/ioutil"
	"path/filepath"

	"github.com/jteeuwen/go-pkg-xmlx"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/manifest"
)

func getPackages(dir string, verbose bool) []string {
	packagePaths := []string{}
	manifestType := manifest.GetManifestType(dir, verbose)
	var dirs []string
	if manifestType != manifest.NoManifestID {
		dirs = []string{dir}

	} else {
		xs, err := ioutil.ReadDir(dir)
		dirs = []string{}
		if err == nil {
			for _, x := range xs {
				if x.IsDir() {
					dirs = append(dirs, x.Name())

				}
			}
		}
	}
	for _, x := range dirs {
		manifestType = manifest.GetManifestType(x, verbose)

		switch manifestType {
		case manifest.ModelPackageManifestID:
			packagePaths = append(packagePaths, x)
		case manifest.RulePackageManifestID:
			packagePaths = append(packagePaths, x)
		case manifest.TestPackageManifestID:
			packagePaths = append(packagePaths, x)
		default:
			// if strings.HasPrefix(x, "/") {
			// packagePaths = append(packagePaths, getPackages(x)...)
			// } else {
			packagePaths = append(packagePaths, getPackages(filepath.Join(dir, x), verbose)...)
			// }

		}

	}

	return packagePaths
}

func getIDFromPackages(dir string) string {
	data, err := ioutil.ReadFile(filepath.Join(dir, "Manifest.xml"))
	if err != nil {
		// log.Warning.Println("Could not read Manifest file", filepath.Join(dir, "Manifest.xml"))
	}
	doc := xmlx.New()
	err = doc.LoadString(string(data), nil)
	if err != nil {
		// log.Warning.Println("Could not parse Manifest file", filepath.Join(dir, "Manifest.xml"))
	}

	node := doc.SelectNode("*", "rulePackageManifest")
	if node != nil {
		return node.As("", "id")
	}
	node = doc.SelectNode("*", "modelPackageManifest")
	if node != nil {
		return node.As("", "id")
	}
	node = doc.SelectNode("*", "testPackageManifest")
	if node != nil {
		return node.As("", "id")
	}

	return ""
}

// GetPackageIDs collect all ids of the avalaible rule groups in a notification rule package
func GetPackageIDs(dir string, verbose bool) []string {

	dirs := getPackages(dir, verbose)
	ids := []string{}
	for _, x := range dirs {
		id := getIDFromPackages(x)
		ids = append(ids, id)
	}
	log.Info.Println("ids:", ids)

	return ids
}
