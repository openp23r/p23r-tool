package packager

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"
	"time"

	"github.com/codegangsta/cli"
	"gitlab.com/openp23r/p23r-common/log"

	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/model"
	"gitlab.com/openp23r/p23r-tool/modelPackage"
	"gitlab.com/openp23r/p23r-tool/profile"
	"gitlab.com/openp23r/p23r-tool/rulePackage"
	"gitlab.com/openp23r/p23r-tool/rulegroup"
	"gitlab.com/openp23r/p23r-tool/testPackage"
	"gitlab.com/openp23r/p23r-tool/testset"
)

// InterpretCli interprets cli flags for command package local
func InterpretCli(c *cli.Context) {
	// work on flag path
	dir := c.String("git-path")
	if len(c.String("git-path")) == 0 {
		dir, _ = os.Getwd()
	}
	// is dir absolute?
	if !path.IsAbs(dir) {
		temp, _ := os.Getwd()
		dir = path.Join(path.Dir(temp), dir)
	}

	var prof *profile.Profile
	var err error
	prof, err = profile.ReadProfile()
	if err != nil {
		log.Error.Fatalln(err)
	}
	log.Info.Println("----> dir:", dir)

	var dirsWithManifest []string
	dirsWithManifest = searchSubDirs(dir, c, dirsWithManifest, c.Bool("verbose"))
	log.Info.Println("dirsWithManifest now: ->", dirsWithManifest)

	// Parameter handling: package name
	if c.IsSet("package-name") {
		prof.PackageName = c.String("package-name")
	} else {
		// Test if profile has the right values
		if len(prof.PackageName) == 0 {
			log.Error.Fatalln("package name must be set over command line (-pn) or in defaults.")
		}
	}

	// Parameter handling: depot host

	if c.IsSet("localhost") {
		hostname, _ := os.Hostname()
		if hostname != "" {
			prof.DepotHost = "http://" + hostname
		}
	} else if c.IsSet("depot-host") {
		prof.DepotHost = c.String("depot-host")
	}
	// Test if profile has the right values
	if len(prof.DepotHost) == 0 {
		log.Error.Fatalln("depot host must be set over command line (-d) or in defaults.")
	}

	// Parameter handling: email
	if c.IsSet("email-address") {
		prof.Email = c.String("email-address")
	} else {
		// Test if profile has the right values
		if len(prof.Email) == 0 {
			log.Error.Fatal("email address must be set over command line (-e) or in defaults.")
		}
	}
	ruleGroupIds, packageIds := createLocalPackage(dir, dirsWithManifest, prof, c.Bool("no-delete"), c.GlobalBool("verbose"))

	if c.Command.HasName("local") {
		if c.IsSet("http") {
			// start web server
			fs := http.FileServer(http.Dir(filepath.Join(dir, "_packages")))
			log.Info.Println("start http server on port", c.Int("port"))
			// http.HandleFunc("/", handler)
			http.Handle("/", fs)
			err := http.ListenAndServe(":"+c.String("port"), nil)
			if err != nil {
				log.Error.Fatalf("HTTP server exit with error: %s", err.Error())
			}

		}
	}
	if c.Command.HasName("docker") {
		insecure := c.IsSet("insecure")
		if insecure {
			log.Info.Println("")
			log.Info.Println("Using insecure registry asumes that you start a new docker machine because insecurity of a docker registy must be set in start")
			log.Info.Println("")
		}
		if c.IsSet("registry") {
			prof.Registry = c.String("registry")
		} else {
			// Test if profile has the right values
			if len(prof.Registry) == 0 {
				log.Error.Fatal("registry must be set over command line (-e) or in defaults.")
			}
		}
		if c.IsSet("depot-user") {
			prof.DepotUser = c.String("depot-user")
		} else {
			if len(prof.DepotUser) == 0 {
				log.Error.Fatal("depot user must be set over command line (-e) or in defaults.")
			}
		}
		if c.IsSet("depot-password") {
			prof.DepotPassword = c.String("depot-password")
		} else {
			if len(prof.DepotPassword) == 0 {
				log.Error.Fatal("depot password must be set over command line (-e) or in defaults.")
			}
		}
		CreateVirtualMachine(prof, dir, "VirtualBox", insecure, ruleGroupIds, packageIds)
	}

}

// CreateLocalPackage creates a fully distributable package
func createLocalPackage(originalPathName string, dirsWithManifest []string, prof *profile.Profile, noDelete, verbose bool) ([]string, []string) {
	log.Info.Println("start packaging for directory:", dirsWithManifest)
	var packageBuilded []int
	var tempDir = ""

	ruleGroupIDs := []string{}
	packageIDs := []string{}

	for _, dir := range dirsWithManifest {

		if verbose {
			log.Info.Println("search for git:", dir)
		}

		te, packageType, err := createPackages(prof, dir, originalPathName, tempDir, noDelete, verbose)
		if err != nil {
			log.Error.Println(err)
		} else {
			packageBuilded = append(packageBuilded, packageType)
			tempDir = te
		}

	}
	if len(packageBuilded) > 0 {
		if err := generateRootArtefacts(tempDir, prof); err != nil {
			log.Error.Fatalln(err)
		}

		log.Info.Println("tempDir:", tempDir)

		// Generate leitstelle
		GeneratePackageList(prof, tempDir, "1.0", packageBuilded, noDelete, verbose)
		GenerateTsl(prof, tempDir)

		// get IDs
		ruleGroupIDs = rulegroup.GetRuleGroupIDs(filepath.Join(tempDir, "nrp"), verbose)
		packageIDs = GetPackageIDs(tempDir, verbose)

		// delete temporary directories
		if !noDelete {
			for _, x := range packageBuilded {
				toDeletePath := ""
				switch x {
				case manifest.RuleGroupManifestID:
					toDeletePath = filepath.Join(tempDir, "nrp")
					break
				case manifest.ModelManifestID:
					toDeletePath = filepath.Join(tempDir, "dmp")
					break
				case manifest.TestSetManifestID:
					toDeletePath = filepath.Join(tempDir, "tp")
					break

				}
				if len(toDeletePath) > 0 {
					log.Info.Println("delete temporary directory", toDeletePath)

					if err := os.RemoveAll(toDeletePath); err != nil {
						log.Warning.Println("in delete temporary directory", toDeletePath, ":", err)
					}
				}

			}

		}

	}
	return ruleGroupIDs, packageIDs
}

func searchSubDirs(dir string, c *cli.Context, xDirs []string, verbose bool) []string {
	if c.GlobalBool("verbose") {
		log.Info.Println("checking directoy", dir)
	}
	manifestType := manifest.GetManifestType(dir, verbose)
	var dirs []string
	if manifestType != manifest.NoManifestID {
		dirs = []string{dir}
	} else {
		// log.Warning.Printf("could not find manifest file (Manifest.xml) in '%s'\n", dir)
		xs, err := ioutil.ReadDir(dir)
		dirs = []string{}
		if err == nil {
			for _, x := range xs {
				if x.IsDir() && !strings.HasPrefix(x.Name(), ".") {
					// manifestType := manifest.GetManifestType( filepath.Join(dir, x.Name())
					// if manifestType != manifest.NoManifestID {
					dirs = append(dirs, x.Name())
					//}
				}
			}
		}
	}
	if c.GlobalBool("verbose") {
		log.Info.Println("now the dirs looks like:", dirs)
	}

	for _, x := range dirs {
		manifestType = manifest.GetManifestType(filepath.Join(dir, x), verbose)
		switch manifestType {
		case manifest.ModelManifestID:
			xDirs = append(xDirs, filepath.Join(dir, x))
		case manifest.RuleManifestID:
			xDirs = append(xDirs, filepath.Join(dir, x))
		case manifest.RuleGroupManifestID:
			xDirs = append(xDirs, filepath.Join(dir, x))
		case manifest.TestCaseManifestID:
			xDirs = append(xDirs, filepath.Join(dir, x))
		case manifest.TestSetManifestID:
			xDirs = append(xDirs, filepath.Join(dir, x))
		case manifest.NoManifestID:
			xDirs = searchSubDirs(filepath.Join(dir, x), c, xDirs, verbose)
		default:
			log.Warning.Printf("unknown kind of manifest in '%s'\n", filepath.Join(dir, x))
		}

	}

	return xDirs
}

func createPackages(prof *profile.Profile, pathName, generatePath, tempDir string, noDelete, verbose bool) (string, int, error) {
	var manifestType = manifest.GetManifestType(pathName, verbose)
	switch manifestType {
	case manifest.ModelManifestID:
		log.Info.Println("ModelManifest found")
		if errorCount := model.Validate(pathName, verbose); errorCount > 0 {
			os.Exit(1)
		} else {
			log.Info.Println("all ok with model ...")
			// generate directory structure
			tempDir = createTempDir(tempDir, generatePath)

			// generate model package
			modelPackage.GeneratePackage(prof, pathName, tempDir, "1.0", noDelete, verbose)
		}
	case manifest.RuleManifestID:
		log.Info.Println("RuleManifest found")
		return "", -1, fmt.Errorf("ERROR: RuleManifest can not be on root of '%s'", pathName)
	case manifest.RuleGroupManifestID:
		log.Info.Println("RuleGroupManifest found")
		if errorCount := rulegroup.Validate(pathName, verbose); errorCount > 0 {
			os.Exit(1)
		} else {
			log.Info.Println("all ok with rulegroup ...")
			// generate directory structure
			tempDir = createTempDir(tempDir, generatePath)

			// generate rule package
			rulePackage.GeneratePackage(prof, pathName, tempDir, "1.0", noDelete, verbose)
		}

	case manifest.TestCaseManifestID:
		log.Info.Println("TestCaseManifest found")
		return "", -1, fmt.Errorf("ERROR: TestCaseManifest can not be on root of '%s'", pathName)
	case manifest.TestSetManifestID:
		log.Info.Println("TestSetManifest found")

		if errorCount := testset.Validate(pathName, verbose); errorCount > 0 {
			os.Exit(1)
		} else {
			log.Info.Println("all ok with testset ...")
			// generate directory structure
			// tDir := createTempDir(tempDir)
			tempDir = createTempDir(tempDir, generatePath)

			// generate test package
			testPackage.GeneratePackage(prof, pathName, tempDir, "1.0", noDelete, verbose)
		}

	case manifest.NoManifestID:
		return "", -1, fmt.Errorf("WARNING: could not find manifest file (Manifest.xml) in '%s'", pathName)

	default:
		// println("WARNING: unknown kind of manifest in '", manifestPath, "'")
		return "", -1, fmt.Errorf("WARNING: unknown kind of manifest in '%s'", pathName)

	}

	return tempDir, manifestType, nil
}

func generateRootArtefacts(dir string, prof *profile.Profile) (err error) {
	// generate package.cat
	type Mapping struct {
		Publisher, Creator, DepotHost, ReleasedAt, Release string
	}
	var mapping *Mapping
	mapping = &Mapping{
		Publisher:  prof.DepotHost,
		Creator:    prof.Email,
		DepotHost:  prof.DepotHost,
		ReleasedAt: time.Now().Format(time.RFC3339),
		Release:    "1.0",
	}

	t := template.Must(template.New("file").Parse(packageCat))
	var f *os.File
	f, err = os.Create(filepath.Join(dir, "packages.cat"))
	defer f.Close()
	if err != nil {
		log.Error.Fatal(err)
	}
	err = t.Execute(f, mapping)
	if err != nil {
		log.Error.Fatal(err)
	}
	// generate package.lst

	return err
}

const packageCat = `<?xml version="1.0" encoding="UTF-8"?>
<packageListCatalogue xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/PackageListCatalogue1-0" xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1">
  <content releasedAt="{{.ReleasedAt}}" publisher="{{.Publisher}}">
    <release>{{.Release}}</release>
    <creators>{{.Creator}}</creators>
    <packageListFiles>{{.DepotHost}}/packages.lst</packageListFiles>
  </content>
</packageListCatalogue>
`

func createTempDir(tempDir string, generatePath string) string {

	if tempDir == "" {
		dir := filepath.Join(generatePath, "_packages")
		if _, err := os.Stat(dir); err == nil {
			if err = os.RemoveAll(dir); err != nil {
				log.Error.Fatal("can not remove directory", dir)
			}
		}

		// gucken, ob tempDir schon existiert, wenn ja dann löschen ...

		if err := os.Mkdir(dir, os.ModeDir|0700); err == nil {
			tempDir = dir
			log.Info.Println("tempDir", tempDir, "created")
		} else {
			log.Error.Fatalln("can not generate _packages directory in '", generatePath, "':", err)
		}
	}
	return tempDir
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}
