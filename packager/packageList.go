package packager

import (
	"crypto/sha256"
	"encoding/base64"
	"io"
	"os"
	"path/filepath"
	"sort"
	"text/template"
	"time"

	"gitlab.com/openp23r/p23r-common/exec"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/modelPackage"
	"gitlab.com/openp23r/p23r-tool/profile"
	"gitlab.com/openp23r/p23r-tool/rulePackage"
	"gitlab.com/openp23r/p23r-tool/testPackage"
)

// Parameter contains the context values for the content
type Parameter struct {
	PackageListVersion string
}

func createFile(rootPath, path string, fileName string, parameter *Parameter, prof *profile.Profile, packagesBuilded []int) (err error) {
	// generate package.lst
	packages := ""
	var rp rulePackage.Manifest
	var dmp modelPackage.Manifest

	// Hack: we order the list according to size becouce the order of elements is fixed in xsd file
	sort.Ints(packagesBuilded)

	for _, x := range packagesBuilded {
		packageText := ""
		switch x {
		case manifest.RuleGroupManifestID:
			rp, packageText = createRulePackages(rootPath, parameter.PackageListVersion, prof)
			packages += packageText
			break
		case manifest.ModelManifestID:
			dmp, packageText = createModelPackages(rootPath, parameter.PackageListVersion, prof)
			packages += packageText
			break
		case manifest.TestSetManifestID:
			packages = packages + createTestPackages(rootPath, parameter.PackageListVersion, prof, rp, dmp)
			break

		}
	}

	type Mapping struct {
		Publisher, Creator, DepotHost, ReleasedAt, Release, UUID, Packages string
	}
	var mapping *Mapping
	mapping = &Mapping{
		Publisher:  prof.DepotHost,
		Creator:    prof.Email,
		DepotHost:  prof.DepotHost,
		ReleasedAt: time.Now().Format(time.RFC3339),
		Release:    parameter.PackageListVersion,
		Packages:   packages,
	}

	t := template.Must(template.New("file").Parse(packageListTemplate))
	var f *os.File
	f, err = os.Create(filepath.Join(path, "Manifest.xml"))
	defer f.Close()
	if err != nil {
		log.Error.Fatalln(err)
	}
	err = t.Execute(f, mapping)
	if err != nil {
		log.Error.Fatalln(err)
	}
	// generate package.lst

	return err
}

// GeneratePackageList generates a package.lst file
func GeneratePackageList(prof *profile.Profile, generatePath, version string, packagesBuilded []int, noDelete, verbose bool) {
	// Create directories
	groundPath := filepath.Join(generatePath, "PackageList", prof.PackageName, version)
	err := os.MkdirAll(groundPath, os.ModeDir|0700)
	if err != nil {
		log.Error.Fatalln(err)
	}
	log.Info.Println("groundPath:", groundPath)

	createFile(generatePath, groundPath, "Manifest.xml", &Parameter{PackageListVersion: version}, prof, packagesBuilded)

	log.Info.Println("generate zip file:", filepath.Join(generatePath, "packages.zip"))
	/*
		zip := new(archivex.ZipFile)
		zip.Create(filepath.Join(generatePath, "packages.zip"))
		zip.AddAll(filepath.Join(generatePath, "PackageList", prof.PackageName), true)
		zip.Close()
	*/
	env := os.Environ()
	cmdArgs := []string{"-r", filepath.Join(generatePath, "packages.zip"), prof.PackageName}
	exec.Exec("zip", env, cmdArgs, filepath.Join(generatePath, "PackageList"))

	// move packages.zip to packages.lst
	err = os.Rename(filepath.Join(generatePath, "packages.zip"), filepath.Join(generatePath, "packages.lst"))
	if err != nil {
		log.Error.Fatalln(err)
	}

	if !noDelete {
		toDeletePath := filepath.Join(generatePath, "PackageList")
		log.Info.Println("delete temporary directory", toDeletePath)
		if err = os.RemoveAll(toDeletePath); err != nil {
			log.Warning.Println("in delete temporary directory", toDeletePath, ":", err)
		}
	}
}

func createRulePackages(path, version string, prof *profile.Profile) (rulePackage.Manifest, string) {
	pathToManifest := filepath.Join(path, "nrp", prof.PackageName, version)
	log.Info.Println("start reading NRP of", pathToManifest)
	pm := rulePackage.XML2PackageManifest(pathToManifest)

	fileNameOfPackage := "NRP-" + prof.PackageName + ".zip"

	hashOfPackage, err := computeSha256(filepath.Join(path, fileNameOfPackage))
	if err != nil {
		log.Error.Fatalln("can not create md5 hash value of", filepath.Join(path, fileNameOfPackage), ":", err)
	}

	log.Info.Println("compute hash of:", filepath.Join(path, fileNameOfPackage))

	locationOfPackage := prof.DepotHost + "/" + fileNameOfPackage
	pm.Location = locationOfPackage
	pm.Hash = base64.StdEncoding.EncodeToString(hashOfPackage)

	return pm, rulePackageArtefakt2XML(pm, locationOfPackage, hashOfPackage)
}

func createModelPackages(path, version string, prof *profile.Profile) (modelPackage.Manifest, string) {
	pathToManifest := filepath.Join(path, "dmp", prof.PackageName, version)
	log.Info.Println("start reading DMP of", pathToManifest)
	pm := modelPackage.XML2PackageManifest(pathToManifest)

	fileNameOfPackage := "DMP-" + prof.PackageName + ".zip"

	hashOfPackage, err := computeSha256(filepath.Join(path, fileNameOfPackage))
	if err != nil {
		log.Error.Fatalln("ERROR: can not create md5 hash value of", filepath.Join(path, fileNameOfPackage), ":", err)
	}
	locationOfPackage := prof.DepotHost + "/" + fileNameOfPackage

	return pm, modelPackageArtefakt2XML(pm, locationOfPackage, hashOfPackage)
}

func createTestPackages(path, version string, prof *profile.Profile, rp rulePackage.Manifest, dmp modelPackage.Manifest) string {
	pathToManifest := filepath.Join(path, "tp", prof.PackageName, version)
	log.Info.Println("start reading TP of", pathToManifest)
	pm := testPackage.XML2PackageManifest(pathToManifest)

	fileNameOfPackage := "TP-" + prof.PackageName + ".zip"

	hashOfPackage, err := computeSha256(filepath.Join(path, fileNameOfPackage))
	if err != nil {
		log.Error.Fatalln("can not create md5 hash value of", filepath.Join(path, fileNameOfPackage), ":", err)
	}
	locationOfPackage := prof.DepotHost + "/" + fileNameOfPackage

	return testPackageArtefakt2XML(pm, locationOfPackage, hashOfPackage, rp, dmp)
}

func computeSha256(filePath string) ([]byte, error) {
	var result []byte
	log.Info.Println("calculate hash of file:", filePath)
	file, err := os.Open(filePath)
	if err != nil {
		return result, err
	}
	defer file.Close()

	hash := sha256.New()
	if _, err := io.Copy(hash, file); err != nil {
		return result, err
	}

	return hash.Sum(nil), nil
}

func rulePackageArtefakt2XML(m rulePackage.Manifest, locationOfPackage string, hashOfPackage []byte) string {

	xml := `
<rulePackages`
	if m.ID != "" {
		xml += " id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += " name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += " title=\"" + m.Title + `"`
	}
	if m.ReleasedAt != "" {
		xml += " releasedAt=\"" + m.ReleasedAt + `"`
	}
	xml += ">"
	if m.Release != "" {
		xml += "\n <release>" + m.Release + "</release>"
	}
	for _, x := range m.Subjects {
		xml += "\n <subjects>" + x + "</subjects>"
	}
	xml += "\n <rulePackageFile location=\"" + locationOfPackage + "\">"
	xml += "\n   <cc:hash>" + base64.StdEncoding.EncodeToString(hashOfPackage) + "</cc:hash>"
	xml += "\n </rulePackageFile>"

	for _, x := range m.Descriptions {
		xml += "\n <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	for _, x := range m.Targets {
		xml += "\n <targets lang=\"" + x.Lang + "\">" + x.Text + "</targets>"
	}
	xml += "\n <dependencies>"
	xml += "\n   <cc:p23rSpecifications>" + m.SpecificationVersion + "</cc:p23rSpecifications>"
	xml += "\n </dependencies>"
	/*
		for _, x := range m.Licenses {
			xml += "\n  <licenses lang=\"" + x.Lang + "\">" + x.Text + "</licenses>"
		}
	*/
	xml += "\n</rulePackages>"
	return xml
}

func modelPackageArtefakt2XML(m modelPackage.Manifest, locationOfPackage string, hashOfPackage []byte) string {

	xml := `
<modelPackages`
	if m.ID != "" {
		xml += " id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += " name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += " title=\"" + m.Title + `"`
	}
	if m.ReleasedAt != "" {
		xml += " releasedAt=\"" + m.ReleasedAt + `"`
	}
	xml += ">"
	if m.Release != "" {
		xml += "\n <release>" + m.Release + "</release>"
	}
	for _, x := range m.Subjects {
		xml += "\n <subjects>" + x + "</subjects>"
	}
	xml += "\n <modelPackageFile location=\"" + locationOfPackage + "\">"
	xml += "\n   <cc:hash>" + base64.StdEncoding.EncodeToString(hashOfPackage) + "</cc:hash>"
	xml += "\n </modelPackageFile>"

	for _, x := range m.Descriptions {
		xml += "\n <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	for _, x := range m.Targets {
		xml += "\n <targets lang=\"" + x.Lang + "\">" + x.Text + "</targets>"
	}
	xml += "\n <dependencies>"
	xml += "\n   <cc:p23rSpecifications>" + m.SpecificationVersion + "</cc:p23rSpecifications>"
	xml += "\n </dependencies>"
	/*
		for _, x := range m.Licenses {
			xml += "\n  <licenses lang=\"" + x.Lang + "\">" + x.Text + "</licenses>"
		}
	*/
	xml += "\n</modelPackages>"
	return xml
}

func testPackageArtefakt2XML(m testPackage.Manifest, locationOfPackage string, hashOfPackage []byte, rp rulePackage.Manifest, dmp modelPackage.Manifest) string {

	xml := `
	<testPackages`
	if m.ID != "" {
		xml += " id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += " name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += " title=\"" + m.Title + `"`
	}
	if m.ReleasedAt != "" {
		xml += " releasedAt=\"" + m.ReleasedAt + `"`
	}
	xml += ">"
	if m.Release != "" {
		xml += "\n <release>" + m.Release + "</release>"
	}
	for _, x := range m.Subjects {
		xml += "\n <subjects>" + x + "</subjects>"
	}
	xml += "\n <testPackageFile location=\"" + locationOfPackage + "\">"
	xml += "\n   <cc:hash>" + base64.StdEncoding.EncodeToString(hashOfPackage) + "</cc:hash>"
	xml += "\n </testPackageFile>"

	for _, x := range m.Descriptions {
		xml += "\n <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	xml += "\n <dependencies>"
	if len(rp.Name) > 0 || len(dmp.Name) > 0 {
		if len(rp.Name) > 0 {
			xml += "\n 	<cc:rulePackages name=\"" + rp.Name + "\">"
			xml += "\n 		<cc:releases>" + rp.Release + "</cc:releases>"
			xml += "\n 	</cc:rulePackages>"

		}
		if len(dmp.Name) > 0 {
			xml += "\n 		<cc:modelPackages name=\"" + dmp.Name + "\">"
			xml += "\n 			<cc:releases>" + dmp.Release + "</cc:releases>"
			xml += "\n 		</cc:modelPackages>"
		}

	}
	xml += "\n   <cc:p23rSpecifications>" + m.SpecificationVersion + "</cc:p23rSpecifications>"
	xml += "\n </dependencies>"

	/*
		for _, x := range m.Licenses {
			xml += "\n  <licenses lang=\"" + x.Lang + "\">" + x.Text + "</licenses>"
		}
	*/
	xml += "\n</testPackages>"
	return xml
}

const packageListTemplate = `<?xml version="1.0" encoding="UTF-8"?>
<packageList xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/PackageList1-1" xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
  <content releasedAt="{{.ReleasedAt}}" publisher="{{.Publisher}}">
    <release>{{.Release}}</release>
    <creators>{{.Creator}}</creators>
    {{.Packages}}
  </content>
  <signedContent>
    <ds:Signature Id="idvalue0">
      <ds:SignedInfo Id="idvalue1">
        <ds:CanonicalizationMethod Algorithm="http://tempuri.org"/>
        <ds:SignatureMethod Algorithm="http://tempuri.org">
          <ds:HMACOutputLength>0</ds:HMACOutputLength>
        </ds:SignatureMethod>
        <ds:Reference Id="idvalue2" Type="http://tempuri.org" URI="http://tempuri.org">
          <ds:Transforms>
            <ds:Transform Algorithm="http://tempuri.org">
              <ds:XPath>ds:XPath</ds:XPath>
            </ds:Transform>
          </ds:Transforms>
          <ds:DigestMethod Algorithm="http://tempuri.org"/>
          <ds:DigestValue>MA==</ds:DigestValue>
        </ds:Reference>
      </ds:SignedInfo>
      <ds:SignatureValue Id="idvalue3">MA==</ds:SignatureValue>
      <ds:KeyInfo Id="idvalue4">
        <ds:KeyName>ds:KeyName</ds:KeyName>
      </ds:KeyInfo>
      <ds:Object Encoding="http://tempuri.org" Id="idvalue5" MimeType="">
        <ds:SPKIData>
          <ds:SPKISexp>MA==</ds:SPKISexp>
        </ds:SPKIData>
      </ds:Object>
    </ds:Signature>
  </signedContent>
</packageList>

`
