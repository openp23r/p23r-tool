package test

import "github.com/gin-gonic/gin"

// AppCSS is app.css for application specific settings
func AppCSS(c *gin.Context) {
	c.String(200, appCSS)
}

const appCSS = `#main {
  margin-top: 5em;
}
.click {
  cursor: pointer;
}`
