package test

import (
	"fmt"

	"github.com/codegangsta/cli"
	"github.com/gin-gonic/gin"

	"gitlab.com/openp23r/p23r-common/log"
)

// HTTPService starts a local http server to visualize
// the last result of a test run
func HTTPService(path string, c *cli.Context) {
	port := c.Int("port")

	// run gin-gonic in production mode
	if !c.Bool("test") {
		gin.SetMode(gin.ReleaseMode)
	}
	m := gin.Default()

	// The view with the test results
	m.GET("/", Index)
	m.GET("/index.html", Index)

	// The static components are available as fixed content via the
	// corresponding routes.
	m.GET("/styles/app.css", AppCSS)
	m.GET("/styles/bootstrap.css", BootstrapCSS)
	m.GET("/styles/bootstrap-theme.css", BootstrapThemeCSS)
	m.GET("/styles/bootstrap-theme.css.map", BootstrapThemeCSSMap)
	m.GET("/styles/highlight.css", HighlightCSS)
	m.GET("/scripts/jquery.js", JqueryJs)
	m.GET("/scripts/bootstrap.js", BootstrapJs)
	m.GET("/scripts/angular.min.js", AngularJs)
	m.GET("/scripts/angular.min.js.map", AngularJsMap)
	m.GET("/scripts/controllers.js", ControllersJs)
	m.GET("/scripts/vkBeautify.js", VkBeautifyJs)
	m.GET("/scripts/highlight.js", HighlightJs)
	m.GET("/test/result", Result)

	// Start http server on a valid port
	if 0 < port && port < 65536 {
		err := m.Run(fmt.Sprintf(":%v", port))
		if err != nil {
			log.Error.Fatalf("HTTP server exit with error: %s", err.Error())
		}
	} else {
		log.Error.Fatalf("Wrong port number %v", port)
	}
}
