package test

import (
	"bytes"
	"io/ioutil"
	"path/filepath"
	"text/template"

	"github.com/codegangsta/cli"
	"github.com/odeke-em/go-uuid"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/message"
)

// Deliver the given file or manifest in the given directory to a p23r processor
func Deliver(name string, c *cli.Context) {

	// Read message file
	data, err := ioutil.ReadFile(name)
	if err != nil {
		data, err = ioutil.ReadFile(filepath.Join("Message.xml"))
	}
	if err != nil {
		log.Error.Fatalln("No message file found")
	}

	// Set UUIDs
	msg := bytes.Buffer{}
	tmpl := template.Must(template.New("message").Parse(string(data)))
	tmpl.Execute(&msg, &parameter{TESTUUID: uuid.New()})

	_, err = message.Deliver(c.String("server"), c.String("tenant"), c.String("context"), "_", msg.String())
	if err != nil {
		log.Error.Fatalln(err)
	}
}
