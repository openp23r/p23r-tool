p23rApp = angular.module('p23rApp', [])

p23rApp.controller('IndexCtrl', ['$scope', '$http', ($scope, $http) ->
	$http.get('test/result').success( (data) ->
		$scope.result = data
		$scope.contexts = {}
		for x in $scope.result.Tests
			if $scope.contexts[x.Context]
				$scope.contexts[x.Context].tests.push(x)
			else
				$scope.contexts[x.Context] = {name: x.Context, tests: [x]}
		$scope.setTest = (testCaseId) =>
			for x in $scope.result.Tests
				if x.TestCaseID == testCaseId
					$scope.test = x
		$scope.setStep = (step) =>
			for x in $scope.test.ProtocolEntries
				if x.Trace && x.Trace.Step == step
					$scope.step = x
)])
