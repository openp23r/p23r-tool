package test

import "github.com/gin-gonic/gin"

// Index is the index.html
func Index(c *gin.Context) {
	c.Header("content-type", "text/html")
	c.String(200, `
		<head>
			<title>P23R-Tool / Test</title>
			<link rel="stylesheet" href="styles/bootstrap.css" />
			<link rel="stylesheet" href="styles/bootstrap-theme.css" />
			<link rel="stylesheet" href="styles/app.css" />
			<link rel="stylesheet" href="styles/highlight.css">
			<script src="scripts/jquery.js"></script>
			<script src="scripts/bootstrap.js"></script>
			<script src="scripts/angular.min.js"></script>
			<script src="scripts/controllers.js"></script>
			<script src="scripts/vkBeautify.js"></script>
			<script src="scripts/highlight.js"></script>
			<script>hljs.initHighlightingOnLoad();</script>
		<head>
		<body ng-app="p23rApp" ng-controller="IndexCtrl as root">
			<NoScript>
				<div class="container-fluid">
					<div class="container">
						<div id="header" class="row">
							<div class="col-md-4">
								<h2 class="text-center">Test</h2>
								<h4 class="text-center text-muted">P23R-Tool</h4>
							</div>
						</div>
						<div class="alert alert-error appErrorBox">
							Es wird Javascript zur Nutzung vom P23R-Tool benötigt. Bitte überprüfen
	sie die Einstellungen ihres Webbrowsers.
						</div>
					</div>
				</div>
			</NoScript>
			<div id="supportJavascript">
				<header role="navigation" class="navbar navbar-inverse navbar-fixed-top">
					<div class="navbar-header">
						<span class="navbar-brand">
							P23R-Tool
						</span>
					</div>
				</header>
				<div id="main">
					<article class="container">
						<div class="row">
							<div class="col-lg-4 hidden-print">
								<h3>Tests</h3>
								<h4><small>{{result.StartedAt|date:'dd.MM.yyyy hh:mm'}} by {{result.StartedBy}}</small></h4>
								<p>&nbsp;</p>
								<ul class="list-group">
									<li class="list-group-item" ng-repeat="context in contexts">
										<ul class="list-unstyled">
											<li>
												<span class="text-info">{{context.name}}</span>
												<ul class="list-styled" ng-repeat="test in context.tests">
													<li ng-repeat="behaviour in test.Behaviours" class="click" ng-click="setTest(test.TestCaseID)">
														<span ng-class="{'text-success':test.Succeeded, 'text-danger':!test.Succeeded}">{{behaviour}}</span>
													</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
							</div>
							<div class="col-lg-8">
								<div class="text-center" ng-show="test">
									<h3>Info</h3>
									<div class="text-left">
										<form class="form-horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label">Rule Name</label>
												<div class="col-sm-10">
													<p class="form-control-static">{{firstStep.RuleName}}</p>
												</div>
												<label class="col-sm-2 control-label">Rule Id</label>
												<div class="col-sm-10">
													<p class="form-control-static">{{firstStep.RuleID}}</p>
												</div>
												<label class="col-sm-2 control-label">Message Id</label>
												<div class="col-sm-10">
													<p class="form-control-static">{{firstStep.MessageID}}</p>
												</div>
												<label class="col-sm-2 control-label">Notification Id</label>
												<div class="col-sm-10">
													<p class="form-control-static">{{firstStep.NotificationID}}</p>
												</div>
												<label class="col-sm-2 control-label">Transaction Id</label>
												<div class="col-sm-10">
													<p class="form-control-static">{{firstStep.TransactionID}}</p>
												</div>
											</div>
										</form>
									</div>
									<a class="btn btn-primary" href="#" role="button" ng-click="showTrace = true">Trace</a>
									&nbsp;
									<a class="btn btn-info" href="#" role="button" ng-click="showTrace = false">Log</a>
									<div class="text-left"  ng-show="showTrace">
										<table class="table table-striped table-condensed">
											<thead>
												<tr>
													<th>Step</th>
													<th>Notes</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="entry in test.ProtocolEntries" ng-click="setStep(entry.Trace.Step)" class="click">
													<td ng-show="entry.Trace">{{entry.Trace.Step}}</td>
													<td ng-show="entry.Trace">
														<div ng-repeat="note in entry.Trace.Notes">
															{{note.Content}}
														</div>
													</td>
												</tr>
											</tbody>
										</table>
										<div ng-show="step">
											<hr/>
											<h5>Step Details</h5>
											<ul class="nav nav-tabs">
												<li role="presentation" ng-class="{active:!stepDetailView || stepDetailView == 1}" ng-click="stepDetailView = 1">
													<a href="#">Profile Script</a>
												</li>
												<li role="presentation" ng-class="{active:stepDetailView == 2}" ng-click="stepDetailView = 2">
													<a href="#">Input</a>
												</li>
												<li role="presentation" ng-class="{active:stepDetailView == 3}" ng-click="stepDetailView = 3">
													<a href="#">Output</a>
												</li>
												<li role="presentation" ng-class="{active:stepDetailView == 4}" ng-click="stepDetailView = 4">
													<a href="#">Transformation Script</a>
												</li>
												<li role="presentation" ng-class="{active:stepDetailView == 5}" ng-click="stepDetailView = 5">
													<a href="#">Input</a>
												</li>
												<li role="presentation" ng-class="{active:stepDetailView == 6}" ng-click="stepDetailView = 6">
													<a href="#">Output</a>
												</li>
											</ul>
											<pre ng-hide="stepDetailView > 1"><div ng-bind-html="profileScriptSave()"/></pre>
											<pre ng-show="stepDetailView == 2"><div ng-bind-html="inputProfileSave()"/></pre>
											<pre ng-show="stepDetailView == 3"><div ng-bind-html="outputProfileSave()"/></pre>
											<pre ng-show="stepDetailView == 4"><div ng-bind-html="transformationScriptSave()"/></pre>
											<pre ng-show="stepDetailView == 5"><div ng-bind-html="inputNotificationSave()"/></pre>
											<pre ng-show="stepDetailView == 6"><div ng-bind-html="outputNotificationSave()"/></pre>
										</div>
									</div>
									<div class="text-left" ng-hide="showTrace">
										<p>&nbsp;</p>
										<ul class="list-unstyled">
											<li ng-repeat="log in test.ProtocolEntries">
												<p ng-show="log.Log" ng-class="{'bg-danger': log.Log.Level == 'error' || log.Log.Level == 'test.fail', 'bg-warning': log.Log.Level == 'security','bg-success':log.Log.Level == 'test.success'}" ng-repeat="note in log.Log.Notes">{{log.Log.Level|uppercase}} {{log.Log.ComponentName}}: {{note.Content}}</p>
												<p ng-show="log.Trace">TRACE: Step {{log.Trace.Step}}</p>
												<p ng-show="log.GenericContent">{{log.GenericContent.Content}}</p>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</article>
				</div>
			</div>
		</body>`)
}
