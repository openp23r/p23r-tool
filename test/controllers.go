package test

import "github.com/gin-gonic/gin"

// ControllersJs is app.css for application specific settings
func ControllersJs(c *gin.Context) {
	c.Header("content-type", "application/javascript")
	c.String(200, controllersJs)
}

const controllersJs = `
angular.module('p23rControllers', []).controller('IndexCtrl', [
  '$scope', '$http', '$sce', function($scope, $http, $sce) {
    return $http.get('test/result').success(function(data) {
      var i, len, ref, x;
      $scope.result = data;
      $scope.contexts = {};
      ref = $scope.result.Tests;
      for (i = 0, len = ref.length; i < len; i++) {
        x = ref[i];
        if ($scope.contexts[x.Context]) {
          $scope.contexts[x.Context].tests.push(x);
        } else {
          $scope.contexts[x.Context] = {
            name: x.Context,
            tests: [x]
          };
        }
      }
      $scope.setTest = (function(_this) {
        return function(testCaseId) {
          $scope.showTrace = true
          var j, len1, len2, ref1, ref2, results;
          ref1 = $scope.result.Tests;
          results = [];
          for (j = 0, len1 = ref1.length; j < len1; j++) {
            x = ref1[j];
            if (x.TestCaseID === testCaseId) {
              results.push($scope.test = x);
            } else {
              results.push(void 0);
            }
          }
          ref2 = $scope.test.ProtocolEntries;
          for (j = 0, len2 = ref2.length; j < len2; j++) {
            x = ref2[j];
            if (x.Trace && x.Trace.Step) {
              $scope.firstStep = x;
              break;
            };
          };
          return results;
        };
      })(this);
      $scope.beautify = (function(_this) {
        return function(input) {
          return input ? hljs.highlightAuto(vkbeautify.xml(input)).value : '<span>---</span>';
        };
      })(this);
      return $scope.setStep = (function(_this) {
        return function(step) {
          var j, len1, ref1, results;
          ref1 = $scope.test.ProtocolEntries;
          results = [];
          for (j = 0, len1 = ref1.length; j < len1; j++) {
            x = ref1[j];
            if (x.Trace && x.Trace.Step === step) {
              $scope.profileScript = $scope.beautify(x.Trace.ProfileScript)
              $scope.profileScriptSave = function() {
                return $sce.trustAsHtml($scope.profileScript);
              };
              $scope.inputProfile = $scope.beautify(x.Trace.InputProfile)
              $scope.inputProfileSave = function() {
                return $sce.trustAsHtml($scope.inputProfile);
              };
              $scope.outputProfile = $scope.beautify(x.Trace.OutputProfile)
              $scope.outputProfileSave = function() {
                return $sce.trustAsHtml($scope.outputProfile);
              };
              $scope.transformationScript = $scope.beautify(x.Trace.TransformationScript)
              $scope.transformationScriptSave = function() {
                return $sce.trustAsHtml($scope.transformationScript);
              };
              $scope.inputNotification = $scope.beautify(x.Trace.InputNotification)
              $scope.inputNotificationSave = function() {
                return $sce.trustAsHtml($scope.inputNotification);
              };
              $scope.outputNotification = $scope.beautify(x.Trace.OutputNotification)
              $scope.outputNotificationSave = function() {
                return $sce.trustAsHtml($scope.outputNotification);
              };
              results.push($scope.step = x);
            } else {
              results.push(void 0);
            }
          }
          return results;
        };
      })(this);
    });
  }
]);

angular.module('p23rFilters', []).filter('beautify', function() {
  return function(input) {
    return input ? hljs.highlightAuto(vkbeautify.xml(input)).value : '---';
  };
});

angular.module('p23rApp', ['p23rControllers','p23rFilters']);`
