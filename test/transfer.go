package test

import (
	"bytes"
	"io/ioutil"
	"path/filepath"
	"text/template"

	"github.com/codegangsta/cli"
	"github.com/odeke-em/go-uuid"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/notificationTransfer"
)

// Transfer the given file or manifest in the given directory to a p23r processor
func Transfer(name string, c *cli.Context) {

	var json notificationTransfer.Parameters
	// Read notification transfer file
	data, err := ioutil.ReadFile(name)
	if err != nil {
		data, err = ioutil.ReadFile(filepath.Join("NotificationTransfer.json"))
		if err == nil {
			// Set UUIDs
			msg := bytes.Buffer{}
			tmpl := template.Must(template.New("message").Parse(string(data)))
			tmpl.Execute(&msg, &parameter{TESTUUID: uuid.New()})
			json, err = notificationTransfer.ParseJSON(string(msg.String()))
			if err != nil {
				log.Error.Fatalf("Could not parse notification transfer json file: '%v'\n", err)
			}
		}
	}
	if err != nil {
		data, err = ioutil.ReadFile(filepath.Join("NotificationTransfer.yaml"))
		if err == nil {
			// Set UUIDs
			msg := bytes.Buffer{}
			tmpl := template.Must(template.New("message").Parse(string(data)))
			tmpl.Execute(&msg, &parameter{TESTUUID: uuid.New()})
			json, err = notificationTransfer.ParseYAML(string(msg.String()))
			if err != nil {
				log.Error.Fatalf("Could not parse notification transfer yaml file: '%v'\n", err)
			}
		}
	}
	if err != nil {
		log.Error.Fatalf("No notification transfer file found\n")
	}

	_, err = notificationTransfer.Transfer(c.String("server"), c.String("tenant"), c.String("context"), "", json, c.Bool("verbose"))
	if err != nil {
		log.Error.Fatalln(err)
	}
}
