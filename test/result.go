package test

import (
	"io/ioutil"
	"path/filepath"

	"github.com/gin-gonic/gin"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/protocol"
)

// Result gives back the latest test protocol result as JSON data
func Result(c *gin.Context) {

	// Read test result file
	data, err := ioutil.ReadFile(filepath.Join("_test", "testlog.xml"))
	if err != nil {
		log.Error.Fatalln("Could not read test result file", filepath.Join("_test", "testlog.xml"))
	}

	content := protocol.ParseXML(data)
	c.JSON(200, content)
}
