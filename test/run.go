package test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/codegangsta/cli"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/profile"
)

type parameters struct {
	TestPackage      string   `json:"testpackage"`
	RulePackage      string   `json:"rulepackage"`
	SelectedTestSets []string `json:"selectedTestSets"`
	Username         string   `json:"username"`
	Password         string   `json:"password"`
}

// Run executes a test run
func Run(ids []string, c *cli.Context) {
	// Read profile to get depot host, user and password
	prof, err := profile.ReadProfile()
	if err != nil {
		log.Error.Fatalf("Can't read profile -> %v", err)
	}

	log.Info.Println("prof:", prof, ", ids: ", ids)

	if c.IsSet("depot-host") {
		prof.DepotHost = c.String("depot-host")
	} else {
		// Test if profile has the right values
		if len(prof.DepotHost) == 0 {
			log.Error.Fatalln("depot host must be set over command line (-d) or in defaults.")
		}
	}

	// prepare request parameter
	server := c.String("server")

	para := parameters{
		prof.DepotHost + "/TP-" + prof.PackageName + ".zip",
		prof.DepotHost + "/NRP-" + prof.PackageName + ".zip",
		ids,
		prof.DepotUser,
		prof.DepotPassword,
	}

	// call test service of p23r processor
	paraJSON, err1 := json.MarshalIndent(para, "", "  ")
	if err1 != nil {
		log.Error.Fatalln("err after marshalling:", err)
	}

	log.Info.Println("JSON object:", string(paraJSON))

	contentReader := bytes.NewReader(paraJSON)
	resp, err := http.Post(server, "application/json", contentReader)
	if err != nil {
		log.Error.Fatalf("Could not run tests on server %s -> %v\n", server, err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error.Fatalf("Failed to receive answer from server %s -> %v\n", server, err)
	}

	// create _test directory
	if err = os.Mkdir("_test", 0755); err != nil {
	}

	// save test protocol in local file
	if err = ioutil.WriteFile("_test/testlog.xml", body, 0644); err != nil {
		log.Error.Fatalf("Failed to create testlog.xml: %v\n", err)
	}

}
