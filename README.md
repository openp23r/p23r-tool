# README.md

Author: Jan Gottschick <service@openp23r.org>

The P23R tool supports developer of P23R notification rule groups, test sets and models.
It should be used to _create_ initial rule groups, test sets and models. The templates
have to be filled with the specific content. Further, the tool should be
used to _validate_ rule groups, test sets and models before these are published to a
P23R Depot.

The P23R tool will support a seamless publishing process from the developer to the P23R depot.

## Usage as a command

The P23R tool is a command line tool to create and validate rule groups, test sets and models.

	p23r help

At first you should set your default values, which will be automatically filled in the templates.

	p23r create defaults -g

You can easily create a new rule group with rules inside, e.g.

	mkdir myProject
	cd myProject
	p23r create rulegroup -s houseMoving
	cd houseMoving
	p23r create rule addressChange
	p23r create rule changeOfAddressOrder

When you completed your rule group you can validate its content, e.g.

	cd myProject
	p23r check houseMoving

The validation checks the syntax of all files and if all required files exists.

## Distribution

You can translate the source using the tools for the Go language (http://golang.org). The
following binary distributions are avalaible:

- Mac OSX <a href="https://gitlab.com/openp23r/p23r-tool/blob/master/distribution/macosx/build/P23R%20Tool.mpkg/Contents/Packages/P23R%20Tool.pkg">⬇︎</a>
- Linux <a href="https://gitlab.com/openp23r/p23r-tool/raw/developer/distribution/linux/p23r">⬇︎</a>

## Legal issues

This project is part of the *openP23R* initiative. All related projects
are listed at https://gitlab.com/openp23r/openp23r .

The legal conditions see *LICENSE* file.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS
