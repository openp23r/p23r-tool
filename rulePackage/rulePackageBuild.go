package rulePackage

import (
	"path/filepath"

	"github.com/jteeuwen/go-pkg-xmlx"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/rulegroup"
)

// Manifest describes the items of a rule package manifest file
type Manifest struct {
	ID                   string
	Name                 string
	Title                string
	Publisher            string
	Release              string
	ReleasedAt           string
	Subjects             []string
	Creators             []string
	SpecificationVersion string
	ModelPackageName     string
	ModelPackageReleases string
	Descriptions         []Description
	Targets              []Description
	Licenses             []Description
	Location             string
	Hash                 string
}

// Description is a multi lingual text
type Description struct {
	Lang string
	Text string
}

// Unmarshall important fields
func Unmarshall(path string) rulegroup.Manifest {
	doc := xmlx.New()
	var err = doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Fatalln("Could not read XML file", filepath.Join(path, "Manifest.xml"))
	}
	node := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1", "ruleGroupManifest")

	var m rulegroup.Manifest
	m.Name = node.As("", "name")
	m.Title = node.As("", "title")
	m.ValidFrom = node.As("", "validFrom")
	m.ValidUntil = node.As("", "validUntil")

	for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1", "subjects") {
		m.Subjects = append(m.Subjects, x.GetValue())
	}

	return m

}

// XML2PackageManifest reads in a rule package manifest (only the used values in build the package list)
func XML2PackageManifest(path string) Manifest {
	doc := xmlx.New()
	var err = doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Fatalln("Could not read XML file", filepath.Join(path, "Manifest.xml"))
	}

	node := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1", "rulePackageManifest")
	var pm Manifest
	pm.ID = node.As("", "id")
	pm.Name = node.As("", "name")
	pm.Title = node.As("", "title")
	pm.ReleasedAt = node.As("", "releasedAt")
	x := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1", "release")
	pm.Release = x.GetValue()
	for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1", "subjects") {
		pm.Subjects = append(pm.Subjects, x.GetValue())
	}
	for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1", "targets") {
		var t Description
		t.Lang = x.As("", "lang")
		t.Text = x.GetValue()
		pm.Targets = append(pm.Targets, t)
	}
	for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1", "descriptions") {
		var d Description
		d.Lang = x.As("", "lang")
		d.Text = x.GetValue()
		pm.Descriptions = append(pm.Descriptions, d)
	}
	x = doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1", "dependencies")
	specNode := x.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1", "p23rSpecifications")
	pm.SpecificationVersion = specNode.GetValue()

	/*
		for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1", "subjects") {
			pm.Subjects = append(pm.Subjects, x.GetValue())
		}
	*/
	return pm

}

func rulePackageManifest2XML(m *Manifest) []byte {
	xml := `<?xml version="1.0" encoding="UTF-8"?>
	<rulePackageManifest xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1"
		xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1"
`
	if m.ID != "" {
		xml += "\n  id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += "\n  name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += "\n  title=\"" + m.Title + `"`
	}
	if m.Publisher != "" {
		xml += "\n  publisher=\"" + m.Publisher + `"`
	}
	if m.ReleasedAt != "" {
		xml += "\n  releasedAt=\"" + m.ReleasedAt + `"`
	}
	xml += ">"
	if m.Release != "" {
		xml += "\n  <release>" + m.Release + "</release>"
	}
	for _, x := range m.Subjects {
		xml += "\n  <subjects>" + x + "</subjects>"
	}
	for _, x := range m.Creators {
		xml += "\n  <creators>" + x + "</creators>"
	}
	xml += "\n <dependencies>"
	if m.ModelPackageName != "" {
		xml += "\n  <cc:modelPackages name=\"" + m.ModelPackageName + "\">"
		xml += "\n  <cc:releases>" + m.ModelPackageReleases + "</cc:releases>"
		xml += "\n  </cc:modelPackages>"
	}

	xml += "\n <cc:p23rSpecifications>" + m.SpecificationVersion + "</cc:p23rSpecifications>"
	xml += "\n </dependencies>"
	for _, x := range m.Descriptions {
		xml += "\n  <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	for _, x := range m.Targets {
		xml += "\n  <targets lang=\"" + x.Lang + "\">" + x.Text + "</targets>"
	}
	for _, x := range m.Licenses {
		xml += "\n  <licenses lang=\"" + x.Lang + "\">" + x.Text + "</licenses>"
	}
	xml += "\n</rulePackageManifest>"
	return []byte(xml)
}
