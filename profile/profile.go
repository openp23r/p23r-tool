/*
The profile package provides function to read and update the profile,
which contains the default values used to prefill the templates or for
the packaging process.

The profile file can be located in the current directory
(p23r-profile.yaml) or the home directory of the user
(.p23r-profile.yaml) for updating. For reads it can be located in the
superior directory, additionally.
*/

package profile

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// Profile contains the default values of the rule developer
type Profile struct {
	Firstname, Lastname, Organisation, Domain, Section, Email, PackageName, DepotHost, DepotUser, DepotPassword, Registry string
}

// Read a single value from the stdio command line after a prompt.
func readValue(prompt, current string) string {
	fmt.Printf("%s (%s): ", prompt, current)
	bio := bufio.NewReader(os.Stdin)
	line, _, _ := bio.ReadLine()
	if len(line) == 0 {
		return current
	}
	return string(line)
}

// UpdateValues shows every value from the profile file
// to the user and allows to overwrite the current value
// on the command line.
func UpdateValues(global bool) error {
	var filename string
	if global {
		filename = os.Getenv("HOME") + "/.p23r-profile.yaml"
	} else {
		filename = "p23r-profile.yaml"
	}
	// Read profile from given or any other profile file
	data, err := ioutil.ReadFile(filename)
	var prof *Profile
	if err == nil {
		err = yaml.Unmarshal(data, &prof)
	}
	if err != nil {
		prof, _ = ReadProfile()
	}

	// Read values from user input
	prof.Firstname = readValue("Vorname", prof.Firstname)
	prof.Lastname = readValue("Nachname", prof.Lastname)
	prof.Email = readValue("Email Addresse", prof.Email)
	prof.Organisation = readValue("Organisation", prof.Organisation)
	prof.Domain = readValue("Internet-Domain", prof.Domain)
	prof.Section = readValue("Namespace-Section", prof.Section)
	prof.PackageName = readValue("Package Name", prof.PackageName)
	prof.DepotHost = readValue("Depot Host", prof.DepotHost)
	prof.DepotUser = readValue("Depot User", prof.DepotUser)
	prof.DepotPassword = readValue("Depot Password", prof.DepotPassword)
	prof.Registry = readValue("Docker Registry", prof.Registry)

	// Write profile file
	data, err = yaml.Marshal(prof)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, data, 0600)
	return err
}

// ReadProfile return the content of the profile file
func ReadProfile() (*Profile, error) {
	data, err := ioutil.ReadFile("./p23r-profile.yaml")
	if err != nil {
		data, err = ioutil.ReadFile("../p23r-profile.yaml")
	}
	if err != nil {
		data, err = ioutil.ReadFile(os.Getenv("HOME") + "/.p23r-profile.yaml")
	}
	var profile *Profile
	if err == nil {
		err = yaml.Unmarshal(data, &profile)
		if err != nil {
			return nil, err
		}
	} else {
		profile = new(Profile)
	}
	return profile, nil
}
