package testset

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"text/template"

	"github.com/jteeuwen/go-pkg-xmlx"
	"github.com/odeke-em/go-uuid"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/profile"
)

// Parameter contains the context values for the content
type Parameter struct {
	TestSet string
}

func valueOrDefault(value string, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

func createFile(path string, fileName string, parameter *Parameter, content string, prof *profile.Profile) {
	type Mapping struct {
		Firstname, Lastname, Organisation, TestCase, TestSet, Step, Domain, Section, UUID string
	}
	var mapping *Mapping
	var err error
	mapping = &Mapping{
		Firstname:    valueOrDefault(prof.Firstname, "--your-firstname--"),
		Lastname:     valueOrDefault(prof.Lastname, "--your-lastname--"),
		Organisation: valueOrDefault(prof.Organisation, "--your-organisation--"),
		Domain:       valueOrDefault(prof.Domain, "--your-domain--"),
		Section:      valueOrDefault(prof.Section, "--section--"),
		TestSet:      valueOrDefault(parameter.TestSet, "--test-set--"),
		UUID:         uuid.New(),
	}
	t := template.Must(template.New("file").Parse(content))
	var f *os.File
	f, err = os.Create(filepath.Join(path, fileName))
	defer f.Close()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	err = t.Execute(f, mapping)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")
}

func getTestset() (bool, string) {
	data, err := ioutil.ReadFile("Manifest.xml")
	subproject := true
	if err != nil {
		data, err = ioutil.ReadFile("../Manifest.xml")
		subproject = false
	}
	if err != nil {
		log.Error.Fatal("No manifest file found")
		return false, ""
	}
	doc := xmlx.New()
	err = doc.LoadString(string(data), nil)
	if err != nil {
		log.Error.Fatal(err)
		return false, ""
	}
	node := doc.SelectNode("*", "testSetManifest")
	if node == nil {
		return false, ""
	}
	return subproject, node.As("*", "name")
}

// GenerateTemplate generates all directories and files nescessary for a P23R
// test case.
//
func GenerateTemplate(name string, isSubproject bool, createSources bool) {
	// create directory if is subproject
	var path string
	if isSubproject {
		err := os.Mkdir(name, os.ModeDir|0700)
		if err != nil {
			println("F")
			log.Error.Fatal(err)
		}
		print(".")
		path = name
	} else {
		path = "."
	}

	// Read profile
	var prof *profile.Profile
	var err error
	prof, err = profile.ReadProfile()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}

	// create Manifest file for rule
	createFile(path, "Manifest.xml", &Parameter{TestSet: name}, testSetManifestXML, prof)

	// Create directories
	err = os.Mkdir(filepath.Join(path, "data"), os.ModeDir|0700)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")
	err = os.Mkdir(filepath.Join(path, "sources"), os.ModeDir|0700)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")

	if createSources {
		print("S")

		// Create Manifest content
		createFile(filepath.Join(path, "sources"), "Manifest.lityaml", &Parameter{TestSet: name}, testSetManifestLitYaml, prof)
	}
}

const testSetManifestLitYaml = `	xmlns: http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1

Basic information about the rule

	id: {{.UUID}}
	name: {{.TestSet}}
	title: «Titel für den Testfall»

	release: 0.1
	releasedat: 2015-01-01T12:00:00

Context in behaviour driven tests

	context: Given a ...

Keywords for a search portal and as filter for packaging

	subjects:
		- testset
		- Testsatz

List of authors and reviewers

	creators:
		- {{.Firstname}} {{.Lastname}} ({{.Organisation}})

	auditors:
		- «Vorname» «Nachname»

	auditbys:
		- «Organisation»

Textual descriptions in various languages for the people, which
decides about the usage of a test set.

	descriptions:
		- lang: de-DE
			text: |
				--

	licenses:
		- lang: de-DE
			text: |
				--
`

const testSetManifestXML = `<?xml version="1.0" encoding="UTF-8"?>
<testSetManifest
	xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

	id="{{.UUID}}"
	name="{{.TestSet}}"
	title="«Titel für den Testfall»"
	releasedAt="2015-01-01T12:00:00Z"
	context="Given a ..."
>
	<release>0.1</release>
	<subjects>testset</subjects>
	<subjects>Testsatz</subjects>

	<creators>{{.Firstname}} {{.Lastname}} ({{.Organisation}})</creators>
	<auditors>«Vorname» «Nachname»</auditors>
	<auditBys>«Organisation»</auditBys>

	<descriptions lang="de-DE">--</descriptions>
	<licenses lang="de-DE">--</licenses>

</testSetManifest>
`
