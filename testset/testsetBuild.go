package testset

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/markdown"
	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/testcase"

	"gopkg.in/yaml.v2"
)

// Manifest describes the format of the yaml file
type Manifest struct {
	ID           string
	Name         string
	Title        string
	Release      string
	ReleasedAt   string
	Subjects     []string
	Creators     []string
	Auditors     []string
	AuditBys     []string
	Context      string
	Descriptions []Description
	Licenses     []Description
}

// Description is a multi lingual text
type Description struct {
	Lang string
	Text string
}

func testSetManifest2XML(m *Manifest) []byte {
	xml := `<?xml version="1.0" encoding="UTF-8"?>
<testSetManifest
  xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
`
	if m.ID != "" {
		xml += "\n  id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += "\n  name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += "\n  title=\"" + m.Title + `"`
	}
	if m.ReleasedAt != "" {
		xml += "\n  releasedAt=\"" + m.ReleasedAt + `"`
	}
	if m.Context != "" {
		xml += "\n  context=\"" + m.Context + `"`
	}
	xml += "\n>"
	if m.Release != "" {
		xml += "\n  <release>" + m.Release + "</release>"
	}
	for _, x := range m.Subjects {
		xml += "\n  <subjects>" + x + "</subjects>"
	}
	for _, x := range m.Creators {
		xml += "\n  <creators>" + x + "</creators>"
	}
	for _, x := range m.Auditors {
		xml += "\n  <auditors>" + x + "</auditors>"
	}
	for _, x := range m.AuditBys {
		xml += "\n  <auditBys>" + x + "</auditBys>"
	}
	for _, x := range m.Descriptions {
		xml += "\n  <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	for _, x := range m.Licenses {
		xml += "\n  <licenses lang=\"" + x.Lang + "\">" + x.Text + "</licenses>"
	}
	xml += "\n</testSetManifest>"
	return []byte(xml)
}

// Build read all files from sources and convert them
func Build(path string, verbose bool) {
	// Generate Manifest.xml from yaml sources if file in sources is newer
	var sourceStat os.FileInfo
	data, err := ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.lityaml"))
	if err != nil {
		data, err = ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.yaml"))
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.yaml"))
	} else {
		data = markdown.Filter(data)
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.lityaml"))
	}
	if err == nil {
		targetStat, err := os.Stat(filepath.Join(path, "Manifest.xml"))
		if err != nil || sourceStat.ModTime().After(targetStat.ModTime()) {
			if verbose {
				log.Info.Println("create Manifest.xml from", filepath.Join(path, "sources"))
			}
			var m *Manifest
			err = yaml.Unmarshal(data, &m)
			if err == nil {
				err = ioutil.WriteFile(filepath.Join(path, "Manifest.xml"), testSetManifest2XML(m), 0600)
				if err != nil {
					log.Error.Fatalln("Couldn't write Manifest.xml in", path, "because", err)
				}
			} else {
				log.Error.Fatalln("Couldn't read Manifest in", filepath.Join(path, "sources"), "because", err)
			}
		}
	}

	// build all sub directories with a manifest inside
	xs, err := ioutil.ReadDir(path)
	dirs := []string{}
	if err == nil {
		for _, x := range xs {
			if x.IsDir() {
				dirs = append(dirs, x.Name())
			}
		}
	} else {
		log.Error.Fatalf("Can't read directory %v", path)
	}
	for _, x := range dirs {
		manifestType := manifest.GetManifestType(filepath.Join(path, x), verbose)
		switch manifestType {
		case manifest.RuleManifestID:
			log.Warning.Println("a test set may not contain rules")
		case manifest.NoManifestID:
		case manifest.UnknownManifestID:
			log.Warning.Printf("unknown kind of manifest in '%s'", filepath.Join(path, x))
		case manifest.RuleGroupManifestID:
			log.Warning.Println("a test set may not contain other rule groups")
		case manifest.TestCaseManifestID:
			testcase.Build(filepath.Join(path, x), verbose)
		case manifest.TestSetManifestID:
			log.Warning.Println("a test set may not contain test sets")
		case manifest.ModelManifestID:
			log.Warning.Println("a test set may not contain pivot data models")
		default:
			log.Warning.Printf("a test set may only contain rules and not '%s'", filepath.Join(path, x))
		}
	}

}
