package testset

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/jteeuwen/go-pkg-xmlx"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/testcase"
	"gitlab.com/openp23r/p23r-tool/xsd"
)

// Validate do
func Validate(path string, verbose bool) int {
	foundErrorNb := 0
	if verbose {
		log.Info.Println("validate test set " + path)
	}

	// validate if all mandatory files exist
	if verbose {
		log.Info.Println("check if all mandatory files exist")
	}
	if _, err := os.Stat(filepath.Join(path, "Manifest.xml")); err != nil {
		log.Error.Println("could not find the manifest file (Manifest.xml) in " + path)
		foundErrorNb++
	}

	// validate manifest using the schema
	commonNrl := xsd.SchemaDependence{Filename: "CommonNrl1-1.xsd", Schema: manifest.CommonNrl1_1Schema}
	dependencies := []*xsd.SchemaDependence{&commonNrl}
	if verbose {
		log.Info.Println("check if manifest is valid", filepath.Join(path, "Manifest.xml"))
	}
	if err := xsd.ValidateFile(filepath.Join(path, "Manifest.xml"), manifest.TestSetManifestSchema, dependencies); err != nil {
		log.Error.Println("could not validate", filepath.Join(path, "Manifest.xml"), "->", err)
		foundErrorNb++
	}

	// validate if directory name of test set is identical with the test set name
	// in the manifest
	if verbose {
		log.Info.Println("check if directory name of test set is valid")
	}
	doc := xmlx.New()
	err := doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Println("Could not read XML file", filepath.Join(path, "Manifest.xml"))
		foundErrorNb++
	}
	testSetName := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1", "testSetManifest").As("", "name")
	cwd, _ := os.Getwd()
	_, dirName := filepath.Split(filepath.Dir(filepath.Clean(filepath.Join(cwd, path, "x.y"))))
	if testSetName != dirName {
		log.Error.Println("Test set name doesn't match with directory name ", "'"+dirName+"'")
		foundErrorNb++
	}

	// Read files for current directory
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Error.Println("could read directory", path, "->", err)
		foundErrorNb++
	}

	// validate if all included files exist
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if all includes in XML file are valid", filepath.Join(path, f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XML file", filepath.Join(path, f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/2001/XInclude", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(filepath.Join(path, file)); err != nil {
						log.Error.Println("could not find the file '" + filepath.Join(path, file) + "', which is referenced in the file '" + filepath.Join(path, f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}

	// Read files for data directory and extend the files list
	datafiles, err := ioutil.ReadDir(filepath.Join(path, "data"))

	// validate if all included files exist
	for _, f := range datafiles {
		if strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if all includes in XML file are valid", filepath.Join(path, "data", f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, "data", f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XML file", filepath.Join(path, "data", f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/2001/XInclude", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(filepath.Join(path, "data", file)); err != nil {
						log.Error.Println("could not find the file '" + filepath.Join(path, "data", file) + "', which is referenced in the file '" + filepath.Join(path, "data", f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}

	// validate all sub directories with a manifest inside
	xs, err := ioutil.ReadDir(path)
	dirs := []string{}
	if err == nil {
		for _, x := range xs {
			if x.IsDir() {
				dirs = append(dirs, x.Name())
			}
		}
	} else {
		log.Error.Printf("Can't read directory %v", path)
		foundErrorNb++
	}
	for _, x := range dirs {
		manifestType := manifest.GetManifestType(filepath.Join(path, x), verbose)
		switch manifestType {
		case manifest.UnknownManifestID:
			log.Warning.Printf("unknown kind of manifest in '%s'", filepath.Join(path, x))
		case manifest.NoManifestID:
		case manifest.RuleManifestID:
			log.Error.Println("a test set may not contain rules")
			foundErrorNb++
		case manifest.RuleGroupManifestID:
			log.Error.Println("a test set may not contain rule groups")
			foundErrorNb++
		case manifest.TestCaseManifestID:
			testcase.Validate(filepath.Join(path, x), verbose)
		case manifest.TestSetManifestID:
			log.Error.Println("a test set may not contain other test sets")
			foundErrorNb++
		case manifest.ModelManifestID:
			log.Error.Println("a test set may not contain pivot data models")
			foundErrorNb++
		default:
			log.Error.Printf("a test set may only contain test cases and not '%s'", filepath.Join(path, x))
			foundErrorNb++
		}
	}

	return foundErrorNb
}
