package model

import (
	"os"
	"path/filepath"
	"text/template"

	"github.com/odeke-em/go-uuid"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/profile"
)

// Parameter contains the context values for the content
type Parameter struct {
	Model string
}

func valueOrDefault(value string, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

func createFile(path string, fileName string, parameter *Parameter, content string, prof *profile.Profile) {
	type Mapping struct {
		Firstname, Lastname, Organisation, Model, Domain, Section, UUID string
	}
	var mapping *Mapping
	var err error
	mapping = &Mapping{
		Firstname:    valueOrDefault(prof.Firstname, "«your firstname»"),
		Lastname:     valueOrDefault(prof.Lastname, "«your lastname»"),
		Organisation: valueOrDefault(prof.Organisation, "«your organisation»"),
		Domain:       valueOrDefault(prof.Domain, "«your domain»"),
		Section:      valueOrDefault(prof.Section, "«section»"),
		Model:        valueOrDefault(parameter.Model, "«model name»"),
		UUID:         uuid.New(),
	}
	t := template.Must(template.New("file").Parse(content))
	var f *os.File
	f, err = os.Create(filepath.Join(path, fileName))
	defer f.Close()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	err = t.Execute(f, mapping)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")
}

// GenerateTemplate generates all directories and files nescessary for a P23R
// notification rule group.
//
func GenerateTemplate(name string, isSubproject bool, createSources bool) {
	// create directory if is subproject
	var path string
	if isSubproject {
		err := os.Mkdir(name, os.ModeDir|0700)
		if err != nil {
			println("F")
			log.Error.Fatal(err)
		}
		print(".")
		path = name
	} else {
		path = "."
	}

	// Read profile
	var prof *profile.Profile
	var err error
	prof, err = profile.ReadProfile()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}

	// create Manifest file for model
	createFile(path, "Manifest.xml", &Parameter{Model: name}, modelManifestXML, prof)

	// Create directories
	err = os.Mkdir(filepath.Join(path, "sources"), os.ModeDir|0700)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")

	if createSources {
		print("S")

		// Create Manifest content
		createFile(filepath.Join(path, "sources"), "Manifest.lityaml", &Parameter{Model: name}, modelManifestLitYaml, prof)
	}
}

const modelManifestXML = `<?xml version="1.0" encoding="UTF-8"?>
<modelManifest

	xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

	id="{{.UUID}}"
	name="{{.Model}}"
	releasedAt="2014-01-01T12:00:00Z"
	namespace="http://{{.Domain}}/NS/{{.Section}}/{{.Model}}"
	schema="./{{.Model}}.xsd" >
	<release>0.1</release>
	<creators>{{.Firstname}} {{.Lastname}} ({{.Organisation}})</creators>
	<descriptions lang="de-DE">--</descriptions>
	<targets lang="de-DE">--</targets>
	<licenses lang="de-DE">--</licenses>

</modelManifest>
`

const modelManifestLitYaml = `	xmlns: http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1

Basic information about the model

	id: {{.UUID}}
	name: {{.Model}}
	title: «Titel für das Model»

	release: 0.1

The implemented schema of the model

	namespace: http://{{.Domain}}/NS/{{.Section}}/{{.Model}}
	schema: ./{{.Model}}.xsd

Keywords for a search portal and as filter for packaging

	subjects:
		- model
		- pivot
		- Modell
		- Pivot-Modell

List of authors and reviewers

	creators:
		- {{.Firstname}} {{.Lastname}} ({{.Organisation}})

Textual descriptions in various languages for the people, which
decides about the usage of a model.

	descriptions:
		- lang: de-DE
			text: |
				--

	targets:
		- lang: de-DE
			text: |
				--

	licenses:
		- lang: de-DE
			text: |
				--
`
