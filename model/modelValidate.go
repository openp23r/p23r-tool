package model

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/jbussdieker/golibxml"
	"github.com/jteeuwen/go-pkg-xmlx"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/xsd"
)

func localFileName(path string, name string) string {
	if strings.HasPrefix(name, "model:///") {
		return filepath.Join(path, "..", name[9:])
	}
	if strings.HasPrefix(name, "model://") {
		return filepath.Join(path, "..", "..", "..", name[8:])
	}
	return filepath.Join(path, name)
}

// Validate do
func Validate(path string, verbose bool) int {
	foundErrorNb := 0

	if verbose {
		log.Info.Println("validate model", path)
	}

	// validate if all mandatory files exist
	if verbose {
		log.Info.Println("check if all mandatory files exist")
	}
	if _, err := os.Stat(filepath.Join(path, "Manifest.xml")); err != nil {
		log.Error.Println("could not find the manifest file (Manifest.xml) in " + path)
		foundErrorNb++
	}

	// dependences for schema validation
	namespace := xsd.SchemaDependence{Filename: "__namespace.xsd", Schema: xsd.NamespaceSchema}

	// validate manifest using the schema
	commonNrl := xsd.SchemaDependence{Filename: "CommonNrl1-1.xsd", Schema: manifest.CommonNrl1_1Schema}
	dependencies := []*xsd.SchemaDependence{&commonNrl}
	if verbose {
		log.Info.Println("check if manifest is valid", filepath.Join(path, "Manifest.xml"))
	}
	if err := xsd.ValidateFile(filepath.Join(path, "Manifest.xml"), manifest.ModelManifestSchema, dependencies); err != nil {
		log.Error.Println("could not validate", filepath.Join(path, "Manifest.xml"), "->", err)
		foundErrorNb++
	}

	// Read files for current directory
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Error.Println("could read directory", path, "->", err)
		foundErrorNb++
	}

	// validate syntax of all xml files
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xsd") || strings.HasSuffix(f.Name(), ".xslt") || strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if xml syntax is valid", filepath.Join(path, f.Name()))
			}
			if doc := golibxml.ParseFile(filepath.Join(path, f.Name())); doc == nil {
				log.Error.Println("file", filepath.Join(path, f.Name()), "has no valid xml syntax")
				foundErrorNb++
			}
			golibxml.CleanupParser()
		}
	}

	// validate syntax of all schema file
	dependencies = []*xsd.SchemaDependence{&namespace}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xsd") {
			if verbose {
				log.Info.Println("check if schema is valid", filepath.Join(path, f.Name()))
			}
			if err := xsd.ValidateFile(filepath.Join(path, f.Name()), xsd.XSDSchema, dependencies); err != nil {
				log.Error.Println("could not validate", filepath.Join(path, f.Name()), "->", err)
				foundErrorNb++
			}
		}
	}

	// validate if directory name of rulegroup is identical with the rulegroup name
	// in the manifest
	if verbose {
		log.Info.Println("check if directory name of rule group is valid")
	}
	doc := xmlx.New()
	err = doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Println("Could not read XML file", filepath.Join(path, "Manifest.xml"))
		foundErrorNb++
	}
	modelName := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1", "modelManifest").As("", "name")
	cwd, _ := os.Getwd()
	_, dirName := filepath.Split(filepath.Dir(filepath.Clean(filepath.Join(cwd, path, "x.y"))))
	if modelName != dirName {
		log.Error.Println("Model name doesn't match with directory name ", "'"+dirName+"'")
		foundErrorNb++
	}

	// validate if all included files exist
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if all includes in XML file are valid", filepath.Join(path, f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XML file", filepath.Join(path, f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/2001/XInclude", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(localFileName(path, file)); err != nil {
						log.Error.Println("could not find the file '" + localFileName(path, file) + "', which is referenced in the file '" + filepath.Join(path, f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
		for _, f := range files {
			if strings.HasSuffix(f.Name(), ".xsd") {
				if verbose {
					log.Info.Println("check if all schema locations in the XSD file are valid", filepath.Join(path, f.Name()))
				}
				doc := xmlx.New()
				err = doc.LoadFile(filepath.Join(path, f.Name()), nil)
				if err != nil {
					log.Error.Println("Could not read XSD file", filepath.Join(path, f.Name()))
					foundErrorNb++
				}
				for _, x := range doc.SelectNodes("http://www.w3.org/2001/XMLSchema", "import") {
					file := x.As("", "schemaLocation")
					if file != "" {
						if _, err := os.Stat(localFileName(path, file)); err != nil {
							log.Error.Println("could not find the file '" + localFileName(path, file) + "', which is referenced in the file '" + filepath.Join(path, f.Name()+"'"))
							foundErrorNb++
						}
					}
				}
			}
		}
	}

	// validate if all files referenced in the manifest exist
	if verbose {
		log.Info.Println("check if all files referenced in the Manifest", filepath.Join(path, "Manifest.xml"), "exist")
	}
	doc = xmlx.New()
	err = doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Println("Could not read XML file", filepath.Join(path, "Manifest.xml"))
		foundErrorNb++
	}
	x := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1", "modelManifest")
	file := x.As("", "schema")
	if file != "" {
		if _, err := os.Stat(localFileName(path, file)); err != nil {
			log.Error.Println("could not find the file '" + localFileName(path, file) + "', which is referenced in the Manifest file '" + filepath.Join(path, "Manifest.xml") + "'")
			foundErrorNb++
		}
	}

	return foundErrorNb

}
