package model

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/markdown"

	"gopkg.in/yaml.v2"
)

// ModelManifest describes the format of the yaml file
type ModelManifest struct {
	ID           string
	Name         string
	Title        string
	Release      string
	ReleasedAt   string
	Subjects     []string
	Namespace    string
	Schema       string
	Creators     []string
	Descriptions []Description
	Targets      []Description
	Licenses     []Description
}

// Description is a multi lingual text
type Description struct {
	Lang string
	Text string
}

func modelManifest2XML(m *ModelManifest) []byte {
	xml := `<?xml version="1.0" encoding="UTF-8"?>
<modelManifest
  xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
`
	if m.ID != "" {
		xml += "\n  id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += "\n  name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += "\n  title=\"" + m.Title + `"`
	}
	if m.ReleasedAt != "" {
		xml += "\n  releasedAt=\"" + m.ReleasedAt + `"`
	}
	if m.Namespace != "" {
		xml += "\n  namespace=\"" + m.Namespace + `"`
	}
	if m.Schema != "" {
		xml += "\n  schema=\"" + m.Schema + `"`
	}
	xml += "\n>"
	if m.Release != "" {
		xml += "\n  <release>" + m.Release + "</release>"
	}
	for _, x := range m.Subjects {
		xml += "\n  <subjects>" + x + "</subjects>"
	}
	for _, x := range m.Creators {
		xml += "\n  <creators>" + x + "</creators>"
	}
	for _, x := range m.Descriptions {
		xml += "\n  <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	for _, x := range m.Targets {
		xml += "\n  <targets lang=\"" + x.Lang + "\">" + x.Text + "</targets>"
	}
	for _, x := range m.Licenses {
		xml += "\n  <licenses lang=\"" + x.Lang + "\">" + x.Text + "</licenses>"
	}
	xml += "\n</modelManifest>"
	return []byte(xml)
}

// Build read all files from sources and convert them
func Build(path string, verbose bool) {
	// Generate Manifest.xml from yaml sources if file in sources is newer
	var sourceStat os.FileInfo
	data, err := ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.lityaml"))
	if err != nil {
		data, err = ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.yaml"))
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.yaml"))
	} else {
		data = markdown.Filter(data)
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.lityaml"))
	}
	if err == nil {
		targetStat, err := os.Stat(filepath.Join(path, "Manifest.xml"))
		if err != nil || sourceStat.ModTime().After(targetStat.ModTime()) {
			if verbose {
				log.Info.Println("create Manifest.xml from", filepath.Join(path, "sources"))
			}
			var m *ModelManifest
			err = yaml.Unmarshal(data, &m)
			if err == nil {
				err = ioutil.WriteFile(filepath.Join(path, "Manifest.xml"), modelManifest2XML(m), 0600)
				if err != nil {
					log.Error.Fatalln("Couldn't write Manifest.xml in", path, "because", err)
				}
			} else {
				log.Error.Fatalln("Couldn't read Manifest in", filepath.Join(path, "sources"), "because", err)
			}
		}
	}

	// Compile all domain models with the P23R Model Compiler
	_, err = exec.Command("which", "modelc").Output()
	if err == nil {
		// Read files from directory sources
		files, err := ioutil.ReadDir(filepath.Join(path, "sources"))
		if err != nil {
			log.Error.Fatalln("could read directory", filepath.Join(path, "sources"), "->", err)
		}
		for _, f := range files {
			if strings.HasSuffix(f.Name(), ".model") {
				source := filepath.Join(path, "sources", f.Name())
				sourceStat, _ := os.Stat(source)
				target := filepath.Join(path, strings.Split(f.Name(), ".")[0]+".xsd")
				targetStat, targetErr := os.Stat(target)
				if targetErr != nil || sourceStat.ModTime().After(targetStat.ModTime()) {
					if verbose {
						log.Info.Println("create XSD schema from model file", filepath.Join(path, f.Name()))
					}
					if _, err := exec.Command("modelc", "xsd", source, target).Output(); err != nil {
						log.Error.Fatalln("could not translate", filepath.Join(path, "sources", f.Name()))
					}
				}
			}
		}
	}
}
