package rulegroup

import (
	"os"
	"path/filepath"
	"text/template"

	"github.com/odeke-em/go-uuid"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/profile"
)

// Parameter contains the context values for the content
type Parameter struct {
	RuleGroup, Step string
}

func valueOrDefault(value string, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

func createFile(path string, fileName string, parameter *Parameter, content string, prof *profile.Profile) {
	type Mapping struct {
		Firstname, Lastname, Organisation, RuleGroup, Domain, Section, UUID string
	}
	var mapping *Mapping
	var err error
	mapping = &Mapping{
		Firstname:    valueOrDefault(prof.Firstname, "«your firstname»"),
		Lastname:     valueOrDefault(prof.Lastname, "«your lastname»"),
		Organisation: valueOrDefault(prof.Organisation, "«your organisation»"),
		Domain:       valueOrDefault(prof.Domain, "«your domain»"),
		Section:      valueOrDefault(prof.Section, "«section»"),
		RuleGroup:    valueOrDefault(parameter.RuleGroup, "«rule group»"),
		UUID:         uuid.New(),
	}
	t := template.Must(template.New("file").Parse(content))
	var f *os.File
	f, err = os.Create(filepath.Join(path, fileName))
	defer f.Close()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	err = t.Execute(f, mapping)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")
}

// GenerateTemplate generates all directories and files nescessary for a P23R
// notification rule group.
//
func GenerateTemplate(name string, isSubproject bool, createSources bool) {
	// create directory if is subproject
	var path string
	if isSubproject {
		err := os.Mkdir(name, os.ModeDir|0700)
		if err != nil {
			println("F")
			log.Error.Fatal(err)
		}
		print(".")
		path = name
	} else {
		path = "."
	}

	// Read profile
	var prof *profile.Profile
	var err error
	prof, err = profile.ReadProfile()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}

	// create Manifest XML file for rule group
	createFile(path, "Manifest.xml", &Parameter{RuleGroup: name}, ruleGroupManifestXML, prof)

	// create Recommendation files for rule group
	createFile(path, "Recommendation.dats", &Parameter{RuleGroup: name}, recommendationDats, prof)
	createFile(path, "Recommendation.xslt", &Parameter{RuleGroup: name}, recommendationXslt, prof)

	// create Configuration files for rule group
	createFile(path, "Configuration.dats", &Parameter{RuleGroup: name}, configurationDats, prof)
	createFile(path, "Configuration.xslt", &Parameter{RuleGroup: name}, configurationXslt, prof)

	// Create directories
	err = os.Mkdir(filepath.Join(path, "shared"), os.ModeDir|0700)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")
	err = os.Mkdir(filepath.Join(path, "shared", "sources"), os.ModeDir|0700)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")
	err = os.Mkdir(filepath.Join(path, "sources"), os.ModeDir|0700)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")

	// create Manifest YAML file for rule group
	if createSources {
		print("S")
		// Create filter to sign the mandatory content
		createFile(filepath.Join(path, "sources"), "Manifest.lityaml", &Parameter{RuleGroup: name}, ruleGroupManifestLitYaml, prof)
	}
}

const configurationDats = `

Configuration.dats

Pfad:		 «Rule Package Name»/0.1/{{.RuleGroup}}/
Präsenz:	optional
Referenz: T-BRS Kapitel 5 / Tabelle 32
Inhalt:	 P23R Selection Skript zur Konfiguration von persistenten Standardwerten für eine Benachrichtigunsregelgruppe

	Model «Model Shortcut» = http://{{.Domain}}/NS/{{.Section}}/«model Name»1-0

Your elements, attributes and queries

	configuration {
		@namespace { "http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/configuration" }

	}
`

const configurationXslt = `<?xml version="1.0" encoding="UTF-8"?>
<!--

	Configuration.xslt

	Pfad:		 «Rule Package Name»/0.1/{{.RuleGroup}}/
	Präsenz:	optional
	Referenz: T-BRS Kapitel 5 / Tabelle 32
	Inhalt:	 Skript zur Konfiguration von persistenten Standardwerten einer Benachrichtigungsregelgruppe

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
`

const recommendationDats = `

Recommendation.dats

Pfad:		 «Rule Package Name»/0.1/{{.RuleGroup}}/
Präsenz:	verbindlich
Referenz: T-BRS Kapitel 5 / Tabelle 32
Inhalt:	 P23R Selection Skript zur Empfehlung einer Benachrichtigungsregelgruppe

	recommendationResult {
		@namespace { "http://leitstelle.p23r.de/NS/p23r/nrl/RecommendationResult1-1" }
		@recommendation { "optional" }
	}
`

const recommendationXslt = `<?xml version="1.0" encoding="UTF-8"?>
<!--

	Recommendation.xslt

	Pfad:		 «Rule Package Name»/0.1/{{.RuleGroup}}/
	Präsenz:	verbindlich
	Referenz: T-BRS Kapitel 5 / Tabelle 32
	Inhalt:	 Skript zur Empfehlung einer Benachrichtigungsregelgruppe

	The recommendation should unrecommended, optional, recommended or mandatory

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="/">
		<recommendationResult
			xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/RecommendationResult1-1"
			recommendation="optional"
		/>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>`

const ruleGroupManifestXML = `<?xml version="1.0" encoding="UTF-8"?>
<ruleGroupManifest

	xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

	id="{{.UUID}}"
	name="{{.RuleGroup}}"
	releasedAt="2015-01-01T12:00:00"
	validFrom="2015-01-01T00:00:00" >
	<release>0.1</release>
	<creators>{{.Firstname}} {{.Lastname}} ({{.Organisation}})</creators>
	<auditors>«Vorname» «Nachname»</auditors>
	<auditBys>«Organisation»</auditBys>
	<descriptions lang="de-DE">--</descriptions>
	<targets lang="de-DE">--</targets>
	<legalBackgrounds lang="de-DE">--</legalBackgrounds>
	<licenses lang="de-DE">--</licenses>

</ruleGroupManifest>
`

const ruleGroupManifestLitYaml = `	xmlns: http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1

This rule group ...

Basic information about the rule group

	id: {{.UUID}}
	name: {{.RuleGroup}}
	releasedat: 2015-01-01T12:00:00
	validfrom: 2015-01-01T00:00:00
	release: 0.1

Keywords for a search portal and labels used as filter for packaging

	subjects:
		- rulegroup
		- Benachrichtigungsregelgruppe

List of authors and quatility assurance staff

	creators:
		- {{.Firstname}} {{.Lastname}} ({{.Organisation}})
	auditors:
		- «Vorname» «Nachname»
	auditbys:
		- «Organisation»

Textual descriptions in various languages for the people, which
decides about the activation of a rule group.

	descriptions:
		- lang: de-DE
			text: |
				--

	targets:
		- lang: de-DE
			text: |
				--

	legalbackgrounds:
		- lang: de-DE
			text: |
				--

	licenses:
		- lang: de-DE
			text: |
				--
`
