package rulegroup

import (
	"io/ioutil"
	"path/filepath"
	"strings"

	"gitlab.com/openp23r/p23r-common/log"

	"github.com/jteeuwen/go-pkg-xmlx"
	"gitlab.com/openp23r/p23r-tool/manifest"
)

func getRuleGroups(dir string, verbose bool) []string {
	ruleGroupPaths := []string{}
	manifestType := manifest.GetManifestType(dir, verbose)
	var dirs []string
	if manifestType != manifest.NoManifestID && manifestType == manifest.RuleGroupManifestID {
		dirs = []string{dir}

	} else {
		xs, err := ioutil.ReadDir(dir)
		dirs = []string{}
		if err == nil {
			for _, x := range xs {
				if x.IsDir() {
					dirs = append(dirs, x.Name())

				}
			}
		}
	}
	for _, x := range dirs {
		manifestType = manifest.GetManifestType(x, verbose)

		if manifestType == manifest.RuleGroupManifestID {
			ruleGroupPaths = append(ruleGroupPaths, x)
		} else {
			if strings.HasPrefix(x, "/") {
				ruleGroupPaths = append(ruleGroupPaths, getRuleGroups(x, verbose)...)
			} else {
				ruleGroupPaths = append(ruleGroupPaths, getRuleGroups(filepath.Join(dir, x), verbose)...)
			}
		}
	}

	return ruleGroupPaths
}

func getIDFromRuleGroups(dir string) string {
	data, err := ioutil.ReadFile(filepath.Join(dir, "Manifest.xml"))
	if err != nil {
		log.Error.Fatalln("Could not read Manifest file", filepath.Join(dir, "Manifest.xml"))
	}
	doc := xmlx.New()
	err = doc.LoadString(string(data), nil)
	if err != nil {
		log.Error.Fatalln("Could not parse Manifest file", filepath.Join(dir, "Manifest.xml"))
	}
	node := doc.SelectNode("*", "ruleGroupManifest")
	if node == nil {
		log.Error.Fatalln("Could not read ID from Manifest file", filepath.Join(dir, "Manifest.xml"))
	}
	return node.As("", "id")
}

// GetRuleGroupIDs collect all ids of the avalaible rule groups in a notification rule package
func GetRuleGroupIDs(dir string, verbose bool) []string {

	dirs := getRuleGroups(dir, verbose)
	ids := []string{}
	for _, x := range dirs {
		id := getIDFromRuleGroups(x)
		ids = append(ids, id)
	}
	return ids
}
