package rulegroup

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/jbussdieker/golibxml"
	"github.com/jteeuwen/go-pkg-xmlx"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/dats"
	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/rule"
	"gitlab.com/openp23r/p23r-tool/xsd"
)

func localFileName(basepath string, relpath string, name string) string {
	if strings.HasPrefix(name, "rule:///") {
		return filepath.Join(basepath, "..", name[8:])
	}
	if strings.HasPrefix(name, "rule://") {
		return filepath.Join(basepath, "..", "..", "..", name[7:])
	}
	return filepath.Join(basepath, relpath, name)
}

func localSchemaFileName(basepath string, relpath string, name string) string {
	if strings.HasPrefix(name, "rule:///") {
		name = filepath.Join(basepath, "..", name[8:])
	}
	if strings.HasPrefix(name, "rule://") {
		return filepath.Join(basepath, "..", "..", "..", name[7:])
	}
	if strings.HasPrefix(name, "model:///") {
		log.Error.Println("referencing with model:///... is not permitted")
	}
	if strings.HasPrefix(name, "model://") {
		return filepath.Join(basepath, "..", "..", "..", name[8:])
	}
	return filepath.Join(basepath, relpath, name)
}

// Validate do
func Validate(path string, verbose bool) int {
	foundErrorNb := 0
	if verbose {
		log.Info.Println("validate rule group " + path)
	}

	// validate if all mandatory files exist
	if verbose {
		log.Info.Println("check if all mandatory files exist")
	}
	if _, err := os.Stat(filepath.Join(path, "Manifest.xml")); err != nil {
		log.Error.Println("could not find the manifest file (Manifest.xml) in " + path)
		foundErrorNb++
	}

	_, recommendationErr1 := os.Stat(filepath.Join(path, "Recommendation.dats"))
	_, recommendationErr2 := os.Stat(filepath.Join(path, "Recommendation.xslt"))
	if (recommendationErr1 != nil) && (recommendationErr2 != nil) {
		log.Error.Println("could not find a recommendation script (Recommendation.dats / Recommendation.xslt) in " + path)
		foundErrorNb++
	}

	// dependences for schema validation
	namespace := xsd.SchemaDependence{Filename: "__namespace.xsd", Schema: xsd.NamespaceSchema}
	xmlSchema := xsd.SchemaDependence{Filename: "__XMLSchema.xsd", Schema: xsd.XMLSchema}

	// validate manifest using the schema
	commonNrl := xsd.SchemaDependence{Filename: "CommonNrl1-1.xsd", Schema: manifest.CommonNrl1_1Schema}
	dependencies := []*xsd.SchemaDependence{&commonNrl}
	if verbose {
		log.Info.Println("check if manifest is valid", filepath.Join(path, "Manifest.xml"))
	}
	if err := xsd.ValidateFile(filepath.Join(path, "Manifest.xml"), manifest.RuleGroupManifestSchema, dependencies); err != nil {
		log.Error.Println("could not validate", filepath.Join(path, "Manifest.xml"), "->", err)
		foundErrorNb++
	}

	// Read files for current directory
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Error.Println("could read directory", path, "->", err)
		foundErrorNb++
	}

	// Read files for shared directory and extend the files list
	sharedfiles, err := ioutil.ReadDir(filepath.Join(path, "shared"))
	if err == nil {
		files = append(files, sharedfiles...)
	}

	// validate syntax of all xml files
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xsd") || strings.HasSuffix(f.Name(), ".xslt") || strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if xml syntax is valid ", filepath.Join(path, f.Name()))
			}
			if doc := golibxml.ParseFile(filepath.Join(path, f.Name())); doc == nil {
				log.Error.Println("file", filepath.Join(path, f.Name()), "has no valid xml syntax")
				foundErrorNb++
			}
			golibxml.CleanupParser()
		}
	}

	// validate syntax of all schema file
	dependencies = []*xsd.SchemaDependence{&namespace}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xsd") {
			if verbose {
				log.Info.Println("check if schema file is valid", filepath.Join(path, f.Name()))
			}
			if err := xsd.ValidateFile(filepath.Join(path, f.Name()), xsd.XSDSchema, dependencies); err != nil {
				log.Error.Println("could not validate", filepath.Join(path, f.Name()), "->", err)
				foundErrorNb++
			}
		}
	}

	// validate syntax of all xslt files
	dependencies = []*xsd.SchemaDependence{&namespace, &xmlSchema}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xslt") {
			if verbose {
				log.Info.Println("check if xslt file is valid", filepath.Join(path, f.Name()))
			}
			if err := xsd.ValidateFile(filepath.Join(path, f.Name()), xsd.XSLTSchema, dependencies); err != nil {
				log.Error.Println("could not validate", filepath.Join(path, f.Name()), "->", err)
				foundErrorNb++
			}
		}
	}

	// validate syntax of all dats files
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".dats") {
			if verbose {
				log.Info.Println("check if P23R selection file is valid", filepath.Join(path, f.Name()))
			}
			if err := dats.ParseDatsFile(filepath.Join(path, f.Name())); err != nil {
				log.Error.Println("could not validate", filepath.Join(path, f.Name()), "->", err)
				foundErrorNb++
			}
		}
	}

	// validate if all included files exist

	// validate if directory name of rulegroup is identical with the rulegroup name
	// in the manifest
	if verbose {
		log.Info.Println("check if directory name of rule group is valid")
	}
	doc := xmlx.New()
	err = doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Println("Could not read XML file", filepath.Join(path, "Manifest.xml"))
		foundErrorNb++
	}
	ruleGroupName := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1", "ruleGroupManifest").As("", "name")
	cwd, _ := os.Getwd()
	_, dirName := filepath.Split(filepath.Dir(filepath.Clean(filepath.Join(cwd, path, "x.y"))))
	if ruleGroupName != dirName {
		log.Error.Println("Rule group name doesn't match with directory name ", "'"+dirName+"'")
		foundErrorNb++
	}

	// Read files for current directory
	files, err = ioutil.ReadDir(path)
	if err != nil {
		log.Error.Println("could read directory", path, "->", err)
		foundErrorNb++
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if all includes in XML file are valid", filepath.Join(path, f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XML file", filepath.Join(path, f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/2001/XInclude", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(localFileName(path, "", file)); err != nil {
						log.Error.Println("could not find the file '" + localFileName(path, "", file) + "', which is referenced in the file '" + filepath.Join(path, f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xslt") {
			if verbose {
				log.Info.Println("check if all includes in the XSLT file are valid", filepath.Join(path, f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XSLT file", filepath.Join(path, f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/1999/XSL/Transform", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(localFileName(path, "", file)); err != nil {
						log.Error.Println("could not find the file '" + localFileName(path, "", file) + "', which is referenced in the file '" + filepath.Join(path, f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xsd") {
			if verbose {
				log.Info.Println("check if all schema locations in the XSD file are valid", filepath.Join(path, f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XSD file", filepath.Join(path, f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/2001/XMLSchema", "import") {
				file := x.As("", "schemaLocation")
				if file != "" {
					if _, err := os.Stat(localSchemaFileName(path, "", file)); err != nil {
						log.Error.Println("could not find the file '" + localSchemaFileName(path, "", file) + "', which is referenced in the file '" + filepath.Join(path, f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}

	// Read files for shared directory
	files, err = ioutil.ReadDir(filepath.Join(path, "shared"))

	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if all includes in XML file are valid", filepath.Join(path, "shared", f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, "shared", f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XML file", filepath.Join(path, "shared", f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/2001/XInclude", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(localFileName(path, "shared", file)); err != nil {
						log.Error.Println("could not find the file '" + localFileName(path, "shared", file) + "', which is referenced in the file '" + filepath.Join(path, "shared", f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xslt") {
			if verbose {
				log.Info.Println("check if all includes in the XSLT file are valid", filepath.Join(path, "shared", f.Name()))
				foundErrorNb++
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, "shared", f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XSLT file", filepath.Join(path, "shared", f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/1999/XSL/Transform", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(localFileName(path, "shared", file)); err != nil {
						log.Error.Println("could not find the file '" + localFileName(path, "shared", file) + "', which is referenced in the file '" + filepath.Join(path, "shared", f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xsd") {
			if verbose {
				log.Info.Println("check if all schema locations in the XSD file are valid", filepath.Join(path, "shared", f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, "shared", f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XSD file", filepath.Join(path, "shared", f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/2001/XMLSchema", "import") {
				file := x.As("", "schemaLocation")
				if file != "" {
					if _, err := os.Stat(localSchemaFileName(path, "shared", file)); err != nil {
						log.Error.Println("could not find the file '" + localSchemaFileName(path, "shared", file) + "', which is referenced in the file '" + filepath.Join(path, "shared", f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}

	// validate all sub directories with a manifest inside
	xs, err := ioutil.ReadDir(path)
	dirs := []string{}
	if err == nil {
		for _, x := range xs {
			if x.IsDir() {
				dirs = append(dirs, x.Name())
			}
		}
	} else {
		log.Error.Printf("Can't read directory %v", path)
		foundErrorNb++
	}
	for _, x := range dirs {
		manifestType := manifest.GetManifestType(filepath.Join(path, x), verbose)
		switch manifestType {
		case manifest.RuleManifestID:
			rule.Validate(filepath.Join(path, x), verbose)
		case manifest.NoManifestID:
		case manifest.UnknownManifestID:
			log.Warning.Printf("unknown kind of manifest in '%s'", filepath.Join(path, x))
		case manifest.RuleGroupManifestID:
			log.Error.Println("a rule group may not contain other rule groups")
			foundErrorNb++
		case manifest.TestCaseManifestID:
			log.Error.Println("a rule group may not contain test cases")
			foundErrorNb++
		case manifest.TestSetManifestID:
			log.Error.Println("a rule group may not contain test sets")
			foundErrorNb++
		case manifest.ModelManifestID:
			log.Error.Println("a rule group may not contain pivot data models")
			foundErrorNb++
		default:
			log.Error.Printf("a rule group may only contain rules and not '%s'", filepath.Join(path, x))
			foundErrorNb++
		}
	}

	return foundErrorNb
}
