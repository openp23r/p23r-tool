package rulegroup

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/markdown"
	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/rule"

	"gopkg.in/yaml.v2"
)

// Manifest describes the format of the yaml file
type Manifest struct {
	ID                      string
	Name                    string
	Title                   string
	Release                 string
	ReleasedAt              string
	SourceLanguages         []SourceLanguage
	ValidFrom               string
	ValidUntil              string
	Subjects                []string
	Conflicts               []string
	RecommendationNSs       []string
	ConfigurationRequireNSs []string
	Creators                []string
	Auditors                []string
	AuditBys                []string
	Descriptions            []Description
	Targets                 []Description
	Hints                   []Description
	LegalBackgrounds        []Description
	Licenses                []Description
	Variants                []Variant
	Policies                []Policy
}

// Description is a multi lingual text
type Description struct {
	Lang string
	Text string
}

// SourceLanguage describes the used tools
type SourceLanguage struct {
	Name     string
	Compiler string
}

// Variant describes rule variants
type Variant struct {
	ID           string
	Descriptions []Description
	Choices      []Choice
}

// Choice describes
type Choice struct {
	ID           string
	IsDefault    string
	Rules        []string
	Descriptions []Description
}

// Policy describes
type Policy struct {
	Label         string
	IsImplemented string
}

func ruleGroupManifest2XML(m *Manifest) []byte {
	xml := `<?xml version="1.0" encoding="UTF-8"?>
<ruleGroupManifest
  xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
`
	if m.ID != "" {
		xml += "\n  id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += "\n  name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += "\n  title=\"" + m.Title + `"`
	}
	if m.ReleasedAt != "" {
		xml += "\n  releasedAt=\"" + m.ReleasedAt + `"`
	}
	if m.ValidFrom != "" {
		xml += "\n  validFrom=\"" + m.ValidFrom + `"`
	}
	if m.ValidUntil != "" {
		xml += "\n  validUntil=\"" + m.ValidUntil + `"`
	}
	xml += "\n>"
	if m.Release != "" {
		xml += "\n  <release>" + m.Release + "</release>"
	}
	for _, x := range m.SourceLanguages {
		xml += "\n  <sourceLanguages name=\"" + x.Name + "\" compiler=\"" + x.Compiler + "\" />"
	}
	for _, x := range m.Subjects {
		xml += "\n  <subjects>" + x + "</subjects>"
	}
	for _, x := range m.Conflicts {
		xml += "\n  <conflicts>" + x + "</conflicts>"
	}
	for _, x := range m.RecommendationNSs {
		xml += "\n  <recommendationNSs>" + x + "</recommendationNSs>"
	}
	for _, x := range m.ConfigurationRequireNSs {
		xml += "\n  <configurationRequireNSs>" + x + "</configurationRequireNSs>"
	}
	for _, x := range m.Creators {
		xml += "\n  <creators>" + x + "</creators>"
	}
	for _, x := range m.Auditors {
		xml += "\n  <auditors>" + x + "</auditors>"
	}
	for _, x := range m.AuditBys {
		xml += "\n  <auditBys>" + x + "</auditBys>"
	}
	for _, x := range m.Descriptions {
		xml += "\n  <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	for _, x := range m.Targets {
		xml += "\n  <targets lang=\"" + x.Lang + "\">" + x.Text + "</targets>"
	}
	for _, x := range m.Hints {
		xml += "\n  <hints lang=\"" + x.Lang + "\">" + x.Text + "</hints>"
	}
	for _, x := range m.LegalBackgrounds {
		xml += "\n  <legalBackgrounds lang=\"" + x.Lang + "\">" + x.Text + "</legalBackgrounds>"
	}
	for _, x := range m.Licenses {
		xml += "\n  <licenses lang=\"" + x.Lang + "\">" + x.Text + "</licenses>"
	}
	for _, x := range m.Variants {
		xml += "\n  <variants id=\"" + x.ID + "\">"
		for _, y := range x.Descriptions {
			xml += "\n    <descriptions lang=\"" + y.Lang + "\">" + y.Text + "</descriptions>"
		}
		for _, y := range x.Choices {
			xml += "\n    <choices id=\"" + y.ID + "\" isDefault=\"" + y.IsDefault + "\" >"
			for _, z := range y.Rules {
				xml += "\n  <rules>" + z + "</rules>"
			}
			for _, z := range y.Descriptions {
				xml += "\n  <descriptions lang=\"" + z.Lang + "\">" + z.Text + "</descriptions>"
			}
			xml += "\n</choices>"
		}
		xml += "\n  </variants>"
	}
	for _, x := range m.Policies {
		xml += "\n  <policies label=\"" + x.Label + "\" isImplemented=\"" + x.IsImplemented + "\"/>"
	}
	xml += "\n</ruleGroupManifest>"
	return []byte(xml)
}

// Build read all files from sources and convert them
func Build(path string, verbose bool) {
	// Generate Manifest.xml from yaml sources if file in sources is newer
	var sourceStat os.FileInfo
	data, err := ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.lityaml"))
	if err != nil {
		data, err = ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.yaml"))
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.yaml"))
	} else {
		data = markdown.Filter(data)
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.lityaml"))
	}
	if err == nil {
		target := filepath.Join(path, "Manifest.xml")
		targetStat, err := os.Stat(target)
		if err != nil || sourceStat.ModTime().After(targetStat.ModTime()) {
			if verbose {
				log.Info.Println("create Manifest.xml from", filepath.Join(path, "sources"))
			}
			var m *Manifest
			err = yaml.Unmarshal(data, &m)
			if err == nil {
				err = ioutil.WriteFile(filepath.Join(path, "Manifest.xml"), ruleGroupManifest2XML(m), 0600)
				if err != nil {
					log.Error.Fatalln("Couldn't write Manifest.xml in", path, "because", err)
				}
			} else {
				log.Error.Fatalln("Couldn't read Manifest in", filepath.Join(path, "sources"), "because", err)
			}
		} else {
			if verbose {
				log.Info.Printf("Manifest %s is up-to-date", target)
			}
		}
	}

	// build all sub directories with a manifest inside
	xs, err := ioutil.ReadDir(path)
	dirs := []string{}
	if err == nil {
		for _, x := range xs {
			if x.IsDir() {
				dirs = append(dirs, x.Name())
			}
		}
	} else {
		log.Error.Fatalf("Can't read directory %v", path)
	}
	for _, x := range dirs {
		manifestType := manifest.GetManifestType(filepath.Join(path, x), verbose)
		switch manifestType {
		case manifest.RuleManifestID:
			rule.Build(filepath.Join(path, x), verbose)
		case manifest.NoManifestID:
		case manifest.UnknownManifestID:
			log.Warning.Printf("unknown kind of manifest in '%s'", filepath.Join(path, x))
		case manifest.RuleGroupManifestID:
			log.Warning.Println("a rule group may not contain other rule groups")
		case manifest.TestCaseManifestID:
			log.Warning.Println("a rule group may not contain test cases")
		case manifest.TestSetManifestID:
			log.Warning.Println("a rule group may not contain test sets")
		case manifest.ModelManifestID:
			log.Warning.Println("a rule group may not contain pivot data models")
		default:
			log.Warning.Printf("a rule group may only contain rules and not '%s'", filepath.Join(path, x))
		}
	}
}
