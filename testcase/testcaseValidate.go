package testcase

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/jbussdieker/golibxml"
	"github.com/jteeuwen/go-pkg-xmlx"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/manifest"
	"gitlab.com/openp23r/p23r-tool/xsd"
)

// Validate do
func Validate(path string, verbose bool) int {
	foundErrorNb := 0
	if verbose {
		log.Info.Println("validate test case", path)
	}

	// validate if all mandatory files exist
	if verbose {
		log.Info.Println("check if all mandatory files exist")
	}
	if _, err := os.Stat(filepath.Join(path, "Manifest.xml")); err != nil {
		log.Error.Println("could not find the manifest file (Manifest.xml) in " + path)
		foundErrorNb++
	}
	if _, err := os.Stat(filepath.Join(path, "Message.xml")); err != nil {
		log.Error.Println("could not find the message file (Message.xml) in " + path)
		foundErrorNb++
	}

	// dependences for schema validation
	namespace := xsd.SchemaDependence{Filename: "__namespace.xsd", Schema: xsd.NamespaceSchema}
	xmlSchema := xsd.SchemaDependence{Filename: "__XMLSchema.xsd", Schema: xsd.XMLSchema}

	// validate manifest using the schema
	common := xsd.SchemaDependence{Filename: "Common1-0.xsd", Schema: manifest.Common1_0Schema}
	dependencies := []*xsd.SchemaDependence{&common}
	if verbose {
		log.Info.Println("check if manifest is valid", filepath.Join(path, "Manifest.xml"))
	}
	if err := xsd.ValidateFile(filepath.Join(path, "Manifest.xml"), manifest.TestCaseManifestSchema, dependencies); err != nil {
		log.Error.Println("could not validate", filepath.Join(path, "Manifest.xml"), "->", err)
		foundErrorNb++
	}

	// validate if directory name of test case is identical with the test case name
	// in the manifest
	if verbose {
		log.Info.Println("check if directory name of test case is valid")
	}
	doc := xmlx.New()
	err := doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Println("Could not read XML file", filepath.Join(path, "Manifest.xml"))
		foundErrorNb++
	}
	testCaseName := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/TestCaseManifest1-0", "testCaseManifest").As("", "name")
	cwd, _ := os.Getwd()
	_, dirName := filepath.Split(filepath.Dir(filepath.Clean(filepath.Join(cwd, path, "x.y"))))
	if testCaseName != dirName {
		log.Error.Println("Test case name doesn't match with directory name ", "'"+dirName+"'")
		foundErrorNb++
	}

	// Read files for current directory
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Error.Println("could read directory", path, "->", err)
		foundErrorNb++
	}

	// Read files for data directory and extend the files list
	datafiles, err := ioutil.ReadDir(filepath.Join(path, "data"))
	if err == nil {
		files = append(files, datafiles...)
	}

	// validate syntax of all xml files
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xsd") || strings.HasSuffix(f.Name(), ".xslt") || strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if xml syntax is valid", filepath.Join(path, f.Name()))
			}
			if doc := golibxml.ParseFile(filepath.Join(path, f.Name())); doc == nil {
				log.Error.Println("file", filepath.Join(path, f.Name()), "has no valid xml syntax")
				foundErrorNb++
			}
			golibxml.CleanupParser()
		}
	}

	// validate syntax of all xslt files
	dependencies = []*xsd.SchemaDependence{&namespace, &xmlSchema}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xslt") {
			if verbose {
				log.Info.Println("check if xslt file is valid", filepath.Join(path, f.Name()))
			}
			if err := xsd.ValidateFile(filepath.Join(path, f.Name()), xsd.XSLTSchema, dependencies); err != nil {
				log.Error.Println("could not validate", filepath.Join(path, f.Name()), "->", err)
				foundErrorNb++
			}
		}
	}

	// TODO validate syntax of pfx signature file

	// validate if all included files exist
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xml") {
			if verbose {
				log.Info.Println("check if P23R selection file is valid", filepath.Join(path, f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XML file", filepath.Join(path, f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/2001/XInclude", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(filepath.Join(path, file)); err != nil {
						log.Error.Println("could not find the file '" + filepath.Join(path, file) + "', which is referenced in the file '" + filepath.Join(path, f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xslt") {
			if verbose {
				log.Info.Println("check if P23R selection file is valid", filepath.Join(path, f.Name()))
			}
			doc := xmlx.New()
			err = doc.LoadFile(filepath.Join(path, f.Name()), nil)
			if err != nil {
				log.Error.Println("Could not read XSLT file", filepath.Join(path, f.Name()))
				foundErrorNb++
			}
			for _, x := range doc.SelectNodes("http://www.w3.org/1999/XSL/Transform", "include") {
				file := x.As("", "href")
				if file != "" {
					if _, err := os.Stat(filepath.Join(path, file)); err != nil {
						log.Error.Println("could not find the file '" + filepath.Join(path, file) + "', which is referenced in the file '" + filepath.Join(path, f.Name()+"'"))
						foundErrorNb++
					}
				}
			}
		}
	}

	return foundErrorNb

}
