package testcase

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"text/template"

	"github.com/codegangsta/cli"
	"github.com/jteeuwen/go-pkg-xmlx"
	"github.com/odeke-em/go-uuid"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/profile"
)

// Parameter contains the context values for the content
type Parameter struct {
	TestCase, TestSet, Rule, RuleGroup string
}

func valueOrDefault(value string, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

func createFile(path string, fileName string, parameter *Parameter, content string, prof *profile.Profile) {
	type Mapping struct {
		Firstname, Lastname, Organisation, TestCase, TestSet, Rule, RuleGroup, Step, Domain, Section, UUID, TESTUUID string
	}
	var mapping *Mapping
	var err error
	mapping = &Mapping{
		Firstname:    valueOrDefault(prof.Firstname, "--your-firstname--"),
		Lastname:     valueOrDefault(prof.Lastname, "--your-lastname--"),
		Organisation: valueOrDefault(prof.Organisation, "--your-organisation--"),
		Domain:       valueOrDefault(prof.Domain, "--your-domain--"),
		Section:      valueOrDefault(prof.Section, "--section--"),
		TestCase:     valueOrDefault(parameter.TestCase, "--test-case--"),
		TestSet:      valueOrDefault(parameter.TestSet, "--test-set--"),
		Rule:         valueOrDefault(parameter.Rule, "«zu testende Benachrichtigungsregel»"),
		RuleGroup:    valueOrDefault(parameter.RuleGroup, "«zu testende Benachrichtigungsregelgruppe»"),
		UUID:         uuid.New(),
		TESTUUID:     "{{.TESTUUID}}",
	}
	t := template.Must(template.New("file").Parse(content))
	var f *os.File
	f, err = os.Create(filepath.Join(path, fileName))
	defer f.Close()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	err = t.Execute(f, mapping)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")
}

func getTestset() (bool, string) {
	data, err := ioutil.ReadFile("Manifest.xml")
	subproject := true
	if err != nil {
		data, err = ioutil.ReadFile("../Manifest.xml")
		subproject = false
	}
	if err != nil {
		log.Error.Fatal("No manifest file found")
		return false, ""
	}
	doc := xmlx.New()
	err = doc.LoadString(string(data), nil)
	if err != nil {
		log.Error.Fatal(err)
		return false, ""
	}
	node := doc.SelectNode("*", "testSetManifest")
	if node == nil {
		return false, ""
	}
	return subproject, node.As("*", "name")
}

// GenerateTemplate generates all directories and files nescessary for a P23R
// test case.
//
func GenerateTemplate(name string, c *cli.Context, createSources bool) {
	isSubproject, testSet := getTestset()
	if testSet == "" {
		log.Error.Fatal("Test case must be created inside a test set")
		return
	}
	rule := c.String("rule")
	ruleGroup := c.String("rulegroup")

	// create directory if is subproject
	var path string
	if isSubproject {
		err := os.Mkdir(name, os.ModeDir|0700)
		if err != nil {
			println("F")
			log.Error.Fatal(err)
		}
		print(".")
		path = name
	} else {
		path = "."
	}

	// Read profile
	var prof *profile.Profile
	var err error
	prof, err = profile.ReadProfile()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}

	// create Manifest file for rule
	createFile(path, "Manifest.xml", &Parameter{TestCase: name, TestSet: testSet, Rule: rule, RuleGroup: ruleGroup}, testCaseManifestXML, prof)

	// create test message for test case
	createFile(path, "Message.xml", &Parameter{TestCase: name, TestSet: testSet, Rule: rule, RuleGroup: ruleGroup}, testCaseMessage, prof)

	// Create final notification schema
	createFile(path, "Notification-001.xml", &Parameter{TestCase: name, TestSet: testSet, Rule: rule, RuleGroup: ruleGroup}, notificationXML, prof)

	// Create directories
	err = os.Mkdir(filepath.Join(path, "data"), os.ModeDir|0700)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")
	err = os.Mkdir(filepath.Join(path, "sources"), os.ModeDir|0700)
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}
	print(".")

	if createSources {
		print("S")

		// Create Manifest content
		createFile(filepath.Join(path, "sources"), "Manifest.lityaml", &Parameter{TestCase: name, TestSet: testSet, Rule: rule, RuleGroup: ruleGroup}, testCaseManifestLitYaml, prof)
	}
}

const notificationXML = `<?xml version="1.0" encoding="UTF-8"?>
<notification
  xmlns="http://{{.Domain}}/NS/{{.Section}}/Notification1-0"
>
</notification>
`

const testCaseMessage = `<?xml version="1.0" encoding="UTF-8"?>
<message
  xmlns="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.Rule}}/Message1-0"
  xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
>
  <content>
    <common
      messageId=""
      creator="{{.Firstname}} {{.Lastname}} ({{.Organisation}})"
      sendBy="P23R Test"
      legalStatement="---"
    />
  </content>
  <signedContent/>
</message>
`

const testCaseManifestXML = `<?xml version="1.0" encoding="UTF-8"?>
<testCaseManifest
	xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/TestCaseManifest1-0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

	id="{{.UUID}}"
	name="{{.TestCase}}"
	title="«Titel für den Testfall»"
	releasedAt="2014-01-01T12:00:00Z"
	ruleName="{{.Rule}}"
	ruleGroupName="{{.RuleGroup}}"
>
	<release>0.1</release>
	<subjects>testcase</subjects>
	<subjects>Testfall</subjects>
	<subjects>{{.Rule}}</subjects>
	<subjects>{{.RuleGroup}}</subjects>

	<creators>{{.Firstname}} {{.Lastname}} ({{.Organisation}})</creators>

	<behaviours>It should ... behave like ...</behaviours>

	<descriptions lang="de-DE">--</descriptions>

</testCaseManifest>
`

const testCaseManifestLitYaml = `	xmlns: http://leitstelle.p23r.de/NS/p23r/nrl/TestCaseManifest1-0

Basic information about the rule

	id: {{.UUID}}
	name: {{.TestCase}}
	title: «Titel für den Testfall»

	release: 0.1
	releasedat: 2015-01-01T12:00:00

What will be tested

	rulename: {{.Rule}}
	rulegroupname: {{.RuleGroup}}

Keywords for a search portal and as filter for packaging

	subjects:
		- testcase
		- Testfall
		- {{.Rule}}
		- {{.RuleGroup}}

List of authors

	creators:
		- {{.Firstname}} {{.Lastname}} ({{.Organisation}})

Behaviour in BDD tests

	behaviours:
		- It should ... behave like ...

Textual descriptions in various languages for the people, which
need to understand the test case.

	descriptions:
		- lang: de-DE
			text: |
				--
`
