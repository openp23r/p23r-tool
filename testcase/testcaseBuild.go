package testcase

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/markdown"

	"gopkg.in/yaml.v2"
)

// Manifest describes the format of the yaml file
type Manifest struct {
	ID            string
	Name          string
	Title         string
	Release       string
	ReleasedAt    string
	Subjects      []string
	Creators      []string
	Behaviours    []string
	RuleName      string
	RuleGroupName string
	Expect        string
	Descriptions  []Description
}

// Description is a multi lingual text
type Description struct {
	Lang string
	Text string
}

func testCaseManifest2XML(m *Manifest) []byte {
	xml := `<?xml version="1.0" encoding="UTF-8"?>
<testCaseManifest
  xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/TestCaseManifest1-0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
`
	if m.ID != "" {
		xml += "\n  id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += "\n  name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += "\n  title=\"" + m.Title + `"`
	}
	if m.ReleasedAt != "" {
		xml += "\n  releasedAt=\"" + m.ReleasedAt + `"`
	}
	if m.RuleName != "" {
		xml += "\n  ruleName=\"" + m.RuleName + `"`
	}
	if m.RuleGroupName != "" {
		xml += "\n  ruleGroupName=\"" + m.RuleGroupName + `"`
	}
	if m.Expect != "" {
		xml += "\n  expect=\"" + m.Expect + `"`
	}
	xml += "\n>"
	if m.Release != "" {
		xml += "\n  <release>" + m.Release + "</release>"
	}
	for _, x := range m.Subjects {
		xml += "\n  <subjects>" + x + "</subjects>"
	}
	for _, x := range m.Creators {
		xml += "\n  <creators>" + x + "</creators>"
	}
	for _, x := range m.Behaviours {
		xml += "\n  <behaviours>" + x + "</behaviours>"
	}
	for _, x := range m.Descriptions {
		xml += "\n  <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	xml += "\n</testCaseManifest>"
	return []byte(xml)
}

// Build read all files from sources and convert them
func Build(path string, verbose bool) {
	// Generate Manifest.xml from yaml sources if file in sources is newer
	var sourceStat os.FileInfo
	data, err := ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.lityaml"))
	if err != nil {
		data, err = ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.yaml"))
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.yaml"))
	} else {
		data = markdown.Filter(data)
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.lityaml"))
	}
	if err == nil {
		targetStat, err := os.Stat(filepath.Join(path, "Manifest.xml"))
		if err != nil || sourceStat.ModTime().After(targetStat.ModTime()) {
			if verbose {
				log.Info.Println("create Manifest.xml from", filepath.Join(path, "sources"))
			}
			var m *Manifest
			err = yaml.Unmarshal(data, &m)
			if err == nil {
				err = ioutil.WriteFile(filepath.Join(path, "Manifest.xml"), testCaseManifest2XML(m), 0600)
				if err != nil {
					log.Error.Fatalln("Couldn't write Manifest.xml in", path, "because", err)
				}
			} else {
				log.Error.Fatalln("Couldn't read Manifest in", filepath.Join(path, "sources"), "because", err)
			}
		}
	}
}
