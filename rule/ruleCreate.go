package rule

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"text/template"

	"github.com/jteeuwen/go-pkg-xmlx"
	"github.com/odeke-em/go-uuid"
	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/profile"
)

// Parameter contains the context values for the content
type Parameter struct {
	RuleName, RuleGroup, Step string
}

func valueOrDefault(value string, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

func createFile(path string, fileName string, parameter *Parameter, content string, prof *profile.Profile) {
	type Mapping struct {
		Firstname, Lastname, Organisation, RuleName, RuleGroup, Step, Domain, Section, UUID string
	}
	var mapping *Mapping
	var err error
	mapping = &Mapping{
		Firstname:    valueOrDefault(prof.Firstname, "--your-firstname--"),
		Lastname:     valueOrDefault(prof.Lastname, "--your-lastname--"),
		Organisation: valueOrDefault(prof.Organisation, "--your-organisation--"),
		Domain:       valueOrDefault(prof.Domain, "--your-domain--"),
		Section:      valueOrDefault(prof.Section, "--section--"),
		RuleName:     valueOrDefault(parameter.RuleName, "--rule-name--"),
		RuleGroup:    valueOrDefault(parameter.RuleGroup, "--rule-group--"),
		Step:         valueOrDefault(parameter.Step, "--step-number--"),
		UUID:         uuid.New(),
	}
	t := template.Must(template.New("file").Parse(content))
	var f *os.File
	f, err = os.Create(filepath.Join(path, fileName))
	defer f.Close()
	if err != nil {
		print("F")
		log.Error.Fatal(err)
		return
	}
	err = t.Execute(f, mapping)
	if err != nil {
		print("F")
		log.Error.Fatal(err)
		return
	}
	print(".")
}

func getRulegroup() (bool, string) {
	data, err := ioutil.ReadFile("Manifest.xml")
	subproject := true
	if err != nil {
		data, err = ioutil.ReadFile("../Manifest.xml")
		subproject = false
	}
	if err != nil {
		log.Error.Fatal("No manifest file found")
		return false, ""
	}
	doc := xmlx.New()
	err = doc.LoadString(string(data), nil)
	if err != nil {
		log.Error.Fatal(err)
		return false, ""
	}
	node := doc.SelectNode("*", "ruleGroupManifest")
	if node == nil {
		return false, ""
	}
	return subproject, node.As("*", "name")
}

// GenerateTemplate generates all directories and files nescessary for a P23R
// notification rule.
//
func GenerateTemplate(name string, createSources bool) {
	isSubproject, ruleGroup := getRulegroup()
	if ruleGroup == "" {
		log.Error.Fatal("Rules must be created inside a rule group")
		return
	}

	// create directory if is subproject
	var path string
	if isSubproject {
		err := os.Mkdir(name, os.ModeDir|0700)
		if err != nil {
			println("F")
			log.Error.Fatal(err)
		} else {
			print(".")
		}
		path = name
	} else {
		path = "."
	}

	// Read profile
	var prof *profile.Profile
	var err error
	prof, err = profile.ReadProfile()
	if err != nil {
		println("F")
		log.Error.Fatal(err)
	}

	// create Manifest file for rule
	createFile(path, "Manifest.xml", &Parameter{RuleName: name, RuleGroup: ruleGroup}, ruleManifestXML, prof)

	// create Message schema for rule
	createFile(path, "Message.xsd", &Parameter{RuleName: name, RuleGroup: ruleGroup}, messageXsd, prof)

	// create multi notification files
	createFile(path, "MultiNotificationProfile.dats", &Parameter{RuleName: name, RuleGroup: ruleGroup}, multiNotificationProfileDats, prof)
	createFile(path, "MultiNotificationProfile.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup}, multiNotificationProfileXslt, prof)

	// create selection file
	createFile(path, "Selection.dats", &Parameter{RuleName: name, RuleGroup: ruleGroup}, selectionDats, prof)

	// create for each step profileTransformation, notificationTransformation and schema files
	createFile(path, "NotificationTransformationStep01.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "01"}, notificationTransformationFirstStepXslt, prof)
	createFile(path, "NotificationTransformationStep02.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "02"}, notificationTransformationXslt, prof)
	createFile(path, "NotificationTransformationStep03.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "03"}, notificationTransformationXslt, prof)
	createFile(path, "NotificationTransformationStep42.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "42"}, notificationTransformationXslt, prof)
	createFile(path, "ProfileTransformationStep00.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "00"}, profileTransformationXslt, prof)
	createFile(path, "ProfileTransformationStep01.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "01"}, profileTransformationXslt, prof)
	createFile(path, "ProfileTransformationStep02.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "02"}, profileTransformationXslt, prof)
	createFile(path, "ProfileTransformationStep03.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "03"}, profileTransformationXslt, prof)
	createFile(path, "ProfileTransformationStep42.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "42"}, profileTransformationXslt, prof)
	createFile(path, "NotificationIntermediate01.xsd", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "01"}, notificationIntermediateXsd, prof)
	createFile(path, "NotificationIntermediate02.xsd", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "02"}, notificationIntermediateXsd, prof)
	createFile(path, "NotificationIntermediate03.xsd", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "03"}, notificationIntermediateXsd, prof)
	createFile(path, "NotificationIntermediate42.xsd", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "42"}, notificationIntermediateXsd, prof)

	// Create final notification schema
	createFile(path, "Notification.xsd", &Parameter{RuleName: name, RuleGroup: ruleGroup}, notificationXsd, prof)

	// Create filter to sign the mandatory content
	createFile(path, "NotificationCore.xslt", &Parameter{RuleName: name, RuleGroup: ruleGroup}, notificationCoreXslt, prof)

	// Create directories
	err = os.Mkdir(filepath.Join(path, "shared"), os.ModeDir|0700)
	if err != nil {
		print("F")
		log.Error.Fatal(err)
	} else {
		print(".")
	}
	err = os.Mkdir(filepath.Join(path, "shared", "sources"), os.ModeDir|0700)
	if err != nil {
		print("F")
		log.Error.Fatal(err)
	} else {
		print(".")
	}
	err = os.Mkdir(filepath.Join(path, "sources"), os.ModeDir|0700)
	if err != nil {
		print("F")
		log.Error.Fatal(err)
	} else {
		print(".")
	}

	if createSources {
		print("S")

		// create Message schema for rule
		createFile(filepath.Join(path, "sources"), "Message.model", &Parameter{RuleName: name, RuleGroup: ruleGroup}, messageModel, prof)

		// Create Manifest content
		createFile(filepath.Join(path, "sources"), "Manifest.lityaml", &Parameter{RuleName: name, RuleGroup: ruleGroup}, ruleManifestLitYaml, prof)

		// create for each step profileTransformation, notificationTransformation and schema files
		createFile(filepath.Join(path, "sources"), "NotificationIntermediate01.model", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "01"}, notificationIntermediateModel, prof)
		createFile(filepath.Join(path, "sources"), "NotificationIntermediate02.model", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "02"}, notificationIntermediateModel, prof)
		createFile(filepath.Join(path, "sources"), "NotificationIntermediate03.model", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "03"}, notificationIntermediateModel, prof)
		createFile(filepath.Join(path, "sources"), "NotificationIntermediate42.model", &Parameter{RuleName: name, RuleGroup: ruleGroup, Step: "42"}, notificationIntermediateModel, prof)

		// Create final notification schema
		createFile(filepath.Join(path, "sources"), "Notification.model", &Parameter{RuleName: name, RuleGroup: ruleGroup}, notificationModel, prof)
	}
}

const selectionDats = `
>>> REPLACE ALL «...» !!!

Selection.dats

Pfad:     «Rule Package Name»/«Release»/{{.RuleGroup}}/{{.RuleName}}/
Präsenz:  verbindlich
Referenz: T-BRS Kapitel 5 / Tabelle 33
Inhalt:   P23R Selection Skript zur Selektion der Quelldaten aus einem Quelldatenkonnektor

Model «Model Shortcut» = http://{{.Domain}}/NS/{{.Section}}/«Model Name»1-0

Your elements, attributes and queries

selection {
	@namespace { "http://{{.Domain}}/NS/«section»/{{.RuleGroup}}/{{.RuleName}}/selection" }
}
`

const notificationIntermediateXsd = `<?xml version="1.0" encoding="UTF-8"?>
<!--

NotificationIntermediate{{.Step}}.xsd

Pfad:   «Rule Package Name»/0.1/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate{{.Step}}.xsd
Datei:  verpflichtend
Referenz: T-BRS Kapitel 5 / Tabelle 33 / Req-TBRS_24
Inhalt:   XML-Schema für syntaktische Validierung der temporären Benachrichtigung

-->

<xsd:schema
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate{{.Step}}1-0"
targetNamespace="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate{{.Step}}1-0"
elementFormDefault="qualified"
version="1.0">
</xsd:schema>
`

const notificationIntermediateModel = `### Specification of the Message

#### _{{.Section}}.{{.RuleGroup}}.{{.RuleName}}.NotificationIntermediate{{.Step}}_

Describes the model for an intermediate notification to be validated by the rule.

##### Notes

_none_

##### Specification of the model

	model {{.Section}}.{{.RuleGroup}}.{{.RuleName}}.NotificationIntermediate{{.Step}} as nfc release 0.1 {

		creator '{{.Firstname}} {{.Lastname}}'
		organisation '{{.Organisation}}'
		base http://{{.Domain}}/NS/
		requires p23r release 1.0

		title 'intermediate notification format for the rule'

		tag 'notification'

		description 'This model describes the format of an intermediate notification.'

		entity Notification {
		}

	}
`

const profileTransformationXslt = `<?xml version="1.0" encoding="UTF-8"?>
<!--

ProfileTransformationStep{{.Step}}.xslt

Pfad:     «Rule Package Name»/0.1/{{.RuleGroup}}/{{.RuleName}}/
Präsenz:  optional
Referenz: T-BRS Kapitel 5 / Tabelle 33
Inhalt: 	Skript zur Transformation des Profile in Schritt {{.Step}}

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:np="http://leitstelle.p23r.de/NS/p23r/nrl/NotificationProfile1-1"
  version="2.0">

  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="/np:notificationProfile"/>
  </xsl:template>

  <xsl:template match="np:notificationProfile">
    <np:notificationProfile>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()|comment()"/>
    </np:notificationProfile>
  </xsl:template>

  <xsl:template match="np:notificationProfile/np:communication">
    <np:communication>
      <np:criteria name="unused"/>
      <np:publicTimestamp/>
      <xsl:element name="receivers" namespace="{namespace-uri()}">
        <xsl:attribute name="receiverId">log.rest</xsl:attribute>
        <xsl:attribute name="receiverName">Default Log</xsl:attribute>
        <xsl:element name="support" namespace="{namespace-uri()}"/>
        <xsl:element name="technicalSupport" namespace="{namespace-uri()}"/>
        <xsl:element name="channels" namespace="{namespace-uri()}">
          <xsl:attribute name="id">de.p23r.connector.log.rest</xsl:attribute>
          <xsl:attribute name="name">p23r-log-rest-connector</xsl:attribute>
          <xsl:attribute name="type">system.log.rest</xsl:attribute>
          <xsl:attribute name="submissionTime"><xsl:value-of select='current-dateTime() + xs:dayTimeDuration("PT1H")'/></xsl:attribute>
          <xsl:attribute name="timeoutAt"><xsl:value-of select='current-dateTime() + xs:dayTimeDuration("PT1H")'/></xsl:attribute>
          <np:parameters name="transactionId"><xsl:value-of select="np:notificationProfile/np:general/@transactionId"/></np:parameters>
        </xsl:element>
      </xsl:element>
    </np:communication>
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
`

const notificationTransformationXslt = `<?xml version="1.0" encoding="UTF-8"?>
<!--

NotificationTransformationStep{{.Step}}.xslt

Pfad:     «Rule Package Name»/0.1/{{.RuleGroup}}/{{.RuleName}}/
Präsenz:  verbindlich
Referenz: T-BRS Kapitel 5 / Tabelle 33
Inhalt: 	Skript zur Transformation der Notification in Schritt {{.Step}}

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:ms="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/Message1-0"
  xmlns:np="http://leitstelle.p23r.de/NS/p23r/nrl/NotificationProfile1-1"
	version="2.0">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
	  <xsl:copy>
	    <xsl:apply-templates select="@*|node()" mode="copy"/>
	  </xsl:copy>
	</xsl:template>

	<xsl:template match="@*|node()" mode="copy">
	  <xsl:copy>
	    <xsl:apply-templates select="@*|node()" mode="copy"/>
	  </xsl:copy>
	</xsl:template>
</xsl:stylesheet>
`

const notificationTransformationFirstStepXslt = `<?xml version="1.0" encoding="UTF-8"?>
<!--

NotificationTransformationStep{{.Step}}.xslt

Pfad:     «Rule Package Name»/0.1/{{.RuleGroup}}/{{.RuleName}}/
Präsenz:  verbindlich
Referenz: T-BRS Kapitel 5 / Tabelle 33
Inhalt: 	Skript zur Transformation der Notification in Schritt {{.Step}}

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:ms="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/Message1-0"
  xmlns:np="http://leitstelle.p23r.de/NS/p23r/nrl/NotificationProfile1-1"
	version="2.0">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template name="default">
	  <xsl:copy>
	    <xsl:apply-templates select="@*|node()" mode="copy"/>
	  </xsl:copy>
	</xsl:template>

	<xsl:template match="@*|node()" mode="copy">
	  <xsl:copy>
	    <xsl:apply-templates select="@*|node()" mode="copy"/>
	  </xsl:copy>
	</xsl:template>
</xsl:stylesheet>
`

const notificationCoreXslt = `<?xml version="1.0" encoding="UTF-8"?>
<!--

NotificationCore.xslt

Pfad:     «Rule Package Name»/0.1/{{.RuleGroup}}/{{.RuleName}}/
Präsenz:  verbindlich
Referenz: T-BRS Kapitel 5 / Tabelle 33
Inhalt:   Skript zum Filtern der finalen Benachrichtung zwecks Signierung des Inhaltes

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
`

const notificationXsd = `<?xml version="1.0" encoding="UTF-8"?>
<!--

Notification.xsd

Pfad:     «Rule Package Name»/0.1/{{.RuleGroup}}/{{.RuleName}}/
Präsenz:  verpflichtend
Referenz: T-BRS Kapitel 5 / Tabelle 33
Inhalt:   XSD-Schema der finalen Benachrichtigung

-->
<xsd:schema
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/Notification1-0"
targetNamespace="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/Notification1-0"
elementFormDefault="qualified"
version="1.0">
</xsd:schema>
`

const notificationModel = `### Specification of the final Notification

#### _{{.Section}}.{{.RuleGroup}}.{{.RuleName}}.Notification_

Describes the model for the final notification to be sent by the rule.

##### Notes

_none_

##### Specification of the model

	model {{.Section}}.{{.RuleGroup}}.{{.RuleName}}.Notification as nfc release 0.1 {

		creator '{{.Firstname}} {{.Lastname}}'
		organisation '{{.Organisation}}'
		base http://{{.Domain}}/NS/
		requires p23r release 1.0

		title 'final notification format for the rule'

		tag 'notification'

		description 'This model describes the information which finally will be sent as notification.'

		entity Notification {
		}

	}
`

const multiNotificationProfileDats = `

MultiNotificationProfile.dats

Pfad:     «Rule Package Name»/0.1/{{.RuleGroup}}/{{.RuleName}}/
Präsenz:  optional
Referenz: T-BRS Kapitel 5 / Tabelle 33
Inhalt:   P23R Selection Skript zum Selektieren der Daten für die initialen Notification Profiles

notificationProfile {
	@ns { "http://leitstelle.p23r.de/NS/P23R/nrl/NotificationProfile1-0" }

Copy the existing notification profile.

	[
		for each ng
		take 1
		return ng.notificationProfile.*
	]
}
`

const multiNotificationProfileXslt = `<?xml version="1.0" encoding="UTF-8"?>
<!--

MultiNotificationProfile.xslt

Pfad:     «Rule Package Name»/0.1/{{.RuleGroup}}/{{.RuleName}}/
Präsenz:  verbindlich
Referenz: T-BRS Kapitel 5 / Tabelle 33
Inhalt: 	Skript zur Vervielfachung des NotificationProfiles bei Bedarf für mehrere Empfänger, ...

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
`

const messageXsd = `<?xml version="1.0" encoding="UTF-8"?>
<!--

Message.xsd

Pfad:   «Rule Package Name»/0.1\{{.RuleGroup}}/{{.RuleName}}/Message.xsd
Datei:  verpflichtend
Referenz: T-BRS Kapitel 5 / Tabelle 33 / Req-TBRS_24
Inhalt:   XML-Schema für syntaktische Validierung der auslösenden Nachricht

-->

<xsd:schema
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/Message1-0"
targetNamespace="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/Message1-0"
elementFormDefault="qualified"
version="1.0">
<xsd:complexType name="P23RMessageType">
  <xsd:sequence>
    <xsd:element name="messageId" type="xsd:string"/>
    <xsd:element name="subject" type="xsd:string" default="{{.RuleName}}" minOccurs="0"/>
    <xsd:element name="transactionId" type="xsd:string" minOccurs="0"/>
    <xsd:element name="applyAt" type="xsd:date" minOccurs="0"/>
  </xsd:sequence>
  <xsd:attribute name="namespace" type="xsd:anyURI" use="optional"/>
</xsd:complexType>
<xsd:element name="message">
  <xsd:complexType>
    <xsd:complexContent>
      <xsd:extension base="P23RMessageType">
        <xsd:sequence>
          <!--
             Wenn man Parameter beim Auslösen einer Benachrichtigung übergeben will
             sollte man einen Benachrichtigungsregel-spezifischen Block an
             Parametern definieren. Nachfolgend ein Beispiel:

          <xsd:element name="parameters" minOccurs="0">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="«parameter name»" type="xsd:string"/>
              </xsd:sequence>
            </xsd:complexType>
          </xsd:element>
          -->
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
</xsd:element>
</xsd:schema>
`

const messageModel = `### Specification of the Message

#### _p23r.nrl.Message_

Describes the model for a standard part of a message used for all messages which are compatible with the P23R protocol.

##### Notes

_none_

##### Specification of the model


This model describes a fragment to be part of a
message compatible with the P23R protocol.
As each message must have a notification rule specific
namespace is this model only an example.

	model {{.Section}}.{{.RuleGroup}}.{{.RuleName}}.Message as msg release 1.0 {

		creator 'Jan Gottschick'
		organisation 'P23R-Team'
		requires p23r release 1.0

		title 'Message format for the P23R'

		tag 'message'

		description 'This model describes the information which should be always included in a message.'

		namespace ds=http://www.w3.org/2000/09/xmldsig# file 'xmldsig-core-schema.xsd'


This model is an example for a message compatible
with the P23R protocol.

		entity Message {

This is the content of input message to be
processed by the P23R.

			content : Content

This is the signed hash code for the whole
content signed by the signee.

			signedContent : instance of ds:Signature
		}

The content of a notification is mainly rule
specific.

		entity Content : * {

This is the part of a notification, which
every notification compatible with the
P23R protocol has to include.

			common : Common
		}

The common part of a notification contains
P23R specific and generic information to enable
all unique features of the P23R.

		entity Common {

This is a unique identification of the
sender of this message. It must be an URI.

			SenderId : anyURI optional

This is a unique name of the creator of the
message, e.g. the legal name of the public
authority.

			creator : string

This time specifies when the message was
received by the OSCI gateway.

			receiveTime : dateTime optional

If the P23R is behind an OSCI gateway, this
attribute should contain the original
physical address (URI) from where the message
was received.

			sourceURI : anyURI optional

This is the readable name of the person who has
approved and signed the message.

			sentBy : string

This is an email address of the person
responsible for this message,
where questions can be sent to.

			questionTos : anyURI match /^mailto:./ many optional

This is a (predefined) statement, where the
signee defines the legal state and other
conditions valid for this message.

			legalStatement : string

These are the web service addresses, to which
answers and other replies should be sent.

			replyTo : anyURI optional

This is the internal transaction identifier for
the P23R, which were given by a previous
notification.

			transactionId : string optional

This is the internal identifier of the sender
for this message.

			messageId : string

The date at which the notification must be
applied. This means that the newest
notification rule must be applied, which is
valid at that date:
validFrom <= applyAt <= validUntil

			applyAt : date optional
		}
	}
`

const ruleManifestXML = `<?xml version="1.0" encoding="UTF-8"?>
<ruleManifest
xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/RuleManifest1-1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

id="{{.UUID}}"
name="{{.RuleName}}"
title="«Titel für die Benachrichtigungsregel»"
releasedAt="2015-01-01T12:00:00"
validFrom="2015-01-01T00:00:00"
>
<release>0.1</release>
<subjects>rule</subjects>
<subjects>Benachrichtigungsregel</subjects>

<submitAtMonthlyPeriods>---01Z</submitAtMonthlyPeriods>

<creators>{{.Firstname}} {{.Lastname}} ({{.Organisation}})</creators>

<descriptions lang="de-DE">--</descriptions>
<targets lang="de-DE">--</targets>
<hints lang="de-DE">--</hints>
<legalBackgrounds lang="de-DE">--</legalBackgrounds>

<generation>
	<steps name="Selection" title="Select data from source connector" notificationType="" notificationTransformation="Selection.dats" profileTransformation="ProfileTransformationStep00.xslt"/>
	<steps name="SyntacticValidation" title="Validate the selected data against syntax pattern" notificationType="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate01" notificationTransformation="NotificationTransformationStep01.xslt" profileTransformation="ProfileTransformationStep01.xslt"/>
	<steps name="SemanticValidation" title="Validate the selected data against semantic dependencies" notificationType="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate02" notificationTransformation="NotificationTransformationStep02.xslt" profileTransformation="ProfileTransformationStep02.xslt"/>
	<steps name="BusinessTransaction" title="Business transactions to implement the rule's main goals" notificationType="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate03" notificationTransformation="NotificationTransformationStep03.xslt" profileTransformation="ProfileTransformationStep03.xslt"/>
	<steps name="Finalisation" title="Create final notification" notificationType="http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate42" notificationTransformation="NotificationTransformationStep42.xslt" profileTransformation="ProfileTransformationStep42.xslt"/>
</generation>

</ruleManifest>
`

const ruleManifestLitYaml = `	xmlns: http://leitstelle.p23r.de/NS/p23r/nrl/RuleManifest1-1

Basic information about the rule

	id: {{.UUID}}
	name: {{.RuleName}}
	title: «Titel für die Benachrichtigungsregel»

	release: 0.1
	releasedat: 2015-01-01T12:00:00
	validfrom: 2015-01-01T00:00:00
	submitatmonthlyperiods:
		- ---01Z

Keywords for a search portal and as filter for packaging

	subjects:
		- rule
		- Benachrichtigungsregel

List of authors

	creators:
		- {{.Firstname}} {{.Lastname}} ({{.Organisation}})

Textual descriptions in various languages for the people, which
decides about the activation of a rule group and its rules.

	descriptions:
		- lang: de-DE
			text: |
				--

	targets:
		- lang: de-DE
			text: |
				--

	hints:
		- lang: de-DE
			text: |
				--

	legalbackgrounds:
		- lang: de-DE
			text: |
				--

Specification of the generation steps for the notifcation

	steps:

Attention, the first step has no notificationType and a transformation script must
use a default template for the first match!

		- name: Selection
			title: Select data from source connector
			notificationtype:
			notificationtransformation: Selection.dats
			profiletransformation: ProfileTransformationStep00.xslt

		- name: SyntacticValidation
			title: Validate the selected data against syntax pattern
			notificationtype: http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate01
			notificationtransformation: NotificationTransformationStep01.xslt
			profiletransformation: ProfileTransformationStep01.xslt

		- name: SemanticValidation
			title: Validate the selected data against semantic dependencies
			notificationtype: http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate02
			notificationtransformation: NotificationTransformationStep02.xslt
			profiletransformation: ProfileTransformationStep02.xslt

		- name: BusinessTransaction
			title: Business transactions to implement the rule's main goals
			notificationtype: http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate03
			notificationtransformation: NotificationTransformationStep03.xslt
			profiletransformation: ProfileTransformationStep03.xslt

		- name: Finalisation
			title: Create final notification
			notificationtype: http://{{.Domain}}/NS/{{.Section}}/{{.RuleGroup}}/{{.RuleName}}/NotificationIntermediate42
			notificationtransformation: NotificationTransformationStep42.xslt
			profiletransformation: ProfileTransformationStep42.xslt

`
