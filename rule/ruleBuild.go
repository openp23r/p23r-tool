package rule

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/markdown"

	"gopkg.in/yaml.v2"
)

// RuleManifest describes the format of the yaml file
type Manifest struct {
	ID                         string
	Name                       string
	Title                      string
	Release                    string
	ReleasedAt                 string
	ValidFrom                  string
	ValidUntil                 string
	Signer                     string
	Subjects                   []string
	SubmitAtDailyPeriods       []string
	SubmitAtWeeklyPeriods      []string
	SubmitAtMonthlyPeriods     []string
	SubmitAtYearlyPeriods      []string
	SubmitAts                  []string
	CreateDaysBefore           string
	CriticalSubmitDelay        string
	RequirementsOnApprove      string
	RegisteredAt               string
	QualifiedSignatureRequired bool
	Creators                   []string
	Descriptions               []Description
	Targets                    []Description
	Hints                      []Description
	LegalBackgrounds           []Description
	Steps                      []step
}

// Description is a multi lingual text
type Description struct {
	Lang string
	Text string
}

// step defines which scripts to use for each step in the pipeline
type step struct {
	Name                       string
	Title                      string
	NotificationType           string
	NotificationTransformation string
	ProfileTransformation      string
}

func ruleManifest2XML(m *Manifest) []byte {
	xml := `<?xml version="1.0" encoding="UTF-8"?>
<ruleManifest
  xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/RuleManifest1-1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
`
	if m.ID != "" {
		xml += "\n  id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += "\n  name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += "\n  title=\"" + m.Title + `"`
	}
	if m.ReleasedAt != "" {
		xml += "\n  releasedAt=\"" + m.ReleasedAt + `"`
	}
	if m.ValidFrom != "" {
		xml += "\n  validFrom=\"" + m.ValidFrom + `"`
	}
	if m.ValidUntil != "" {
		xml += "\n  validUntil=\"" + m.ValidUntil + `"`
	}
	if m.Signer != "" {
		xml += "\n  signer=\"" + m.Signer + `"`
	}
	if m.CreateDaysBefore != "" {
		xml += "\n  createDaysBefore=\"" + m.CreateDaysBefore + `"`
	}
	if m.CriticalSubmitDelay != "" {
		xml += "\n  criticalSubmitDelay=\"" + m.CriticalSubmitDelay + `"`
	}
	if m.RequirementsOnApprove != "" {
		xml += "\n  requirementsOnApprove=\"" + m.RequirementsOnApprove + `"`
	}
	if m.RegisteredAt != "" {
		xml += "\n  registeredAt=\"" + m.RegisteredAt + `"`
	}
	if m.QualifiedSignatureRequired {
		xml += "\n  qualifiedSignatureRequired=\"true\""
	}
	xml += "\n>"
	if m.Release != "" {
		xml += "\n  <release>" + m.Release + "</release>"
	}
	for _, x := range m.Subjects {
		xml += "\n  <subjects>" + x + "</subjects>"
	}
	for _, x := range m.SubmitAtDailyPeriods {
		xml += "\n  <submitAtDailyPeriods>" + x + "</submitAtDailyPeriods>"
	}
	for _, x := range m.SubmitAtWeeklyPeriods {
		xml += "\n  <submitAtWeeklyPeriods>" + x + "</submitAtWeeklyPeriods>"
	}
	for _, x := range m.SubmitAtMonthlyPeriods {
		xml += "\n  <submitAtMonthlyPeriods>" + x + "</submitAtMonthlyPeriods>"
	}
	for _, x := range m.SubmitAtYearlyPeriods {
		xml += "\n  <submitAtYearlyPeriods>" + x + "</submitAtYearlyPeriods>"
	}
	for _, x := range m.SubmitAts {
		xml += "\n  <submitAts>" + x + "</submitAts>"
	}
	for _, x := range m.Creators {
		xml += "\n  <creators>" + x + "</creators>"
	}
	for _, x := range m.Descriptions {
		xml += "\n  <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	for _, x := range m.Targets {
		xml += "\n  <targets lang=\"" + x.Lang + "\">" + x.Text + "</targets>"
	}
	for _, x := range m.Hints {
		xml += "\n  <hints lang=\"" + x.Lang + "\">" + x.Text + "</hints>"
	}
	for _, x := range m.LegalBackgrounds {
		xml += "\n  <legalBackgrounds lang=\"" + x.Lang + "\">" + x.Text + "</legalBackgrounds>"
	}
	xml += "\n  <generation>"
	for _, x := range m.Steps {
		xml += "\n    <steps name=\"" + x.Name + "\"" +
			" title=\"" + x.Title + "\"" +
			" notificationType=\"" + x.NotificationType + "\"" +
			" notificationTransformation=\"" + x.NotificationTransformation + "\"" +
			" profileTransformation=\"" + x.ProfileTransformation + "\"" +
			" />"
	}
	xml += "\n  </generation>"
	xml += "\n</ruleManifest>"
	return []byte(xml)
}

// Build read all files from sources and convert them
func Build(path string, verbose bool) {
	if verbose {
		log.Info.Printf("Build rule at %s", path)
	}
	// Generate Manifest.xml from yaml sources if file in sources is newer
	var sourceStat os.FileInfo
	data, err := ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.lityaml"))
	if err != nil {
		data, err = ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.yaml"))
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.yaml"))
	} else {
		data = markdown.Filter(data)
		sourceStat, _ = os.Stat(filepath.Join(path, "sources", "Manifest.lityaml"))
	}
	if err == nil {
		target := filepath.Join(path, "Manifest.xml")
		targetStat, err := os.Stat(target)
		if err != nil || sourceStat.ModTime().After(targetStat.ModTime()) {
			if verbose {
				log.Info.Println("Create Manifest.xml from", filepath.Join(path, "sources"))
			}
			var m *Manifest
			err = yaml.Unmarshal(data, &m)
			if err == nil {
				err = ioutil.WriteFile(filepath.Join(path, "Manifest.xml"), ruleManifest2XML(m), 0600)
				if err != nil {
					log.Error.Fatalln("Couldn't write Manifest.xml in", path, "because", err)
				}
			} else {
				log.Error.Fatalln("Couldn't read Manifest in", filepath.Join(path, "sources"), "because", err)
			}
		} else {
			if verbose {
				log.Info.Printf("Manifest %s is up-to-date", target)
			}
		}
	}

	// Compile all domain models with the P23R Model Compiler
	_, modelcErr := exec.Command("which", "modelc").Output()
	// Read files from directory sources
	files, err := ioutil.ReadDir(filepath.Join(path, "sources"))
	if err != nil {
		log.Error.Fatalln("could read directory", filepath.Join(path, "sources"), "->", err)
	}
	for _, f := range files {
		source := filepath.Join(path, "sources", f.Name())
		target := filepath.Join(path, strings.Split(f.Name(), ".")[0]+".xsd")
		if strings.HasSuffix(f.Name(), ".model") {
			if modelcErr != nil {
				log.Error.Printf("Could not build '%s' as the domain model compiler modelc is not installed", target)
			}
			sourceStat, _ := os.Stat(source)
			targetStat, targetErr := os.Stat(target)
			if targetErr != nil || sourceStat.ModTime().After(targetStat.ModTime()) {
				if verbose {
					log.Info.Println("Create XSD schema from model file", filepath.Join(path, f.Name()))
				}
				if msg, err := exec.Command("modelc", "xsd", source, target).CombinedOutput(); err != nil {
					log.Error.Printf("Could not translate '%s'", filepath.Join(path, "sources", f.Name()))
					log.Info.Println(string(msg))
				}
			} else {
				if verbose {
					log.Info.Printf("XSD schema %s is up-to-date", target)
				}
			}
		}

	}
}
