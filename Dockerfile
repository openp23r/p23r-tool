FROM golang:latest

RUN go clean -i net
RUN go install -tags netgo std
