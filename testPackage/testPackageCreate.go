package testPackage

import (
	"bufio"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/helper"
	"gitlab.com/openp23r/p23r-tool/profile"
	"gitlab.com/openp23r/p23r-tool/testset"

	"github.com/odeke-em/go-uuid"
)

// Parameter contains the context values for the content
type Parameter struct {
	TestSetPackageVersion string
}

func createFile(path string, fileName string, parameter *Parameter, prof *profile.Profile, testsetManifest testset.Manifest) {
	var err error
	var mp Manifest
	mp.ID = uuid.New()
	mp.Name = prof.PackageName
	mp.Title = "Projektleistelle"
	mp.ReleasedAt = time.Now().Format(time.RFC3339)
	mp.Publisher = "Fraunhofer Fokus"
	mp.Release = parameter.TestSetPackageVersion
	mp.Creators = append(mp.Creators, prof.Email)
	var d Description
	d.Lang = "de-DE"
	d.Text = ""
	mp.Descriptions = append(mp.Descriptions, d)
	/*
		var t Description
		t.Lang = "de-DE"
		t.Text = "keine spezifische Zielgruppe"
		mp.Targets = append(mp.Targets, t)
		var l Description
		l.Lang = "de-DE"
		l.Text = "Es gilt die Lizenz des Entwicklerportals"
		mp.Licenses = append(mp.Licenses, l)
	*/
	c := testPackageManifest2XML(&mp)

	err = ioutil.WriteFile(filepath.Join(path, fileName), c, 0644)
	if err != nil {
		log.Error.Fatal(err)
	}
	log.Info.Println("end of manifest creation in directory", path)
}

// GeneratePackage generates all directories and files nescessary for a P23R
// model package.
//
func GeneratePackage(prof *profile.Profile, path, generatePath, version string, noDelete, verbose bool) {
	// Create directories
	groundPath := filepath.Join(generatePath, "tp", prof.PackageName, version)
	err := os.MkdirAll(groundPath, os.ModeDir|0700)
	if err != nil {
		// println("F")
		log.Error.Fatal(err)
	}
	log.Info.Println("groundPath:", groundPath)

	// data, err := ioutil.ReadFile(filepath.Join(path, "Manifest.xml"))

	var m testset.Manifest
	m = Unmarshall(path)

	createFile(groundPath, "Manifest.xml", &Parameter{TestSetPackageVersion: version}, prof, m)

	// copy files and directories
	if err = helper.CopyDir(path, filepath.Join(groundPath, m.Name)); err != nil {
		log.Error.Fatalln("in copy directory", path, "to", filepath.Join(groundPath, m.Name))
	}

	log.Info.Println("generate zip file:", filepath.Join(generatePath, "TP-"+prof.PackageName+".zip"))
	/*
		zip := new(archivex.ZipFile)
		zip.Create(filepath.Join(generatePath, "TP-"+prof.PackageName+".zip"))
		zip.AddAll(filepath.Join(generatePath, "tp", prof.PackageName), true)
		zip.Close()
	*/
	// zipit(filepath.Join(generatePath, "tp", prof.PackageName), filepath.Join(generatePath, "TP-"+prof.PackageName+".zip"))
	env := os.Environ()

	cmdArgs := []string{"-r", filepath.Join(generatePath, "TP-"+prof.PackageName+".zip"), prof.PackageName}
	//	runCommand("cd "+filepath.Join("_packages", "tp", prof.PackageName)+" && zip", env, cmdArgs)
	runCommand("zip", env, cmdArgs, filepath.Join(generatePath, "tp"))

}

func runCommand(command string, env []string, cmdArgs []string, cmdExecPath string) {

	// log.Info.Println("---> env:", env)
	log.Info.Println("cmd:", command, " ", cmdArgs)
	cmd := exec.Command(command, cmdArgs...)
	cmd.Env = env
	cmd.Dir = cmdExecPath
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		log.Error.Fatalln("Error creating StdoutPipe for Cmd '", command, cmdArgs, "':", err)
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			log.Info.Println(command, "|", scanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		log.Error.Fatalln("Error starting Cmd '", command, cmdArgs, "':", err)
	}

	err = cmd.Wait()
	if err != nil {
		log.Warning.Println("Error waiting for Cmd '", command, cmdArgs, "':", err.Error())

	}

}
