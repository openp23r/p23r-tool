package testPackage

import (
	"io/ioutil"
	"path/filepath"

	"gitlab.com/openp23r/p23r-common/log"

	"github.com/jteeuwen/go-pkg-xmlx"
	"gitlab.com/openp23r/p23r-tool/manifest"
)

func getTestSets(dir string, verbose bool) []string {
	testSetPaths := []string{}
	manifestType := manifest.GetManifestType(dir, verbose)
	var dirs []string
	if manifestType != manifest.NoManifestID {
		dirs = []string{dir}
	} else {
		xs, err := ioutil.ReadDir(dir)
		dirs = []string{}
		if err == nil {
			for _, x := range xs {
				if x.IsDir() {
					dirs = append(dirs, x.Name())
				}
			}
		}
	}
	for _, x := range dirs {
		manifestType = manifest.GetManifestType(x, verbose)
		switch manifestType {
		case manifest.TestSetManifestID:
			testSetPaths = append(testSetPaths, x)
		case manifest.NoManifestID:
			testSetPaths = append(testSetPaths, getTestSets(x, verbose)...)
		}
	}
	return testSetPaths
}

func getIDFromTestSet(dir string) string {
	data, err := ioutil.ReadFile(filepath.Join(dir, "Manifest.xml"))
	if err != nil {
		log.Error.Fatalln("Could not read Manifest file", filepath.Join(dir, "Manifest.xml"))
	}
	doc := xmlx.New()
	err = doc.LoadString(string(data), nil)
	if err != nil {
		log.Error.Fatalln("Could not parse Manifest file", filepath.Join(dir, "Manifest.xml"))
	}
	node := doc.SelectNode("*", "testSetManifest")
	if node == nil {
		log.Error.Fatalln("Could not read ID from Manifest file", filepath.Join(dir, "Manifest.xml"))
	}
	return node.As("", "id")
}

// GetTestSetIDs collect all ids of the avalaible test sets
func GetTestSetIDs(dir string, verbose bool) []string {
	dirs := getTestSets(dir, verbose)
	ids := []string{}
	for _, x := range dirs {
		id := getIDFromTestSet(x)
		println(id)
		ids = append(ids, id)
	}
	return ids
}
