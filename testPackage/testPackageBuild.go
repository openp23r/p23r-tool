package testPackage

import (
	"path/filepath"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/testset"

	// "github.com/jbussdieker/golibxml"
	"github.com/jteeuwen/go-pkg-xmlx"
)

// Manifest describes the items of a test package manifest file
type Manifest struct {
	ID                   string
	Name                 string
	Title                string
	Publisher            string
	ReleasedAt           string
	Release              string
	Subjects             []string
	Namespaces           []string
	Context              string
	Creators             []string
	Descriptions         []Description
	SpecificationVersion string
}

// Description is a multi lingual text
type Description struct {
	Lang string
	Text string
}

// Unmarshall important fields
func Unmarshall(path string) testset.Manifest {
	doc := xmlx.New()
	var err = doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Fatalln("ERROR: Could not read XML file", filepath.Join(path, "Manifest.xml"))
	}
	node := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1", "testSetManifest")

	var m testset.Manifest
	m.Name = node.As("", "name")
	m.Title = node.As("", "title")

	for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1", "creators") {
		m.Creators = append(m.Creators, x.GetValue())
	}

	return m

}

// XML2PackageManifest reads in a test package manifest (only the used values in build the package list)
func XML2PackageManifest(path string) Manifest {
	doc := xmlx.New()
	var err = doc.LoadFile(filepath.Join(path, "Manifest.xml"), nil)
	if err != nil {
		log.Error.Fatalln("Could not read XML file", filepath.Join(path, "Manifest.xml"))
	}

	node := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/TestPackageManifest1-0", "testPackageManifest")
	var pm Manifest
	pm.ID = node.As("", "id")
	pm.Name = node.As("", "name")
	pm.Title = node.As("", "title")
	pm.ReleasedAt = node.As("", "releasedAt")
	x := doc.SelectNode("http://leitstelle.p23r.de/NS/p23r/nrl/TestPackageManifest1-0", "release")
	pm.Release = x.GetValue()
	for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/TestPackageManifest1-0", "subjects") {
		pm.Subjects = append(pm.Subjects, x.GetValue())
	}
	for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/TestPackageManifest1-0", "descriptions") {
		var d Description
		d.Lang = x.As("", "lang")
		d.Text = x.GetValue()
		pm.Descriptions = append(pm.Descriptions, d)
	}
	pm.SpecificationVersion = "1.5"
	/*
		for _, x := range doc.SelectNodes("http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1", "subjects") {
			pm.Subjects = append(pm.Subjects, x.GetValue())
		}
	*/
	return pm

}

func testPackageManifest2XML(m *Manifest) []byte {
	xml := `<?xml version="1.0" encoding="UTF-8"?>
	<testPackageManifest xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/TestPackageManifest1-0"
	xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1"
`
	if m.ID != "" {
		xml += "\n  id=\"" + m.ID + `"`
	}
	if m.Name != "" {
		xml += "\n  name=\"" + m.Name + `"`
	}
	if m.Title != "" {
		xml += "\n  title=\"" + m.Title + `"`
	}
	if m.Publisher != "" {
		xml += "\n  publisher=\"" + m.Publisher + `"`
	}
	if m.ReleasedAt != "" {
		xml += "\n  releasedAt=\"" + m.ReleasedAt + `"`
	}
	xml += ">"
	if m.Release != "" {
		xml += "\n  <release>" + m.Release + "</release>"
	}
	for _, x := range m.Creators {
		xml += "\n  <creators>" + x + "</creators>"
	}
	for _, x := range m.Descriptions {
		xml += "\n  <descriptions lang=\"" + x.Lang + "\">" + x.Text + "</descriptions>"
	}
	/*
		for _, x := range m.Targets {
			xml += "\n  <targets lang=\"" + x.Lang + "\">" + x.Text + "</targets>"
		}
		for _, x := range m.Licenses {
			xml += "\n  <licenses lang=\"" + x.Lang + "\">" + x.Text + "</licenses>"
		}
	*/
	xml += "\n</testPackageManifest>"
	return []byte(xml)
}
