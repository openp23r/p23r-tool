package modelPackage

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-tool/helper"
	"gitlab.com/openp23r/p23r-tool/model"
	"gitlab.com/openp23r/p23r-tool/profile"

	"github.com/jhoonb/archivex"
	"github.com/odeke-em/go-uuid"
)

// Parameter contains the context values for the content
type Parameter struct {
	ModelPackageVersion string
}

func createFile(path, fileName string, parameter *Parameter, prof *profile.Profile, modelManifest model.ModelManifest) {
	var err error
	var mp Manifest
	mp.ID = uuid.New()
	mp.Name = prof.PackageName
	mp.Title = "Projektleistelle"
	mp.ReleasedAt = time.Now().Format(time.RFC3339)
	mp.Publisher = "Fraunhofer Fokus"
	mp.Release = parameter.ModelPackageVersion
	mp.Subjects = modelManifest.Subjects
	mp.Creators = append(mp.Creators, prof.Email)
	mp.SpecificationVersion = "1.5"
	var d Description
	d.Lang = "de-DE"
	d.Text = ""
	mp.Descriptions = append(mp.Descriptions, d)
	var t Description
	t.Lang = "de-DE"
	t.Text = "Entwickler"
	mp.Targets = append(mp.Targets, t)
	var l Description
	l.Lang = "de-DE"
	l.Text = "Es gilt die Lizenz des Entwicklerportals"
	mp.Licenses = append(mp.Licenses, l)

	c := modelPackageManifest2XML(&mp)

	err = ioutil.WriteFile(filepath.Join(path, fileName), c, 0644)
	if err != nil {
		log.Error.Fatal(err)
	}
	log.Info.Println("end of manifest creation in directory", path)
}

// GeneratePackage generates all directories and files nescessary for a P23R
// model package.
//
func GeneratePackage(prof *profile.Profile, path, generatePath, version string, noDelete, verbose bool) {
	// Create directories
	groundPath := filepath.Join(generatePath, "dmp", prof.PackageName, version)
	err := os.MkdirAll(groundPath, os.ModeDir|0700)
	if err != nil {
		// println("F")
		log.Error.Fatal(err)
	}
	log.Info.Println("groundPath:", groundPath)

	// data, err := ioutil.ReadFile(filepath.Join(path, "Manifest.xml"))

	var m model.ModelManifest
	m = Unmarshall(path)

	createFile(groundPath, "Manifest.xml", &Parameter{ModelPackageVersion: version}, prof, m)

	// copy files and directories
	if err = helper.CopyDir(path, filepath.Join(groundPath, m.Name)); err != nil {
		log.Error.Fatalln("in copy directory", path, "to", filepath.Join(groundPath, m.Name))

	}

	log.Info.Println("generate zip file:", filepath.Join(generatePath, "DMP-"+prof.PackageName+".zip"))
	zip := new(archivex.ZipFile)
	zip.Create(filepath.Join(generatePath, "DMP-"+prof.PackageName+".zip"))
	zip.AddAll(filepath.Join(generatePath, "dmp", prof.PackageName), true)
	zip.Close()

}
