// TestParserSelector
// 	Given an absolute selector
// 		When a model definition statement is given
// 			It should be accepted
// 		When multiple model definition statements are given
// 			It should be accepted
// 		When an absolute selector outside a query statement is given
// 			It should be accepted
// 		When an absolute selector with a path outside a query statement is given
// 			It should be accepted
// 		When an absolute selector with a path and an attribute outside a query statement is given
// 			It should be accepted
// 	Given a source selector
// 		When a source definition statement is given
// 			It should be accepted
// 		When multiple source definition statements are given
// 			It should not be accepted
// 		When a source selector outside a query statement is given
// 			It should be accepted
// 		When a source selector with a path outside a query statement is given
// 			It should be accepted
// 		When an element using a path with the source selector is given
// 			It should be accepted
// 		When a source selector accessing the values of a node outside a query statement is given
// 			It should be accepted
// 		When a text content using a path with the source selector is given
// 			It should be accepted
// 		When a source selector accessing all elements outside a query statement is given
// 			It should be accepted
// 		When an element using a wildcard path with the source selector is given
// 			It should be accepted
// 		When a source selector with a path and an attribute outside a query statement is given
// 			It should be accepted
// 		When an attribute using a path with the source selector is given
// 			It should be accepted
// 	Given a variable
// 		When an undefined variable is given
// 			It should not be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserSelector(t *testing.T) {

	Convey("Given an absolute selector", t, func() {

		Convey("When a model definition statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	model Xyz=http://www.pp.de/NS/22/hs
	result {
	}
`), ShouldBeNil)
			})

		})

		Convey("When multiple model definition statements are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	model Xyz = http://www.pp.de/NS/22/hs
	model Uvw=http://www.pp.de/NS/23/hs

	result {
	}
`), ShouldBeNil)
			})

		})

		Convey("When an absolute selector outside a query statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	model Xyz=http://www.pp.de/NS/22/hs
	result {
		Xyz
	}
`), ShouldBeNil)
			})

		})

		Convey("When an absolute selector with a path outside a query statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	model Xyz=http://www.pp.de/NS/22/hs
	result {
		Xyz.a
	}
`), ShouldBeNil)
			})

		})

		Convey("When an absolute selector with a path and an attribute outside a query statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	model Xyz=http://www.pp.de/NS/22/hs
	result {
		Xyz.a@b
	}
`), ShouldBeNil)
			})

		})

		Convey("When an unknown absolute selector is given", func() {

			Convey("It should not be accepted", func() {
				So(ParseDats(`
 
	model Xyz=http://www.pp.de/NS/22/hs
	result {
		Xyyz.a@b
	}
`), ShouldNotBeNil)
			})

		})

	})

	Convey("Given a source selector", t, func() {

		Convey("When a source definition statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://www.pp.de/NS/rule0815/source
	result {
	}
`), ShouldBeNil)
			})

		})

		Convey("When multiple source definition statements are given", func() {

			Convey("It should not be accepted", func() {
				So(ParseDats(`
 
	source = http://processor.p23r.de/rule0815/source
	source = http://processor.p23r.de/rule007/source

	result {
	}
`), ShouldNotBeNil)
			})

		})

		Convey("When a source selector outside a query statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://processor.p23r.de/rule0815/source
	result {
		$
	}
`), ShouldBeNil)
			})

		})

		Convey("When a source selector with a path outside a query statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`source = http://processor.p23r.de/rule0815/source
		result {
			$.a.b.c
		}
	`), ShouldBeNil)
			})

		})

		Convey("When an element using a path with the source selector is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://processor.p23r.de/rule0815/source
	result {
		$.a.b
	}
`), ShouldBeNil)
			})

		})

		Convey("When a source selector accessing the values of a node outside a query statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://processor.p23r.de/rule0815/source
	result {
		$.a@
	}
`), ShouldBeNil)
			})

		})

		Convey("When a text content using a path with the source selector is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://processor.p23r.de/rule0815/source
	result {
		$.a@
	}
`), ShouldBeNil)
			})

		})

		Convey("When a source selector accessing all elements outside a query statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://www.pp.de/NS/rule0817/source
	result {
		$.a.*
	}
`), ShouldBeNil)
			})

		})

		Convey("When an element using a wildcard path with the source selector is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://www.pp.de/NS/rule0817/source
	result {
		$.a.*
	}
`), ShouldBeNil)
			})

		})

		Convey("When a source selector with a path and an attribute outside a query statement is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://www.pp.de/NS/rule0817/source
	result {
		$.a.b@c
	}
`), ShouldBeNil)
			})

		})

		Convey("When an attribute using a path with the source selector is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	source = http://www.pp.de/NS/rule0817/source
	result {
		$.a.b@c
	}
`), ShouldBeNil)
			})

		})

	})

	Convey("Given a variable", t, func() {

		Convey("When an undefined variable is given", func() {

			Convey("It should not be accepted", func() {
				So(ParseDats(`
 
	result {
		ufo
	}
`), ShouldNotBeNil)
			})

		})

	})

}
