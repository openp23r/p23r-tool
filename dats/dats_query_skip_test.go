// TestParserQuerySkip
// 	Given local queries including skip and take in a selection expression for the processor
// 		When a number of results in a query should be skipped
// 			It should be accepted
// 		When a limited number of results in a query should be returned
// 			It should be accepted
// 		When a range of results in a query should be returned
// 			It should be accepted
// 		When a negative take of results in a query is given
// 			It should be accepted
// 		When a negative skip of results in a query is given
// 			It should be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserQuerySkip(t *testing.T) {

	Convey("Given local queries including skip and take in a selection expression for the processor", t, func() {

		Convey("When a number of results in a query should be skipped", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							skip 1
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a limited number of results in a query should be returned", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							take 1
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a range of results in a query should be returned", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							skip 1
							take 1
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a negative take of results in a query is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in P23R:range(1,4)
							take -2
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a negative skip of results in a query is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in P23R:range(1,5)
							skip -2
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

	})

}
