// TestParserQueryFor
// 	Given local queries using for and return in a selection expression for the processor
// 		When a simple query statement which copies the source data is given
// 			It should be accepted
// 		When the source in a query which copies the source data is given
// 			It should be accepted
// 		When the source including a path in a query is given
// 			It should be accepted
// 		When the source requesting the text content in a query is given
// 			It should be accepted
// 		When the source including an attribute in a query
// 			It should be accepted
// 		When the profile in a query which copies the profile data is given
// 			It should be accepted
// 		When an element from a query using a path is given
// 			It should be accepted
// 		When an attribute value from a query using a path is given
// 			It should be accepted
// 		When a method on the returning value from a query is given
// 			It should be accepted
// 		When a constant value from a query is given
// 			It should be accepted
// 		When a method on the query result is given
// 			It should be accepted
// 		When a query inside an attribute is given
// 			It should be accepted
// 		When a function call inside an attribute is given
// 			It should be accepted
// Given nested queries using chaining in a selection expression for the processor
// 	When a range of values from a query is given
// 		It should be accepted
// 	When a range of values from a chained query is given
// 		It should be accepted
// 	When a range of values from a nested query is given
// 		It should be accepted
// 	When sum up a range of values from a nested query
// 		It should be accepted
// 	When identifying undefined variables outside a query (1)
// 		It should not be accepted
// 	When identifying undefined variables outside a query (2)
// 		It should be accepted
// When requesting data from a source connector
// 	It should be accepted
// When requesting a xml document with a namespace
// 	It should be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserQueryFor(t *testing.T) {

	Convey("Given local queries using for and return in a selection expression for the processor", t, func() {

		Convey("When a simple query statement which copies the source data is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When the source in a query which copies the source data is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When the source including a path in a query is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When the source requesting the text content in a query is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d@
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When the source including an attribute in a query", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.b@c
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When the profile in a query which copies the profile data is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When an element from a query using a path is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x
							return x.profile.rule
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When an attribute value from a query using a path is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x
							return x.profile.rule@name
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a method on the returning value from a query is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x
							return x.profile.rule@name.ToUpper()
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a constant value from a query is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for each x in $.a.d
							return "static"
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a method on the query result is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							(
								for each x in $.a.d
								return "2"
							).Count()
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a query inside an attribute is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

					source = http://www.pp.de/NS/rule0817/source
					result {
						@c {
							[
								for each x in $.a.b@c
								return x
							]
						}
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a function call inside an attribute is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						@c { range(1,3) }
					}
				`), ShouldBeNil)
			})

		})

	})

	Convey("Given nested queries using chaining in a selection expression for the processor", t, func() {

		Convey("When returning a range of values from a query", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					result {
						[
							for x in P23R:range(1,3)
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When returning a range of values from a chained query", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					result {
						[
							for x in P23R:range(1,3)
							for y in P23R:range(1,2)
							return x + y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a range of values from a nested query is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					result {
						[
							for x in P23R:range(1,3)
							return [
								for y in P23R:range(1,2)
								return x * y
							]
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When sum up a range of values from a nested query", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					result {
						[
							for x in P23R:range(1,3)
							return 10 + [(
								for y in P23R:range(1,2)
								return x * y
							).Sum()]
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When identifying undefined variables outside a query (1)", func() {

			Convey("It should not be accepted", func() {
				So(ParseDats(`
				 
					result {
						x
						[
							for x in P23R:range(1,3)
							return x
						]
					}
				`), ShouldNotBeNil)
			})

		})

		Convey("When identifying undefined variables outside a query (2)", func() {

			Convey("It should not be accepted", func() {
				So(ParseDats(`
				 
					result {
						[
							for x in P23R:range(1,3)
							return x
						]
						x
					}
				`), ShouldNotBeNil)
			})

		})

		Convey("When requesting data from a source connector", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					model X = http://examples.p23r.de/NS/X
					result {
						[
							for x in X
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When requesting a xml document with a namespace", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					model X = http://examples.p23r.de/NS/X
					result {
						[
							for x in X
							return tag {
								123
								"hugo"
								@xmlns { "http://leitstelle.p23r.de/NS/Selection1-0" }
							}
						]
					}
				`), ShouldBeNil)
			})

		})

	})

}
