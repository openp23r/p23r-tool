package dats

import (
	"fmt"
	"math"
	"sort"
	"strconv"
)

const end_symbol rune = 1114112

/* The rule types inferred from the grammar are below. */
type pegRule uint8

const (
	ruleUnknown pegRule = iota
	ruleGrammar
	ruleIdentifier
	ruleLowercaseIdentifier
	ruleUppercaseIdentifier
	ruleDOMAINID
	ruleHASH
	ruleFILE
	ruleSOURCE
	ruleMODEL
	ruleFOR
	ruleEACH
	ruleIN
	ruleWHERE
	ruleDISTINCT
	ruleORDER
	ruleBY
	ruleASCENDING
	ruleDESCENDING
	ruleTAKE
	ruleSKIP
	ruleLET
	ruleRETURN
	ruleTAB
	ruleSP
	ruleWS
	ruleEOL
	ruleWSEOL
	rule__
	rule_
	ruleEOF
	ruleEMPTYLINE
	ruleNOCOMMENTLINESTART
	ruleCOMMENTLINEEND
	ruleCOMMENTLINECHAR
	ruleCOMMENTLINE
	ruleCOMMENTBLOCK
	ruleCOMMENTS
	ruleSTARTCOMMENT
	ruleFINALCOMMENTS
	ruleDIGIT
	ruleCARDINAL
	ruleINTEGER
	ruleEXPONENT
	ruleFLOAT
	ruleDATE
	ruleTIME
	ruleDATETIME
	ruleSTRING
	ruleREGEX
	ruleOP
	ruleBOOLOP
	ruleCOMPARATOR
	ruleMATCHER
	ruleNOTOP
	ruleMETHODNAME
	ruleMETHOD
	ruleURL
	rulePROTOCOL
	ruleHOST
	rulePARTIALIPNUMBER
	rulePARTIALDOMAIN
	rulePORT
	rulePATH
	ruleSelection
	ruleSourceDeclaration
	ruleModelDeclaration
	ruleElement
	ruleElementIdentifier
	ruleElementContent
	ruleAttribute
	ruleAttributeIdentifier
	ruleAttributeContent
	ruleSelector
	ruleRelativeSelector
	ruleAbsoluteSelector
	ruleSourceSelector
	ruleBoolExpr
	ruleCondition
	ruleExpr
	ruleValue
	ruleExprExtension
	ruleFunction
	ruleArgs
	ruleQuery
	ruleFLOWR
	ruleForeachExpr
	ruleWhereExpr
	ruleDistinctExpr
	ruleOrderByExpr
	ruleTakeExpr
	ruleSkipExpr
	ruleLetExpr
	ruleReturnExpr
	ruleAction0
	rulePegText
	ruleAction1
	ruleAction2
	ruleAction3
	ruleAction4
	ruleAction5
	ruleAction6
	ruleAction7

	rulePre_
	rule_In_
	rule_Suf
)

var rul3s = [...]string{
	"Unknown",
	"Grammar",
	"Identifier",
	"LowercaseIdentifier",
	"UppercaseIdentifier",
	"DOMAINID",
	"HASH",
	"FILE",
	"SOURCE",
	"MODEL",
	"FOR",
	"EACH",
	"IN",
	"WHERE",
	"DISTINCT",
	"ORDER",
	"BY",
	"ASCENDING",
	"DESCENDING",
	"TAKE",
	"SKIP",
	"LET",
	"RETURN",
	"TAB",
	"SP",
	"WS",
	"EOL",
	"WSEOL",
	"__",
	"_",
	"EOF",
	"EMPTYLINE",
	"NOCOMMENTLINESTART",
	"COMMENTLINEEND",
	"COMMENTLINECHAR",
	"COMMENTLINE",
	"COMMENTBLOCK",
	"COMMENTS",
	"STARTCOMMENT",
	"FINALCOMMENTS",
	"DIGIT",
	"CARDINAL",
	"INTEGER",
	"EXPONENT",
	"FLOAT",
	"DATE",
	"TIME",
	"DATETIME",
	"STRING",
	"REGEX",
	"OP",
	"BOOLOP",
	"COMPARATOR",
	"MATCHER",
	"NOTOP",
	"METHODNAME",
	"METHOD",
	"URL",
	"PROTOCOL",
	"HOST",
	"PARTIALIPNUMBER",
	"PARTIALDOMAIN",
	"PORT",
	"PATH",
	"Selection",
	"SourceDeclaration",
	"ModelDeclaration",
	"Element",
	"ElementIdentifier",
	"ElementContent",
	"Attribute",
	"AttributeIdentifier",
	"AttributeContent",
	"Selector",
	"RelativeSelector",
	"AbsoluteSelector",
	"SourceSelector",
	"BoolExpr",
	"Condition",
	"Expr",
	"Value",
	"ExprExtension",
	"Function",
	"Args",
	"Query",
	"FLOWR",
	"ForeachExpr",
	"WhereExpr",
	"DistinctExpr",
	"OrderByExpr",
	"TakeExpr",
	"SkipExpr",
	"LetExpr",
	"ReturnExpr",
	"Action0",
	"PegText",
	"Action1",
	"Action2",
	"Action3",
	"Action4",
	"Action5",
	"Action6",
	"Action7",

	"Pre_",
	"_In_",
	"_Suf",
}

type tokenTree interface {
	Print()
	PrintSyntax()
	PrintSyntaxTree(buffer string)
	Add(rule pegRule, begin, end, next uint32, depth int)
	Expand(index int) tokenTree
	Tokens() <-chan token32
	AST() *node32
	Error() []token32
	trim(length int)
}

type node32 struct {
	token32
	up, next *node32
}

func (node *node32) print(depth int, buffer string) {
	for node != nil {
		for c := 0; c < depth; c++ {
			fmt.Printf(" ")
		}
		fmt.Printf("\x1B[34m%v\x1B[m %v\n", rul3s[node.pegRule], strconv.Quote(string(([]rune(buffer)[node.begin:node.end]))))
		if node.up != nil {
			node.up.print(depth+1, buffer)
		}
		node = node.next
	}
}

func (ast *node32) Print(buffer string) {
	ast.print(0, buffer)
}

type element struct {
	node *node32
	down *element
}

/* ${@} bit structure for abstract syntax tree */
type token32 struct {
	pegRule
	begin, end, next uint32
}

func (t *token32) isZero() bool {
	return t.pegRule == ruleUnknown && t.begin == 0 && t.end == 0 && t.next == 0
}

func (t *token32) isParentOf(u token32) bool {
	return t.begin <= u.begin && t.end >= u.end && t.next > u.next
}

func (t *token32) getToken32() token32 {
	return token32{pegRule: t.pegRule, begin: uint32(t.begin), end: uint32(t.end), next: uint32(t.next)}
}

func (t *token32) String() string {
	return fmt.Sprintf("\x1B[34m%v\x1B[m %v %v %v", rul3s[t.pegRule], t.begin, t.end, t.next)
}

type tokens32 struct {
	tree    []token32
	ordered [][]token32
}

func (t *tokens32) trim(length int) {
	t.tree = t.tree[0:length]
}

func (t *tokens32) Print() {
	for _, token := range t.tree {
		fmt.Println(token.String())
	}
}

func (t *tokens32) Order() [][]token32 {
	if t.ordered != nil {
		return t.ordered
	}

	depths := make([]int32, 1, math.MaxInt16)
	for i, token := range t.tree {
		if token.pegRule == ruleUnknown {
			t.tree = t.tree[:i]
			break
		}
		depth := int(token.next)
		if length := len(depths); depth >= length {
			depths = depths[:depth+1]
		}
		depths[depth]++
	}
	depths = append(depths, 0)

	ordered, pool := make([][]token32, len(depths)), make([]token32, len(t.tree)+len(depths))
	for i, depth := range depths {
		depth++
		ordered[i], pool, depths[i] = pool[:depth], pool[depth:], 0
	}

	for i, token := range t.tree {
		depth := token.next
		token.next = uint32(i)
		ordered[depth][depths[depth]] = token
		depths[depth]++
	}
	t.ordered = ordered
	return ordered
}

type state32 struct {
	token32
	depths []int32
	leaf   bool
}

func (t *tokens32) AST() *node32 {
	tokens := t.Tokens()
	stack := &element{node: &node32{token32: <-tokens}}
	for token := range tokens {
		if token.begin == token.end {
			continue
		}
		node := &node32{token32: token}
		for stack != nil && stack.node.begin >= token.begin && stack.node.end <= token.end {
			stack.node.next = node.up
			node.up = stack.node
			stack = stack.down
		}
		stack = &element{node: node, down: stack}
	}
	return stack.node
}

func (t *tokens32) PreOrder() (<-chan state32, [][]token32) {
	s, ordered := make(chan state32, 6), t.Order()
	go func() {
		var states [8]state32
		for i, _ := range states {
			states[i].depths = make([]int32, len(ordered))
		}
		depths, state, depth := make([]int32, len(ordered)), 0, 1
		write := func(t token32, leaf bool) {
			S := states[state]
			state, S.pegRule, S.begin, S.end, S.next, S.leaf = (state+1)%8, t.pegRule, t.begin, t.end, uint32(depth), leaf
			copy(S.depths, depths)
			s <- S
		}

		states[state].token32 = ordered[0][0]
		depths[0]++
		state++
		a, b := ordered[depth-1][depths[depth-1]-1], ordered[depth][depths[depth]]
	depthFirstSearch:
		for {
			for {
				if i := depths[depth]; i > 0 {
					if c, j := ordered[depth][i-1], depths[depth-1]; a.isParentOf(c) &&
						(j < 2 || !ordered[depth-1][j-2].isParentOf(c)) {
						if c.end != b.begin {
							write(token32{pegRule: rule_In_, begin: c.end, end: b.begin}, true)
						}
						break
					}
				}

				if a.begin < b.begin {
					write(token32{pegRule: rulePre_, begin: a.begin, end: b.begin}, true)
				}
				break
			}

			next := depth + 1
			if c := ordered[next][depths[next]]; c.pegRule != ruleUnknown && b.isParentOf(c) {
				write(b, false)
				depths[depth]++
				depth, a, b = next, b, c
				continue
			}

			write(b, true)
			depths[depth]++
			c, parent := ordered[depth][depths[depth]], true
			for {
				if c.pegRule != ruleUnknown && a.isParentOf(c) {
					b = c
					continue depthFirstSearch
				} else if parent && b.end != a.end {
					write(token32{pegRule: rule_Suf, begin: b.end, end: a.end}, true)
				}

				depth--
				if depth > 0 {
					a, b, c = ordered[depth-1][depths[depth-1]-1], a, ordered[depth][depths[depth]]
					parent = a.isParentOf(b)
					continue
				}

				break depthFirstSearch
			}
		}

		close(s)
	}()
	return s, ordered
}

func (t *tokens32) PrintSyntax() {
	tokens, ordered := t.PreOrder()
	max := -1
	for token := range tokens {
		if !token.leaf {
			fmt.Printf("%v", token.begin)
			for i, leaf, depths := 0, int(token.next), token.depths; i < leaf; i++ {
				fmt.Printf(" \x1B[36m%v\x1B[m", rul3s[ordered[i][depths[i]-1].pegRule])
			}
			fmt.Printf(" \x1B[36m%v\x1B[m\n", rul3s[token.pegRule])
		} else if token.begin == token.end {
			fmt.Printf("%v", token.begin)
			for i, leaf, depths := 0, int(token.next), token.depths; i < leaf; i++ {
				fmt.Printf(" \x1B[31m%v\x1B[m", rul3s[ordered[i][depths[i]-1].pegRule])
			}
			fmt.Printf(" \x1B[31m%v\x1B[m\n", rul3s[token.pegRule])
		} else {
			for c, end := token.begin, token.end; c < end; c++ {
				if i := int(c); max+1 < i {
					for j := max; j < i; j++ {
						fmt.Printf("skip %v %v\n", j, token.String())
					}
					max = i
				} else if i := int(c); i <= max {
					for j := i; j <= max; j++ {
						fmt.Printf("dupe %v %v\n", j, token.String())
					}
				} else {
					max = int(c)
				}
				fmt.Printf("%v", c)
				for i, leaf, depths := 0, int(token.next), token.depths; i < leaf; i++ {
					fmt.Printf(" \x1B[34m%v\x1B[m", rul3s[ordered[i][depths[i]-1].pegRule])
				}
				fmt.Printf(" \x1B[34m%v\x1B[m\n", rul3s[token.pegRule])
			}
			fmt.Printf("\n")
		}
	}
}

func (t *tokens32) PrintSyntaxTree(buffer string) {
	tokens, _ := t.PreOrder()
	for token := range tokens {
		for c := 0; c < int(token.next); c++ {
			fmt.Printf(" ")
		}
		fmt.Printf("\x1B[34m%v\x1B[m %v\n", rul3s[token.pegRule], strconv.Quote(string(([]rune(buffer)[token.begin:token.end]))))
	}
}

func (t *tokens32) Add(rule pegRule, begin, end, depth uint32, index int) {
	t.tree[index] = token32{pegRule: rule, begin: uint32(begin), end: uint32(end), next: uint32(depth)}
}

func (t *tokens32) Tokens() <-chan token32 {
	s := make(chan token32, 16)
	go func() {
		for _, v := range t.tree {
			s <- v.getToken32()
		}
		close(s)
	}()
	return s
}

func (t *tokens32) Error() []token32 {
	ordered := t.Order()
	length := len(ordered)
	tokens, length := make([]token32, length), length-1
	for i, _ := range tokens {
		o := ordered[length-i]
		if len(o) > 1 {
			tokens[i] = o[len(o)-2].getToken32()
		}
	}
	return tokens
}

/*func (t *tokens16) Expand(index int) tokenTree {
	tree := t.tree
	if index >= len(tree) {
		expanded := make([]token32, 2 * len(tree))
		for i, v := range tree {
			expanded[i] = v.getToken32()
		}
		return &tokens32{tree: expanded}
	}
	return nil
}*/

func (t *tokens32) Expand(index int) tokenTree {
	tree := t.tree
	if index >= len(tree) {
		expanded := make([]token32, 2*len(tree))
		copy(expanded, tree)
		t.tree = expanded
	}
	return nil
}

type Dats struct {
	env            Environment
	vars           EnvironmentVars
	models         EnvironmentModels
	identifierName string

	Buffer string
	buffer []rune
	rules  [103]func() bool
	Parse  func(rule ...int) error
	Reset  func()
	tokenTree
}

type textPosition struct {
	line, symbol int
}

type textPositionMap map[int]textPosition

func translatePositions(buffer string, positions []int) textPositionMap {
	length, translations, j, line, symbol := len(positions), make(textPositionMap, len(positions)), 0, 1, 0
	sort.Ints(positions)

search:
	for i, c := range []rune(buffer) {
		if c == '\n' {
			line, symbol = line+1, 0
		} else {
			symbol++
		}
		if i == positions[j] {
			translations[positions[j]] = textPosition{line, symbol}
			for j++; j < length; j++ {
				if i != positions[j] {
					continue search
				}
			}
			break search
		}
	}

	return translations
}

type parseError struct {
	p *Dats
}

func (e *parseError) Error() string {
	tokens, error := e.p.tokenTree.Error(), "\n"
	positions, p := make([]int, 2*len(tokens)), 0
	for _, token := range tokens {
		positions[p], p = int(token.begin), p+1
		positions[p], p = int(token.end), p+1
	}
	translations := translatePositions(e.p.Buffer, positions)
	for _, token := range tokens {
		begin, end := int(token.begin), int(token.end)
		error += fmt.Sprintf("parse error near \x1B[34m%v\x1B[m (line %v symbol %v - line %v symbol %v):\n%v\n",
			rul3s[token.pegRule],
			translations[begin].line, translations[begin].symbol,
			translations[end].line, translations[end].symbol,
			/*strconv.Quote(*/ e.p.Buffer[begin:end] /*)*/)
	}

	return error
}

func (p *Dats) PrintSyntaxTree() {
	p.tokenTree.PrintSyntaxTree(p.Buffer)
}

func (p *Dats) Highlighter() {
	p.tokenTree.PrintSyntax()
}

func (p *Dats) Execute() {
	buffer, _buffer, text, begin, end := p.Buffer, p.buffer, "", 0, 0
	for token := range p.tokenTree.Tokens() {
		switch token.pegRule {

		case rulePegText:
			begin, end = int(token.begin), int(token.end)
			text = string(_buffer[begin:end])

		case ruleAction0:

			// p.models.Print()
			// p.vars.Print()

		case ruleAction1:
			p.models.Push(text)
		case ruleAction2:
			p.vars.Check(text)
		case ruleAction3:
			p.models.Check(text)
		case ruleAction4:
			p.env.Push(p.vars, p.models)
		case ruleAction5:
			p.vars, p.models = p.env.Pop()
		case ruleAction6:
			p.vars.Push(text)
		case ruleAction7:
			p.vars.Push(text)

		}
	}
	_, _, _, _, _ = buffer, _buffer, text, begin, end
}

func (p *Dats) Init() {
	p.buffer = []rune(p.Buffer)
	if len(p.buffer) == 0 || p.buffer[len(p.buffer)-1] != end_symbol {
		p.buffer = append(p.buffer, end_symbol)
	}

	var tree tokenTree = &tokens32{tree: make([]token32, math.MaxInt16)}
	position, depth, tokenIndex, buffer, _rules := uint32(0), uint32(0), 0, p.buffer, p.rules

	p.Parse = func(rule ...int) error {
		r := 1
		if len(rule) > 0 {
			r = rule[0]
		}
		matches := p.rules[r]()
		p.tokenTree = tree
		if matches {
			p.tokenTree.trim(tokenIndex)
			return nil
		}
		return &parseError{p}
	}

	p.Reset = func() {
		position, tokenIndex, depth = 0, 0, 0
	}

	add := func(rule pegRule, begin uint32) {
		if t := tree.Expand(tokenIndex); t != nil {
			tree = t
		}
		tree.Add(rule, begin, position, depth, tokenIndex)
		tokenIndex++
	}

	matchDot := func() bool {
		if buffer[position] != end_symbol {
			position++
			return true
		}
		return false
	}

	/*matchChar := func(c byte) bool {
		if buffer[position] == c {
			position++
			return true
		}
		return false
	}*/

	/*matchRange := func(lower byte, upper byte) bool {
		if c := buffer[position]; c >= lower && c <= upper {
			position++
			return true
		}
		return false
	}*/

	_rules = [...]func() bool{
		nil,
		/* 0 Grammar <- <(STARTCOMMENT Selection? _ FINALCOMMENTS _ EOF Action0)> */
		func() bool {
			position0, tokenIndex0, depth0 := position, tokenIndex, depth
			{
				position1 := position
				depth++
				{
					position2 := position
					depth++
				l3:
					{
						position4, tokenIndex4, depth4 := position, tokenIndex, depth
						if !_rules[ruleEMPTYLINE]() {
							goto l4
						}
						goto l3
					l4:
						position, tokenIndex, depth = position4, tokenIndex4, depth4
					}
				l5:
					{
						position6, tokenIndex6, depth6 := position, tokenIndex, depth
						if !_rules[ruleCOMMENTBLOCK]() {
							goto l6
						}
						goto l5
					l6:
						position, tokenIndex, depth = position6, tokenIndex6, depth6
					}
					{
						position7, tokenIndex7, depth7 := position, tokenIndex, depth
						if !_rules[ruleTAB]() {
							goto l8
						}
						goto l7
					l8:
						position, tokenIndex, depth = position7, tokenIndex7, depth7
					l9:
						{
							position10, tokenIndex10, depth10 := position, tokenIndex, depth
							if !_rules[ruleCOMMENTS]() {
								goto l10
							}
							goto l9
						l10:
							position, tokenIndex, depth = position10, tokenIndex10, depth10
						}
					}
				l7:
					depth--
					add(ruleSTARTCOMMENT, position2)
				}
				{
					position11, tokenIndex11, depth11 := position, tokenIndex, depth
					{
						position13 := position
						depth++
						{
							position14, tokenIndex14, depth14 := position, tokenIndex, depth
							{
								position16 := position
								depth++
								{
									position17, tokenIndex17, depth17 := position, tokenIndex, depth
									if !_rules[ruleCOMMENTS]() {
										goto l17
									}
									goto l18
								l17:
									position, tokenIndex, depth = position17, tokenIndex17, depth17
								}
							l18:
								if !_rules[rule_]() {
									goto l14
								}
								{
									position19 := position
									depth++
									{
										position20, tokenIndex20, depth20 := position, tokenIndex, depth
										if buffer[position] != rune('s') {
											goto l21
										}
										position++
										goto l20
									l21:
										position, tokenIndex, depth = position20, tokenIndex20, depth20
										if buffer[position] != rune('S') {
											goto l14
										}
										position++
									}
								l20:
									{
										position22, tokenIndex22, depth22 := position, tokenIndex, depth
										if buffer[position] != rune('o') {
											goto l23
										}
										position++
										goto l22
									l23:
										position, tokenIndex, depth = position22, tokenIndex22, depth22
										if buffer[position] != rune('O') {
											goto l14
										}
										position++
									}
								l22:
									{
										position24, tokenIndex24, depth24 := position, tokenIndex, depth
										if buffer[position] != rune('u') {
											goto l25
										}
										position++
										goto l24
									l25:
										position, tokenIndex, depth = position24, tokenIndex24, depth24
										if buffer[position] != rune('U') {
											goto l14
										}
										position++
									}
								l24:
									{
										position26, tokenIndex26, depth26 := position, tokenIndex, depth
										if buffer[position] != rune('r') {
											goto l27
										}
										position++
										goto l26
									l27:
										position, tokenIndex, depth = position26, tokenIndex26, depth26
										if buffer[position] != rune('R') {
											goto l14
										}
										position++
									}
								l26:
									{
										position28, tokenIndex28, depth28 := position, tokenIndex, depth
										if buffer[position] != rune('c') {
											goto l29
										}
										position++
										goto l28
									l29:
										position, tokenIndex, depth = position28, tokenIndex28, depth28
										if buffer[position] != rune('C') {
											goto l14
										}
										position++
									}
								l28:
									{
										position30, tokenIndex30, depth30 := position, tokenIndex, depth
										if buffer[position] != rune('e') {
											goto l31
										}
										position++
										goto l30
									l31:
										position, tokenIndex, depth = position30, tokenIndex30, depth30
										if buffer[position] != rune('E') {
											goto l14
										}
										position++
									}
								l30:
									depth--
									add(ruleSOURCE, position19)
								}
								if !_rules[rule_]() {
									goto l14
								}
								if buffer[position] != rune('=') {
									goto l14
								}
								position++
								if !_rules[rule_]() {
									goto l14
								}
								if !_rules[ruleURL]() {
									goto l14
								}
								depth--
								add(ruleSourceDeclaration, position16)
							}
							goto l15
						l14:
							position, tokenIndex, depth = position14, tokenIndex14, depth14
						}
					l15:
					l32:
						{
							position33, tokenIndex33, depth33 := position, tokenIndex, depth
							{
								position34 := position
								depth++
								{
									position35, tokenIndex35, depth35 := position, tokenIndex, depth
									if !_rules[ruleCOMMENTS]() {
										goto l35
									}
									goto l36
								l35:
									position, tokenIndex, depth = position35, tokenIndex35, depth35
								}
							l36:
								if !_rules[rule_]() {
									goto l33
								}
								{
									position37 := position
									depth++
									{
										position38, tokenIndex38, depth38 := position, tokenIndex, depth
										if buffer[position] != rune('m') {
											goto l39
										}
										position++
										goto l38
									l39:
										position, tokenIndex, depth = position38, tokenIndex38, depth38
										if buffer[position] != rune('M') {
											goto l33
										}
										position++
									}
								l38:
									{
										position40, tokenIndex40, depth40 := position, tokenIndex, depth
										if buffer[position] != rune('o') {
											goto l41
										}
										position++
										goto l40
									l41:
										position, tokenIndex, depth = position40, tokenIndex40, depth40
										if buffer[position] != rune('O') {
											goto l33
										}
										position++
									}
								l40:
									{
										position42, tokenIndex42, depth42 := position, tokenIndex, depth
										if buffer[position] != rune('d') {
											goto l43
										}
										position++
										goto l42
									l43:
										position, tokenIndex, depth = position42, tokenIndex42, depth42
										if buffer[position] != rune('D') {
											goto l33
										}
										position++
									}
								l42:
									{
										position44, tokenIndex44, depth44 := position, tokenIndex, depth
										if buffer[position] != rune('e') {
											goto l45
										}
										position++
										goto l44
									l45:
										position, tokenIndex, depth = position44, tokenIndex44, depth44
										if buffer[position] != rune('E') {
											goto l33
										}
										position++
									}
								l44:
									{
										position46, tokenIndex46, depth46 := position, tokenIndex, depth
										if buffer[position] != rune('l') {
											goto l47
										}
										position++
										goto l46
									l47:
										position, tokenIndex, depth = position46, tokenIndex46, depth46
										if buffer[position] != rune('L') {
											goto l33
										}
										position++
									}
								l46:
									depth--
									add(ruleMODEL, position37)
								}
								if !_rules[rule__]() {
									goto l33
								}
								if !_rules[ruleUppercaseIdentifier]() {
									goto l33
								}
								{
									add(ruleAction1, position)
								}
								if !_rules[rule_]() {
									goto l33
								}
								if buffer[position] != rune('=') {
									goto l33
								}
								position++
								if !_rules[rule_]() {
									goto l33
								}
								if !_rules[ruleURL]() {
									goto l33
								}
								depth--
								add(ruleModelDeclaration, position34)
							}
							goto l32
						l33:
							position, tokenIndex, depth = position33, tokenIndex33, depth33
						}
					l49:
						{
							position50, tokenIndex50, depth50 := position, tokenIndex, depth
							if !_rules[ruleElementContent]() {
								goto l50
							}
							goto l49
						l50:
							position, tokenIndex, depth = position50, tokenIndex50, depth50
						}
						depth--
						add(ruleSelection, position13)
					}
					goto l12

					position, tokenIndex, depth = position11, tokenIndex11, depth11
				}
			l12:
				if !_rules[rule_]() {
					goto l0
				}
				{
					position51 := position
					depth++
				l52:
					{
						position53, tokenIndex53, depth53 := position, tokenIndex, depth
						if !_rules[ruleEMPTYLINE]() {
							goto l53
						}
						goto l52
					l53:
						position, tokenIndex, depth = position53, tokenIndex53, depth53
					}
				l54:
					{
						position55, tokenIndex55, depth55 := position, tokenIndex, depth
						if !_rules[ruleCOMMENTBLOCK]() {
							goto l55
						}
						goto l54
					l55:
						position, tokenIndex, depth = position55, tokenIndex55, depth55
					}
					depth--
					add(ruleFINALCOMMENTS, position51)
				}
				if !_rules[rule_]() {
					goto l0
				}
				if !_rules[ruleEOF]() {
					goto l0
				}
				{
					add(ruleAction0, position)
				}
				depth--
				add(ruleGrammar, position1)
			}
			return true
		l0:
			position, tokenIndex, depth = position0, tokenIndex0, depth0
			return false
		},
		/* 1 Identifier <- <<(([a-z] / [A-Z]) ((&('_') '_') | (&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') [0-9]) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&('-') '-') | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z]))*)>> */
		nil,
		/* 2 LowercaseIdentifier <- <<([a-z] ((&('_') '_') | (&('-') '-') | (&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') ([0-9] / [0-9])) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z]))*)>> */
		func() bool {
			position58, tokenIndex58, depth58 := position, tokenIndex, depth
			{
				position59 := position
				depth++
				{
					position60 := position
					depth++
					if c := buffer[position]; c < rune('a') || c > rune('z') {
						goto l58
					}
					position++
				l61:
					{
						position62, tokenIndex62, depth62 := position, tokenIndex, depth
						{
							switch buffer[position] {
							case '_':
								if buffer[position] != rune('_') {
									goto l62
								}
								position++
								break
							case '-':
								if buffer[position] != rune('-') {
									goto l62
								}
								position++
								break
							case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
								{
									position64, tokenIndex64, depth64 := position, tokenIndex, depth
									if c := buffer[position]; c < rune('0') || c > rune('9') {
										goto l65
									}
									position++
									goto l64
								l65:
									position, tokenIndex, depth = position64, tokenIndex64, depth64
									if c := buffer[position]; c < rune('0') || c > rune('9') {
										goto l62
									}
									position++
								}
							l64:
								break
							case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
								if c := buffer[position]; c < rune('A') || c > rune('Z') {
									goto l62
								}
								position++
								break
							default:
								if c := buffer[position]; c < rune('a') || c > rune('z') {
									goto l62
								}
								position++
								break
							}
						}

						goto l61
					l62:
						position, tokenIndex, depth = position62, tokenIndex62, depth62
					}
					depth--
					add(rulePegText, position60)
				}
				depth--
				add(ruleLowercaseIdentifier, position59)
			}
			return true
		l58:
			position, tokenIndex, depth = position58, tokenIndex58, depth58
			return false
		},
		/* 3 UppercaseIdentifier <- <<([A-Z] ((&('_') '_') | (&('-') '-') | (&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') ([0-9] / [0-9])) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z]))*)>> */
		func() bool {
			position66, tokenIndex66, depth66 := position, tokenIndex, depth
			{
				position67 := position
				depth++
				{
					position68 := position
					depth++
					if c := buffer[position]; c < rune('A') || c > rune('Z') {
						goto l66
					}
					position++
				l69:
					{
						position70, tokenIndex70, depth70 := position, tokenIndex, depth
						{
							switch buffer[position] {
							case '_':
								if buffer[position] != rune('_') {
									goto l70
								}
								position++
								break
							case '-':
								if buffer[position] != rune('-') {
									goto l70
								}
								position++
								break
							case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
								{
									position72, tokenIndex72, depth72 := position, tokenIndex, depth
									if c := buffer[position]; c < rune('0') || c > rune('9') {
										goto l73
									}
									position++
									goto l72
								l73:
									position, tokenIndex, depth = position72, tokenIndex72, depth72
									if c := buffer[position]; c < rune('0') || c > rune('9') {
										goto l70
									}
									position++
								}
							l72:
								break
							case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
								if c := buffer[position]; c < rune('A') || c > rune('Z') {
									goto l70
								}
								position++
								break
							default:
								if c := buffer[position]; c < rune('a') || c > rune('z') {
									goto l70
								}
								position++
								break
							}
						}

						goto l69
					l70:
						position, tokenIndex, depth = position70, tokenIndex70, depth70
					}
					depth--
					add(rulePegText, position68)
				}
				depth--
				add(ruleUppercaseIdentifier, position67)
			}
			return true
		l66:
			position, tokenIndex, depth = position66, tokenIndex66, depth66
			return false
		},
		/* 4 DOMAINID <- <<(([a-z] / [A-Z]) ((&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') ([0-9] / [0-9])) | (&('-') '-') | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z' | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') ([a-z] / [A-Z])))*)>> */
		func() bool {
			position74, tokenIndex74, depth74 := position, tokenIndex, depth
			{
				position75 := position
				depth++
				{
					position76 := position
					depth++
					{
						position77, tokenIndex77, depth77 := position, tokenIndex, depth
						if c := buffer[position]; c < rune('a') || c > rune('z') {
							goto l78
						}
						position++
						goto l77
					l78:
						position, tokenIndex, depth = position77, tokenIndex77, depth77
						if c := buffer[position]; c < rune('A') || c > rune('Z') {
							goto l74
						}
						position++
					}
				l77:
				l79:
					{
						position80, tokenIndex80, depth80 := position, tokenIndex, depth
						{
							switch buffer[position] {
							case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
								{
									position82, tokenIndex82, depth82 := position, tokenIndex, depth
									if c := buffer[position]; c < rune('0') || c > rune('9') {
										goto l83
									}
									position++
									goto l82
								l83:
									position, tokenIndex, depth = position82, tokenIndex82, depth82
									if c := buffer[position]; c < rune('0') || c > rune('9') {
										goto l80
									}
									position++
								}
							l82:
								break
							case '-':
								if buffer[position] != rune('-') {
									goto l80
								}
								position++
								break
							default:
								{
									position84, tokenIndex84, depth84 := position, tokenIndex, depth
									if c := buffer[position]; c < rune('a') || c > rune('z') {
										goto l85
									}
									position++
									goto l84
								l85:
									position, tokenIndex, depth = position84, tokenIndex84, depth84
									if c := buffer[position]; c < rune('A') || c > rune('Z') {
										goto l80
									}
									position++
								}
							l84:
								break
							}
						}

						goto l79
					l80:
						position, tokenIndex, depth = position80, tokenIndex80, depth80
					}
					depth--
					add(rulePegText, position76)
				}
				depth--
				add(ruleDOMAINID, position75)
			}
			return true
		l74:
			position, tokenIndex, depth = position74, tokenIndex74, depth74
			return false
		},
		/* 5 HASH <- <<('#' ((&('_') '_') | (&('-') '-') | (&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') ([0-9] / [0-9])) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z]))*)>> */
		nil,
		/* 6 FILE <- <<((((&('_') '_') | (&('-') '-') | (&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') ([0-9] / [0-9])) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z]))+ '.' ((&('_') '_') | (&('-') '-') | (&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') ([0-9] / [0-9])) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z]))*) / ((&('_') '_') | (&('-') '-') | (&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') ([0-9] / [0-9])) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z]))+)>> */
		nil,
		/* 7 SOURCE <- <(('s' / 'S') ('o' / 'O') ('u' / 'U') ('r' / 'R') ('c' / 'C') ('e' / 'E'))> */
		nil,
		/* 8 MODEL <- <(('m' / 'M') ('o' / 'O') ('d' / 'D') ('e' / 'E') ('l' / 'L'))> */
		nil,
		/* 9 FOR <- <(('f' / 'F') ('o' / 'O') ('r' / 'R'))> */
		nil,
		/* 10 EACH <- <(('e' / 'E') ('a' / 'A') ('c' / 'C') ('h' / 'H'))> */
		nil,
		/* 11 IN <- <(('i' / 'I') ('n' / 'N'))> */
		nil,
		/* 12 WHERE <- <(('w' / 'W') ('h' / 'H') ('e' / 'E') ('r' / 'R') ('e' / 'E'))> */
		nil,
		/* 13 DISTINCT <- <(('d' / 'D') ('i' / 'I') ('s' / 'S') ('t' / 'T') ('i' / 'I') ('n' / 'N') ('c' / 'C') ('t' / 'T'))> */
		nil,
		/* 14 ORDER <- <(('o' / 'O') ('r' / 'R') ('d' / 'D') ('e' / 'E') ('r' / 'R'))> */
		nil,
		/* 15 BY <- <(('b' / 'B') ('y' / 'Y'))> */
		nil,
		/* 16 ASCENDING <- <(('a' / 'A') ('s' / 'S') ('c' / 'C') ('e' / 'E') ('n' / 'N') ('d' / 'D') ('i' / 'I') ('n' / 'N') ('g' / 'G'))> */
		nil,
		/* 17 DESCENDING <- <(('d' / 'D') ('e' / 'E') ('s' / 'S') ('c' / 'C') ('e' / 'E') ('n' / 'N') ('d' / 'D') ('i' / 'I') ('n' / 'N') ('g' / 'G'))> */
		nil,
		/* 18 TAKE <- <(('t' / 'T') ('a' / 'A') ('k' / 'K') ('e' / 'E'))> */
		nil,
		/* 19 SKIP <- <(('s' / 'S') ('k' / 'K') ('i' / 'I') ('p' / 'P'))> */
		nil,
		/* 20 LET <- <(('l' / 'L') ('e' / 'E') ('t' / 'T'))> */
		nil,
		/* 21 RETURN <- <(('r' / 'R') ('e' / 'E') ('t' / 'T') ('u' / 'U') ('r' / 'R') ('n' / 'N'))> */
		nil,
		/* 22 TAB <- <'\t'> */
		func() bool {
			position103, tokenIndex103, depth103 := position, tokenIndex, depth
			{
				position104 := position
				depth++
				if buffer[position] != rune('\t') {
					goto l103
				}
				position++
				depth--
				add(ruleTAB, position104)
			}
			return true
		l103:
			position, tokenIndex, depth = position103, tokenIndex103, depth103
			return false
		},
		/* 23 SP <- <(' ' / '\u00a0')> */
		nil,
		/* 24 WS <- <(TAB / SP)> */
		func() bool {
			position106, tokenIndex106, depth106 := position, tokenIndex, depth
			{
				position107 := position
				depth++
				{
					position108, tokenIndex108, depth108 := position, tokenIndex, depth
					if !_rules[ruleTAB]() {
						goto l109
					}
					goto l108
				l109:
					position, tokenIndex, depth = position108, tokenIndex108, depth108
					{
						position110 := position
						depth++
						{
							position111, tokenIndex111, depth111 := position, tokenIndex, depth
							if buffer[position] != rune(' ') {
								goto l112
							}
							position++
							goto l111
						l112:
							position, tokenIndex, depth = position111, tokenIndex111, depth111
							if buffer[position] != rune('\u00a0') {
								goto l106
							}
							position++
						}
					l111:
						depth--
						add(ruleSP, position110)
					}
				}
			l108:
				depth--
				add(ruleWS, position107)
			}
			return true
		l106:
			position, tokenIndex, depth = position106, tokenIndex106, depth106
			return false
		},
		/* 25 EOL <- <('\n' / ('\r' '\n') / '\r')> */
		func() bool {
			position113, tokenIndex113, depth113 := position, tokenIndex, depth
			{
				position114 := position
				depth++
				{
					position115, tokenIndex115, depth115 := position, tokenIndex, depth
					if buffer[position] != rune('\n') {
						goto l116
					}
					position++
					goto l115
				l116:
					position, tokenIndex, depth = position115, tokenIndex115, depth115
					if buffer[position] != rune('\r') {
						goto l117
					}
					position++
					if buffer[position] != rune('\n') {
						goto l117
					}
					position++
					goto l115
				l117:
					position, tokenIndex, depth = position115, tokenIndex115, depth115
					if buffer[position] != rune('\r') {
						goto l113
					}
					position++
				}
			l115:
				depth--
				add(ruleEOL, position114)
			}
			return true
		l113:
			position, tokenIndex, depth = position113, tokenIndex113, depth113
			return false
		},
		/* 26 WSEOL <- <(WS / (EOL+ TAB))> */
		func() bool {
			position118, tokenIndex118, depth118 := position, tokenIndex, depth
			{
				position119 := position
				depth++
				{
					position120, tokenIndex120, depth120 := position, tokenIndex, depth
					if !_rules[ruleWS]() {
						goto l121
					}
					goto l120
				l121:
					position, tokenIndex, depth = position120, tokenIndex120, depth120
					if !_rules[ruleEOL]() {
						goto l118
					}
				l122:
					{
						position123, tokenIndex123, depth123 := position, tokenIndex, depth
						if !_rules[ruleEOL]() {
							goto l123
						}
						goto l122
					l123:
						position, tokenIndex, depth = position123, tokenIndex123, depth123
					}
					if !_rules[ruleTAB]() {
						goto l118
					}
				}
			l120:
				depth--
				add(ruleWSEOL, position119)
			}
			return true
		l118:
			position, tokenIndex, depth = position118, tokenIndex118, depth118
			return false
		},
		/* 27 __ <- <WSEOL+> */
		func() bool {
			position124, tokenIndex124, depth124 := position, tokenIndex, depth
			{
				position125 := position
				depth++
				if !_rules[ruleWSEOL]() {
					goto l124
				}
			l126:
				{
					position127, tokenIndex127, depth127 := position, tokenIndex, depth
					if !_rules[ruleWSEOL]() {
						goto l127
					}
					goto l126
				l127:
					position, tokenIndex, depth = position127, tokenIndex127, depth127
				}
				depth--
				add(rule__, position125)
			}
			return true
		l124:
			position, tokenIndex, depth = position124, tokenIndex124, depth124
			return false
		},
		/* 28 _ <- <WSEOL*> */
		func() bool {
			{
				position129 := position
				depth++
			l130:
				{
					position131, tokenIndex131, depth131 := position, tokenIndex, depth
					if !_rules[ruleWSEOL]() {
						goto l131
					}
					goto l130
				l131:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
				}
				depth--
				add(rule_, position129)
			}
			return true
		},
		/* 29 EOF <- <!.> */
		func() bool {
			position132, tokenIndex132, depth132 := position, tokenIndex, depth
			{
				position133 := position
				depth++
				{
					position134, tokenIndex134, depth134 := position, tokenIndex, depth
					if !matchDot() {
						goto l134
					}
					goto l132
				l134:
					position, tokenIndex, depth = position134, tokenIndex134, depth134
				}
				depth--
				add(ruleEOF, position133)
			}
			return true
		l132:
			position, tokenIndex, depth = position132, tokenIndex132, depth132
			return false
		},
		/* 30 EMPTYLINE <- <(WS* EOL)> */
		func() bool {
			position135, tokenIndex135, depth135 := position, tokenIndex, depth
			{
				position136 := position
				depth++
			l137:
				{
					position138, tokenIndex138, depth138 := position, tokenIndex, depth
					if !_rules[ruleWS]() {
						goto l138
					}
					goto l137
				l138:
					position, tokenIndex, depth = position138, tokenIndex138, depth138
				}
				if !_rules[ruleEOL]() {
					goto l135
				}
				depth--
				add(ruleEMPTYLINE, position136)
			}
			return true
		l135:
			position, tokenIndex, depth = position135, tokenIndex135, depth135
			return false
		},
		/* 31 NOCOMMENTLINESTART <- <(TAB / EOL)> */
		nil,
		/* 32 COMMENTLINEEND <- <(EOL / EOF)> */
		nil,
		/* 33 COMMENTLINECHAR <- <(!COMMENTLINEEND .)> */
		nil,
		/* 34 COMMENTLINE <- <(!NOCOMMENTLINESTART COMMENTLINECHAR* EOL?)> */
		nil,
		/* 35 COMMENTBLOCK <- <((!EOF COMMENTLINE)+ EMPTYLINE*)> */
		func() bool {
			position143, tokenIndex143, depth143 := position, tokenIndex, depth
			{
				position144 := position
				depth++
				{
					position147, tokenIndex147, depth147 := position, tokenIndex, depth
					if !_rules[ruleEOF]() {
						goto l147
					}
					goto l143
				l147:
					position, tokenIndex, depth = position147, tokenIndex147, depth147
				}
				{
					position148 := position
					depth++
					{
						position149, tokenIndex149, depth149 := position, tokenIndex, depth
						{
							position150 := position
							depth++
							{
								position151, tokenIndex151, depth151 := position, tokenIndex, depth
								if !_rules[ruleTAB]() {
									goto l152
								}
								goto l151
							l152:
								position, tokenIndex, depth = position151, tokenIndex151, depth151
								if !_rules[ruleEOL]() {
									goto l149
								}
							}
						l151:
							depth--
							add(ruleNOCOMMENTLINESTART, position150)
						}
						goto l143
					l149:
						position, tokenIndex, depth = position149, tokenIndex149, depth149
					}
				l153:
					{
						position154, tokenIndex154, depth154 := position, tokenIndex, depth
						{
							position155 := position
							depth++
							{
								position156, tokenIndex156, depth156 := position, tokenIndex, depth
								{
									position157 := position
									depth++
									{
										position158, tokenIndex158, depth158 := position, tokenIndex, depth
										if !_rules[ruleEOL]() {
											goto l159
										}
										goto l158
									l159:
										position, tokenIndex, depth = position158, tokenIndex158, depth158
										if !_rules[ruleEOF]() {
											goto l156
										}
									}
								l158:
									depth--
									add(ruleCOMMENTLINEEND, position157)
								}
								goto l154
							l156:
								position, tokenIndex, depth = position156, tokenIndex156, depth156
							}
							if !matchDot() {
								goto l154
							}
							depth--
							add(ruleCOMMENTLINECHAR, position155)
						}
						goto l153
					l154:
						position, tokenIndex, depth = position154, tokenIndex154, depth154
					}
					{
						position160, tokenIndex160, depth160 := position, tokenIndex, depth
						if !_rules[ruleEOL]() {
							goto l160
						}
						goto l161
					l160:
						position, tokenIndex, depth = position160, tokenIndex160, depth160
					}
				l161:
					depth--
					add(ruleCOMMENTLINE, position148)
				}
			l145:
				{
					position146, tokenIndex146, depth146 := position, tokenIndex, depth
					{
						position162, tokenIndex162, depth162 := position, tokenIndex, depth
						if !_rules[ruleEOF]() {
							goto l162
						}
						goto l146
					l162:
						position, tokenIndex, depth = position162, tokenIndex162, depth162
					}
					{
						position163 := position
						depth++
						{
							position164, tokenIndex164, depth164 := position, tokenIndex, depth
							{
								position165 := position
								depth++
								{
									position166, tokenIndex166, depth166 := position, tokenIndex, depth
									if !_rules[ruleTAB]() {
										goto l167
									}
									goto l166
								l167:
									position, tokenIndex, depth = position166, tokenIndex166, depth166
									if !_rules[ruleEOL]() {
										goto l164
									}
								}
							l166:
								depth--
								add(ruleNOCOMMENTLINESTART, position165)
							}
							goto l146
						l164:
							position, tokenIndex, depth = position164, tokenIndex164, depth164
						}
					l168:
						{
							position169, tokenIndex169, depth169 := position, tokenIndex, depth
							{
								position170 := position
								depth++
								{
									position171, tokenIndex171, depth171 := position, tokenIndex, depth
									{
										position172 := position
										depth++
										{
											position173, tokenIndex173, depth173 := position, tokenIndex, depth
											if !_rules[ruleEOL]() {
												goto l174
											}
											goto l173
										l174:
											position, tokenIndex, depth = position173, tokenIndex173, depth173
											if !_rules[ruleEOF]() {
												goto l171
											}
										}
									l173:
										depth--
										add(ruleCOMMENTLINEEND, position172)
									}
									goto l169
								l171:
									position, tokenIndex, depth = position171, tokenIndex171, depth171
								}
								if !matchDot() {
									goto l169
								}
								depth--
								add(ruleCOMMENTLINECHAR, position170)
							}
							goto l168
						l169:
							position, tokenIndex, depth = position169, tokenIndex169, depth169
						}
						{
							position175, tokenIndex175, depth175 := position, tokenIndex, depth
							if !_rules[ruleEOL]() {
								goto l175
							}
							goto l176
						l175:
							position, tokenIndex, depth = position175, tokenIndex175, depth175
						}
					l176:
						depth--
						add(ruleCOMMENTLINE, position163)
					}
					goto l145
				l146:
					position, tokenIndex, depth = position146, tokenIndex146, depth146
				}
			l177:
				{
					position178, tokenIndex178, depth178 := position, tokenIndex, depth
					if !_rules[ruleEMPTYLINE]() {
						goto l178
					}
					goto l177
				l178:
					position, tokenIndex, depth = position178, tokenIndex178, depth178
				}
				depth--
				add(ruleCOMMENTBLOCK, position144)
			}
			return true
		l143:
			position, tokenIndex, depth = position143, tokenIndex143, depth143
			return false
		},
		/* 36 COMMENTS <- <(EMPTYLINE+ COMMENTBLOCK* TAB)> */
		func() bool {
			position179, tokenIndex179, depth179 := position, tokenIndex, depth
			{
				position180 := position
				depth++
				if !_rules[ruleEMPTYLINE]() {
					goto l179
				}
			l181:
				{
					position182, tokenIndex182, depth182 := position, tokenIndex, depth
					if !_rules[ruleEMPTYLINE]() {
						goto l182
					}
					goto l181
				l182:
					position, tokenIndex, depth = position182, tokenIndex182, depth182
				}
			l183:
				{
					position184, tokenIndex184, depth184 := position, tokenIndex, depth
					if !_rules[ruleCOMMENTBLOCK]() {
						goto l184
					}
					goto l183
				l184:
					position, tokenIndex, depth = position184, tokenIndex184, depth184
				}
				if !_rules[ruleTAB]() {
					goto l179
				}
				depth--
				add(ruleCOMMENTS, position180)
			}
			return true
		l179:
			position, tokenIndex, depth = position179, tokenIndex179, depth179
			return false
		},
		/* 37 STARTCOMMENT <- <(EMPTYLINE* COMMENTBLOCK* (TAB / COMMENTS*))> */
		nil,
		/* 38 FINALCOMMENTS <- <(EMPTYLINE* COMMENTBLOCK*)> */
		nil,
		/* 39 DIGIT <- <[0-9]> */
		func() bool {
			position187, tokenIndex187, depth187 := position, tokenIndex, depth
			{
				position188 := position
				depth++
				if c := buffer[position]; c < rune('0') || c > rune('9') {
					goto l187
				}
				position++
				depth--
				add(ruleDIGIT, position188)
			}
			return true
		l187:
			position, tokenIndex, depth = position187, tokenIndex187, depth187
			return false
		},
		/* 40 CARDINAL <- <DIGIT+> */
		func() bool {
			position189, tokenIndex189, depth189 := position, tokenIndex, depth
			{
				position190 := position
				depth++
				if !_rules[ruleDIGIT]() {
					goto l189
				}
			l191:
				{
					position192, tokenIndex192, depth192 := position, tokenIndex, depth
					if !_rules[ruleDIGIT]() {
						goto l192
					}
					goto l191
				l192:
					position, tokenIndex, depth = position192, tokenIndex192, depth192
				}
				depth--
				add(ruleCARDINAL, position190)
			}
			return true
		l189:
			position, tokenIndex, depth = position189, tokenIndex189, depth189
			return false
		},
		/* 41 INTEGER <- <(('+' / '-')? CARDINAL)> */
		func() bool {
			position193, tokenIndex193, depth193 := position, tokenIndex, depth
			{
				position194 := position
				depth++
				{
					position195, tokenIndex195, depth195 := position, tokenIndex, depth
					{
						position197, tokenIndex197, depth197 := position, tokenIndex, depth
						if buffer[position] != rune('+') {
							goto l198
						}
						position++
						goto l197
					l198:
						position, tokenIndex, depth = position197, tokenIndex197, depth197
						if buffer[position] != rune('-') {
							goto l195
						}
						position++
					}
				l197:
					goto l196
				l195:
					position, tokenIndex, depth = position195, tokenIndex195, depth195
				}
			l196:
				if !_rules[ruleCARDINAL]() {
					goto l193
				}
				depth--
				add(ruleINTEGER, position194)
			}
			return true
		l193:
			position, tokenIndex, depth = position193, tokenIndex193, depth193
			return false
		},
		/* 42 EXPONENT <- <(('e' / 'E') INTEGER)> */
		nil,
		/* 43 FLOAT <- <(INTEGER '.' CARDINAL? EXPONENT?)> */
		nil,
		/* 44 DATE <- <(DIGIT DIGIT DIGIT DIGIT '-' DIGIT DIGIT '-' DIGIT DIGIT (('+' / '-') DIGIT DIGIT)?)> */
		func() bool {
			position201, tokenIndex201, depth201 := position, tokenIndex, depth
			{
				position202 := position
				depth++
				if !_rules[ruleDIGIT]() {
					goto l201
				}
				if !_rules[ruleDIGIT]() {
					goto l201
				}
				if !_rules[ruleDIGIT]() {
					goto l201
				}
				if !_rules[ruleDIGIT]() {
					goto l201
				}
				if buffer[position] != rune('-') {
					goto l201
				}
				position++
				if !_rules[ruleDIGIT]() {
					goto l201
				}
				if !_rules[ruleDIGIT]() {
					goto l201
				}
				if buffer[position] != rune('-') {
					goto l201
				}
				position++
				if !_rules[ruleDIGIT]() {
					goto l201
				}
				if !_rules[ruleDIGIT]() {
					goto l201
				}
				{
					position203, tokenIndex203, depth203 := position, tokenIndex, depth
					{
						position205, tokenIndex205, depth205 := position, tokenIndex, depth
						if buffer[position] != rune('+') {
							goto l206
						}
						position++
						goto l205
					l206:
						position, tokenIndex, depth = position205, tokenIndex205, depth205
						if buffer[position] != rune('-') {
							goto l203
						}
						position++
					}
				l205:
					if !_rules[ruleDIGIT]() {
						goto l203
					}
					if !_rules[ruleDIGIT]() {
						goto l203
					}
					goto l204
				l203:
					position, tokenIndex, depth = position203, tokenIndex203, depth203
				}
			l204:
				depth--
				add(ruleDATE, position202)
			}
			return true
		l201:
			position, tokenIndex, depth = position201, tokenIndex201, depth201
			return false
		},
		/* 45 TIME <- <(DIGIT DIGIT ':' DIGIT DIGIT (':' DIGIT DIGIT)?)> */
		func() bool {
			position207, tokenIndex207, depth207 := position, tokenIndex, depth
			{
				position208 := position
				depth++
				if !_rules[ruleDIGIT]() {
					goto l207
				}
				if !_rules[ruleDIGIT]() {
					goto l207
				}
				if buffer[position] != rune(':') {
					goto l207
				}
				position++
				if !_rules[ruleDIGIT]() {
					goto l207
				}
				if !_rules[ruleDIGIT]() {
					goto l207
				}
				{
					position209, tokenIndex209, depth209 := position, tokenIndex, depth
					if buffer[position] != rune(':') {
						goto l209
					}
					position++
					if !_rules[ruleDIGIT]() {
						goto l209
					}
					if !_rules[ruleDIGIT]() {
						goto l209
					}
					goto l210
				l209:
					position, tokenIndex, depth = position209, tokenIndex209, depth209
				}
			l210:
				depth--
				add(ruleTIME, position208)
			}
			return true
		l207:
			position, tokenIndex, depth = position207, tokenIndex207, depth207
			return false
		},
		/* 46 DATETIME <- <(DATE ('t' / 'T') TIME)> */
		nil,
		/* 47 STRING <- <(('"' (!'"' .)* '"') / ('\'' (!'\'' .)* '\''))> */
		nil,
		/* 48 REGEX <- <('/' (!'/' .)* '/')> */
		nil,
		/* 49 OP <- <((&('/') '/') | (&('*') '*') | (&('-') '-') | (&('+') '+'))> */
		nil,
		/* 50 BOOLOP <- <(('|' '|') / ('&' '&'))> */
		nil,
		/* 51 COMPARATOR <- <('<' / '>' / ((&('!') ('!' '=')) | (&('=') ('=' '=')) | (&('>') ('>' '=')) | (&('<') ('<' '='))))> */
		nil,
		/* 52 MATCHER <- <'~'> */
		nil,
		/* 53 NOTOP <- <'!'> */
		func() bool {
			position218, tokenIndex218, depth218 := position, tokenIndex, depth
			{
				position219 := position
				depth++
				if buffer[position] != rune('!') {
					goto l218
				}
				position++
				depth--
				add(ruleNOTOP, position219)
			}
			return true
		l218:
			position, tokenIndex, depth = position218, tokenIndex218, depth218
			return false
		},
		/* 54 METHODNAME <- <(('M' 'i' 'n') / ('S' 'u' 'm') / ('T' 'o' 'U' 'p' 'p' 'e' 'r') / ((&('J') ('J' 'o' 'i' 'n')) | (&('T') ('T' 'o' 'L' 'o' 'w' 'e' 'r')) | (&('S') ('S' 'p' 'l' 'i' 't')) | (&('A') ('A' 'v' 'e' 'r' 'a' 'g' 'e')) | (&('M') ('M' 'a' 'x')) | (&('C') ('C' 'o' 'u' 'n' 't')) | (&('L') ('L' 'a' 's' 't')) | (&('F') ('F' 'i' 'r' 's' 't'))))> */
		nil,
		/* 55 METHOD <- <('.' METHODNAME _ '(' _ ')')> */
		func() bool {
			position221, tokenIndex221, depth221 := position, tokenIndex, depth
			{
				position222 := position
				depth++
				if buffer[position] != rune('.') {
					goto l221
				}
				position++
				{
					position223 := position
					depth++
					{
						position224, tokenIndex224, depth224 := position, tokenIndex, depth
						if buffer[position] != rune('M') {
							goto l225
						}
						position++
						if buffer[position] != rune('i') {
							goto l225
						}
						position++
						if buffer[position] != rune('n') {
							goto l225
						}
						position++
						goto l224
					l225:
						position, tokenIndex, depth = position224, tokenIndex224, depth224
						if buffer[position] != rune('S') {
							goto l226
						}
						position++
						if buffer[position] != rune('u') {
							goto l226
						}
						position++
						if buffer[position] != rune('m') {
							goto l226
						}
						position++
						goto l224
					l226:
						position, tokenIndex, depth = position224, tokenIndex224, depth224
						if buffer[position] != rune('T') {
							goto l227
						}
						position++
						if buffer[position] != rune('o') {
							goto l227
						}
						position++
						if buffer[position] != rune('U') {
							goto l227
						}
						position++
						if buffer[position] != rune('p') {
							goto l227
						}
						position++
						if buffer[position] != rune('p') {
							goto l227
						}
						position++
						if buffer[position] != rune('e') {
							goto l227
						}
						position++
						if buffer[position] != rune('r') {
							goto l227
						}
						position++
						goto l224
					l227:
						position, tokenIndex, depth = position224, tokenIndex224, depth224
						{
							switch buffer[position] {
							case 'J':
								if buffer[position] != rune('J') {
									goto l221
								}
								position++
								if buffer[position] != rune('o') {
									goto l221
								}
								position++
								if buffer[position] != rune('i') {
									goto l221
								}
								position++
								if buffer[position] != rune('n') {
									goto l221
								}
								position++
								break
							case 'T':
								if buffer[position] != rune('T') {
									goto l221
								}
								position++
								if buffer[position] != rune('o') {
									goto l221
								}
								position++
								if buffer[position] != rune('L') {
									goto l221
								}
								position++
								if buffer[position] != rune('o') {
									goto l221
								}
								position++
								if buffer[position] != rune('w') {
									goto l221
								}
								position++
								if buffer[position] != rune('e') {
									goto l221
								}
								position++
								if buffer[position] != rune('r') {
									goto l221
								}
								position++
								break
							case 'S':
								if buffer[position] != rune('S') {
									goto l221
								}
								position++
								if buffer[position] != rune('p') {
									goto l221
								}
								position++
								if buffer[position] != rune('l') {
									goto l221
								}
								position++
								if buffer[position] != rune('i') {
									goto l221
								}
								position++
								if buffer[position] != rune('t') {
									goto l221
								}
								position++
								break
							case 'A':
								if buffer[position] != rune('A') {
									goto l221
								}
								position++
								if buffer[position] != rune('v') {
									goto l221
								}
								position++
								if buffer[position] != rune('e') {
									goto l221
								}
								position++
								if buffer[position] != rune('r') {
									goto l221
								}
								position++
								if buffer[position] != rune('a') {
									goto l221
								}
								position++
								if buffer[position] != rune('g') {
									goto l221
								}
								position++
								if buffer[position] != rune('e') {
									goto l221
								}
								position++
								break
							case 'M':
								if buffer[position] != rune('M') {
									goto l221
								}
								position++
								if buffer[position] != rune('a') {
									goto l221
								}
								position++
								if buffer[position] != rune('x') {
									goto l221
								}
								position++
								break
							case 'C':
								if buffer[position] != rune('C') {
									goto l221
								}
								position++
								if buffer[position] != rune('o') {
									goto l221
								}
								position++
								if buffer[position] != rune('u') {
									goto l221
								}
								position++
								if buffer[position] != rune('n') {
									goto l221
								}
								position++
								if buffer[position] != rune('t') {
									goto l221
								}
								position++
								break
							case 'L':
								if buffer[position] != rune('L') {
									goto l221
								}
								position++
								if buffer[position] != rune('a') {
									goto l221
								}
								position++
								if buffer[position] != rune('s') {
									goto l221
								}
								position++
								if buffer[position] != rune('t') {
									goto l221
								}
								position++
								break
							default:
								if buffer[position] != rune('F') {
									goto l221
								}
								position++
								if buffer[position] != rune('i') {
									goto l221
								}
								position++
								if buffer[position] != rune('r') {
									goto l221
								}
								position++
								if buffer[position] != rune('s') {
									goto l221
								}
								position++
								if buffer[position] != rune('t') {
									goto l221
								}
								position++
								break
							}
						}

					}
				l224:
					depth--
					add(ruleMETHODNAME, position223)
				}
				if !_rules[rule_]() {
					goto l221
				}
				if buffer[position] != rune('(') {
					goto l221
				}
				position++
				if !_rules[rule_]() {
					goto l221
				}
				if buffer[position] != rune(')') {
					goto l221
				}
				position++
				depth--
				add(ruleMETHOD, position222)
			}
			return true
		l221:
			position, tokenIndex, depth = position221, tokenIndex221, depth221
			return false
		},
		/* 56 URL <- <(PROTOCOL HOST PORT? PATH?)> */
		func() bool {
			position229, tokenIndex229, depth229 := position, tokenIndex, depth
			{
				position230 := position
				depth++
				{
					position231 := position
					depth++
					{
						position232, tokenIndex232, depth232 := position, tokenIndex, depth
						{
							position234, tokenIndex234, depth234 := position, tokenIndex, depth
							if buffer[position] != rune('h') {
								goto l235
							}
							position++
							goto l234
						l235:
							position, tokenIndex, depth = position234, tokenIndex234, depth234
							if buffer[position] != rune('H') {
								goto l233
							}
							position++
						}
					l234:
						{
							position236, tokenIndex236, depth236 := position, tokenIndex, depth
							if buffer[position] != rune('t') {
								goto l237
							}
							position++
							goto l236
						l237:
							position, tokenIndex, depth = position236, tokenIndex236, depth236
							if buffer[position] != rune('T') {
								goto l233
							}
							position++
						}
					l236:
						{
							position238, tokenIndex238, depth238 := position, tokenIndex, depth
							if buffer[position] != rune('t') {
								goto l239
							}
							position++
							goto l238
						l239:
							position, tokenIndex, depth = position238, tokenIndex238, depth238
							if buffer[position] != rune('T') {
								goto l233
							}
							position++
						}
					l238:
						{
							position240, tokenIndex240, depth240 := position, tokenIndex, depth
							if buffer[position] != rune('p') {
								goto l241
							}
							position++
							goto l240
						l241:
							position, tokenIndex, depth = position240, tokenIndex240, depth240
							if buffer[position] != rune('P') {
								goto l233
							}
							position++
						}
					l240:
						if buffer[position] != rune(':') {
							goto l233
						}
						position++
						if buffer[position] != rune('/') {
							goto l233
						}
						position++
						if buffer[position] != rune('/') {
							goto l233
						}
						position++
						goto l232
					l233:
						position, tokenIndex, depth = position232, tokenIndex232, depth232
						{
							position242, tokenIndex242, depth242 := position, tokenIndex, depth
							if buffer[position] != rune('h') {
								goto l243
							}
							position++
							goto l242
						l243:
							position, tokenIndex, depth = position242, tokenIndex242, depth242
							if buffer[position] != rune('H') {
								goto l229
							}
							position++
						}
					l242:
						{
							position244, tokenIndex244, depth244 := position, tokenIndex, depth
							if buffer[position] != rune('t') {
								goto l245
							}
							position++
							goto l244
						l245:
							position, tokenIndex, depth = position244, tokenIndex244, depth244
							if buffer[position] != rune('T') {
								goto l229
							}
							position++
						}
					l244:
						{
							position246, tokenIndex246, depth246 := position, tokenIndex, depth
							if buffer[position] != rune('t') {
								goto l247
							}
							position++
							goto l246
						l247:
							position, tokenIndex, depth = position246, tokenIndex246, depth246
							if buffer[position] != rune('T') {
								goto l229
							}
							position++
						}
					l246:
						{
							position248, tokenIndex248, depth248 := position, tokenIndex, depth
							if buffer[position] != rune('p') {
								goto l249
							}
							position++
							goto l248
						l249:
							position, tokenIndex, depth = position248, tokenIndex248, depth248
							if buffer[position] != rune('P') {
								goto l229
							}
							position++
						}
					l248:
						{
							position250, tokenIndex250, depth250 := position, tokenIndex, depth
							if buffer[position] != rune('s') {
								goto l251
							}
							position++
							goto l250
						l251:
							position, tokenIndex, depth = position250, tokenIndex250, depth250
							if buffer[position] != rune('S') {
								goto l229
							}
							position++
						}
					l250:
						if buffer[position] != rune(':') {
							goto l229
						}
						position++
						if buffer[position] != rune('/') {
							goto l229
						}
						position++
						if buffer[position] != rune('/') {
							goto l229
						}
						position++
					}
				l232:
					depth--
					add(rulePROTOCOL, position231)
				}
				{
					position252 := position
					depth++
					{
						position253, tokenIndex253, depth253 := position, tokenIndex, depth
						if !_rules[ruleDOMAINID]() {
							goto l254
						}
						{
							position257 := position
							depth++
							if buffer[position] != rune('.') {
								goto l254
							}
							position++
							if !_rules[ruleDOMAINID]() {
								goto l254
							}
							depth--
							add(rulePARTIALDOMAIN, position257)
						}
					l255:
						{
							position256, tokenIndex256, depth256 := position, tokenIndex, depth
							{
								position258 := position
								depth++
								if buffer[position] != rune('.') {
									goto l256
								}
								position++
								if !_rules[ruleDOMAINID]() {
									goto l256
								}
								depth--
								add(rulePARTIALDOMAIN, position258)
							}
							goto l255
						l256:
							position, tokenIndex, depth = position256, tokenIndex256, depth256
						}
						goto l253
					l254:
						position, tokenIndex, depth = position253, tokenIndex253, depth253
						if !_rules[ruleCARDINAL]() {
							goto l229
						}
						{
							position261 := position
							depth++
							if buffer[position] != rune('.') {
								goto l229
							}
							position++
							if !_rules[ruleCARDINAL]() {
								goto l229
							}
							depth--
							add(rulePARTIALIPNUMBER, position261)
						}
					l259:
						{
							position260, tokenIndex260, depth260 := position, tokenIndex, depth
							{
								position262 := position
								depth++
								if buffer[position] != rune('.') {
									goto l260
								}
								position++
								if !_rules[ruleCARDINAL]() {
									goto l260
								}
								depth--
								add(rulePARTIALIPNUMBER, position262)
							}
							goto l259
						l260:
							position, tokenIndex, depth = position260, tokenIndex260, depth260
						}
					}
				l253:
					depth--
					add(ruleHOST, position252)
				}
				{
					position263, tokenIndex263, depth263 := position, tokenIndex, depth
					{
						position265 := position
						depth++
						if buffer[position] != rune(':') {
							goto l263
						}
						position++
						if !_rules[ruleCARDINAL]() {
							goto l263
						}
						depth--
						add(rulePORT, position265)
					}
					goto l264
				l263:
					position, tokenIndex, depth = position263, tokenIndex263, depth263
				}
			l264:
				{
					position266, tokenIndex266, depth266 := position, tokenIndex, depth
					if !_rules[rulePATH]() {
						goto l266
					}
					goto l267
				l266:
					position, tokenIndex, depth = position266, tokenIndex266, depth266
				}
			l267:
				depth--
				add(ruleURL, position230)
			}
			return true
		l229:
			position, tokenIndex, depth = position229, tokenIndex229, depth229
			return false
		},
		/* 57 PROTOCOL <- <((('h' / 'H') ('t' / 'T') ('t' / 'T') ('p' / 'P') ':' '/' '/') / (('h' / 'H') ('t' / 'T') ('t' / 'T') ('p' / 'P') ('s' / 'S') ':' '/' '/'))> */
		nil,
		/* 58 HOST <- <((DOMAINID PARTIALDOMAIN+) / (CARDINAL PARTIALIPNUMBER+))> */
		nil,
		/* 59 PARTIALIPNUMBER <- <('.' CARDINAL)> */
		nil,
		/* 60 PARTIALDOMAIN <- <('.' DOMAINID)> */
		nil,
		/* 61 PORT <- <(':' CARDINAL)> */
		nil,
		/* 62 PATH <- <(('/' FILE PATH?) / '/' / HASH?)> */
		func() bool {
			{
				position274 := position
				depth++
				{
					position275, tokenIndex275, depth275 := position, tokenIndex, depth
					if buffer[position] != rune('/') {
						goto l276
					}
					position++
					{
						position277 := position
						depth++
						{
							position278 := position
							depth++
							{
								position279, tokenIndex279, depth279 := position, tokenIndex, depth
								{
									switch buffer[position] {
									case '_':
										if buffer[position] != rune('_') {
											goto l280
										}
										position++
										break
									case '-':
										if buffer[position] != rune('-') {
											goto l280
										}
										position++
										break
									case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
										{
											position284, tokenIndex284, depth284 := position, tokenIndex, depth
											if c := buffer[position]; c < rune('0') || c > rune('9') {
												goto l285
											}
											position++
											goto l284
										l285:
											position, tokenIndex, depth = position284, tokenIndex284, depth284
											if c := buffer[position]; c < rune('0') || c > rune('9') {
												goto l280
											}
											position++
										}
									l284:
										break
									case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
										if c := buffer[position]; c < rune('A') || c > rune('Z') {
											goto l280
										}
										position++
										break
									default:
										if c := buffer[position]; c < rune('a') || c > rune('z') {
											goto l280
										}
										position++
										break
									}
								}

							l281:
								{
									position282, tokenIndex282, depth282 := position, tokenIndex, depth
									{
										switch buffer[position] {
										case '_':
											if buffer[position] != rune('_') {
												goto l282
											}
											position++
											break
										case '-':
											if buffer[position] != rune('-') {
												goto l282
											}
											position++
											break
										case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
											{
												position287, tokenIndex287, depth287 := position, tokenIndex, depth
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l288
												}
												position++
												goto l287
											l288:
												position, tokenIndex, depth = position287, tokenIndex287, depth287
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l282
												}
												position++
											}
										l287:
											break
										case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
											if c := buffer[position]; c < rune('A') || c > rune('Z') {
												goto l282
											}
											position++
											break
										default:
											if c := buffer[position]; c < rune('a') || c > rune('z') {
												goto l282
											}
											position++
											break
										}
									}

									goto l281
								l282:
									position, tokenIndex, depth = position282, tokenIndex282, depth282
								}
								if buffer[position] != rune('.') {
									goto l280
								}
								position++
							l289:
								{
									position290, tokenIndex290, depth290 := position, tokenIndex, depth
									{
										switch buffer[position] {
										case '_':
											if buffer[position] != rune('_') {
												goto l290
											}
											position++
											break
										case '-':
											if buffer[position] != rune('-') {
												goto l290
											}
											position++
											break
										case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
											{
												position292, tokenIndex292, depth292 := position, tokenIndex, depth
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l293
												}
												position++
												goto l292
											l293:
												position, tokenIndex, depth = position292, tokenIndex292, depth292
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l290
												}
												position++
											}
										l292:
											break
										case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
											if c := buffer[position]; c < rune('A') || c > rune('Z') {
												goto l290
											}
											position++
											break
										default:
											if c := buffer[position]; c < rune('a') || c > rune('z') {
												goto l290
											}
											position++
											break
										}
									}

									goto l289
								l290:
									position, tokenIndex, depth = position290, tokenIndex290, depth290
								}
								goto l279
							l280:
								position, tokenIndex, depth = position279, tokenIndex279, depth279
								{
									switch buffer[position] {
									case '_':
										if buffer[position] != rune('_') {
											goto l276
										}
										position++
										break
									case '-':
										if buffer[position] != rune('-') {
											goto l276
										}
										position++
										break
									case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
										{
											position297, tokenIndex297, depth297 := position, tokenIndex, depth
											if c := buffer[position]; c < rune('0') || c > rune('9') {
												goto l298
											}
											position++
											goto l297
										l298:
											position, tokenIndex, depth = position297, tokenIndex297, depth297
											if c := buffer[position]; c < rune('0') || c > rune('9') {
												goto l276
											}
											position++
										}
									l297:
										break
									case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
										if c := buffer[position]; c < rune('A') || c > rune('Z') {
											goto l276
										}
										position++
										break
									default:
										if c := buffer[position]; c < rune('a') || c > rune('z') {
											goto l276
										}
										position++
										break
									}
								}

							l294:
								{
									position295, tokenIndex295, depth295 := position, tokenIndex, depth
									{
										switch buffer[position] {
										case '_':
											if buffer[position] != rune('_') {
												goto l295
											}
											position++
											break
										case '-':
											if buffer[position] != rune('-') {
												goto l295
											}
											position++
											break
										case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
											{
												position300, tokenIndex300, depth300 := position, tokenIndex, depth
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l301
												}
												position++
												goto l300
											l301:
												position, tokenIndex, depth = position300, tokenIndex300, depth300
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l295
												}
												position++
											}
										l300:
											break
										case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
											if c := buffer[position]; c < rune('A') || c > rune('Z') {
												goto l295
											}
											position++
											break
										default:
											if c := buffer[position]; c < rune('a') || c > rune('z') {
												goto l295
											}
											position++
											break
										}
									}

									goto l294
								l295:
									position, tokenIndex, depth = position295, tokenIndex295, depth295
								}
							}
						l279:
							depth--
							add(rulePegText, position278)
						}
						depth--
						add(ruleFILE, position277)
					}
					{
						position302, tokenIndex302, depth302 := position, tokenIndex, depth
						if !_rules[rulePATH]() {
							goto l302
						}
						goto l303
					l302:
						position, tokenIndex, depth = position302, tokenIndex302, depth302
					}
				l303:
					goto l275
				l276:
					position, tokenIndex, depth = position275, tokenIndex275, depth275
					if buffer[position] != rune('/') {
						goto l304
					}
					position++
					goto l275
				l304:
					position, tokenIndex, depth = position275, tokenIndex275, depth275
					{
						position305, tokenIndex305, depth305 := position, tokenIndex, depth
						{
							position307 := position
							depth++
							{
								position308 := position
								depth++
								if buffer[position] != rune('#') {
									goto l305
								}
								position++
							l309:
								{
									position310, tokenIndex310, depth310 := position, tokenIndex, depth
									{
										switch buffer[position] {
										case '_':
											if buffer[position] != rune('_') {
												goto l310
											}
											position++
											break
										case '-':
											if buffer[position] != rune('-') {
												goto l310
											}
											position++
											break
										case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
											{
												position312, tokenIndex312, depth312 := position, tokenIndex, depth
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l313
												}
												position++
												goto l312
											l313:
												position, tokenIndex, depth = position312, tokenIndex312, depth312
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l310
												}
												position++
											}
										l312:
											break
										case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
											if c := buffer[position]; c < rune('A') || c > rune('Z') {
												goto l310
											}
											position++
											break
										default:
											if c := buffer[position]; c < rune('a') || c > rune('z') {
												goto l310
											}
											position++
											break
										}
									}

									goto l309
								l310:
									position, tokenIndex, depth = position310, tokenIndex310, depth310
								}
								depth--
								add(rulePegText, position308)
							}
							depth--
							add(ruleHASH, position307)
						}
						goto l306
					l305:
						position, tokenIndex, depth = position305, tokenIndex305, depth305
					}
				l306:
				}
			l275:
				depth--
				add(rulePATH, position274)
			}
			return true
		},
		/* 63 Selection <- <(SourceDeclaration? ModelDeclaration* ElementContent*)> */
		nil,
		/* 64 SourceDeclaration <- <(COMMENTS? _ SOURCE _ '=' _ URL)> */
		nil,
		/* 65 ModelDeclaration <- <(COMMENTS? _ MODEL __ UppercaseIdentifier Action1 _ '=' _ URL)> */
		nil,
		/* 66 Element <- <(COMMENTS? _ ElementIdentifier _ '{' ElementContent* _ '}')> */
		func() bool {
			position317, tokenIndex317, depth317 := position, tokenIndex, depth
			{
				position318 := position
				depth++
				{
					position319, tokenIndex319, depth319 := position, tokenIndex, depth
					if !_rules[ruleCOMMENTS]() {
						goto l319
					}
					goto l320
				l319:
					position, tokenIndex, depth = position319, tokenIndex319, depth319
				}
			l320:
				if !_rules[rule_]() {
					goto l317
				}
				{
					position321 := position
					depth++
					if !_rules[ruleLowercaseIdentifier]() {
						goto l317
					}
					depth--
					add(ruleElementIdentifier, position321)
				}
				if !_rules[rule_]() {
					goto l317
				}
				if buffer[position] != rune('{') {
					goto l317
				}
				position++
			l322:
				{
					position323, tokenIndex323, depth323 := position, tokenIndex, depth
					if !_rules[ruleElementContent]() {
						goto l323
					}
					goto l322
				l323:
					position, tokenIndex, depth = position323, tokenIndex323, depth323
				}
				if !_rules[rule_]() {
					goto l317
				}
				if buffer[position] != rune('}') {
					goto l317
				}
				position++
				depth--
				add(ruleElement, position318)
			}
			return true
		l317:
			position, tokenIndex, depth = position317, tokenIndex317, depth317
			return false
		},
		/* 67 ElementIdentifier <- <LowercaseIdentifier> */
		nil,
		/* 68 ElementContent <- <(Element / Attribute / Expr)> */
		func() bool {
			position325, tokenIndex325, depth325 := position, tokenIndex, depth
			{
				position326 := position
				depth++
				{
					position327, tokenIndex327, depth327 := position, tokenIndex, depth
					if !_rules[ruleElement]() {
						goto l328
					}
					goto l327
				l328:
					position, tokenIndex, depth = position327, tokenIndex327, depth327
					{
						position330 := position
						depth++
						{
							position331, tokenIndex331, depth331 := position, tokenIndex, depth
							if !_rules[ruleCOMMENTS]() {
								goto l331
							}
							goto l332
						l331:
							position, tokenIndex, depth = position331, tokenIndex331, depth331
						}
					l332:
						if !_rules[rule_]() {
							goto l329
						}
						{
							position333 := position
							depth++
							if buffer[position] != rune('@') {
								goto l329
							}
							position++
							if !_rules[ruleLowercaseIdentifier]() {
								goto l329
							}
							depth--
							add(ruleAttributeIdentifier, position333)
						}
						if !_rules[rule_]() {
							goto l329
						}
						if buffer[position] != rune('{') {
							goto l329
						}
						position++
						if !_rules[rule_]() {
							goto l329
						}
						if !_rules[ruleAttributeContent]() {
							goto l329
						}
					l334:
						{
							position335, tokenIndex335, depth335 := position, tokenIndex, depth
							if !_rules[rule__]() {
								goto l335
							}
							if !_rules[ruleAttributeContent]() {
								goto l335
							}
							goto l334
						l335:
							position, tokenIndex, depth = position335, tokenIndex335, depth335
						}
						if !_rules[rule_]() {
							goto l329
						}
						if buffer[position] != rune('}') {
							goto l329
						}
						position++
						depth--
						add(ruleAttribute, position330)
					}
					goto l327
				l329:
					position, tokenIndex, depth = position327, tokenIndex327, depth327
					if !_rules[ruleExpr]() {
						goto l325
					}
				}
			l327:
				depth--
				add(ruleElementContent, position326)
			}
			return true
		l325:
			position, tokenIndex, depth = position325, tokenIndex325, depth325
			return false
		},
		/* 69 Attribute <- <(COMMENTS? _ AttributeIdentifier _ '{' _ AttributeContent (__ AttributeContent)* _ '}')> */
		nil,
		/* 70 AttributeIdentifier <- <('@' LowercaseIdentifier)> */
		nil,
		/* 71 AttributeContent <- <Expr> */
		func() bool {
			position338, tokenIndex338, depth338 := position, tokenIndex, depth
			{
				position339 := position
				depth++
				if !_rules[ruleExpr]() {
					goto l338
				}
				depth--
				add(ruleAttributeContent, position339)
			}
			return true
		l338:
			position, tokenIndex, depth = position338, tokenIndex338, depth338
			return false
		},
		/* 72 Selector <- <((&('$') SourceSelector) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') AbsoluteSelector) | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') RelativeSelector))> */
		func() bool {
			position340, tokenIndex340, depth340 := position, tokenIndex, depth
			{
				position341 := position
				depth++
				{
					switch buffer[position] {
					case '$':
						{
							position343 := position
							depth++
							if buffer[position] != rune('$') {
								goto l340
							}
							position++
						l344:
							{
								position345, tokenIndex345, depth345 := position, tokenIndex, depth
								if buffer[position] != rune('.') {
									goto l345
								}
								position++
								if !_rules[ruleLowercaseIdentifier]() {
									goto l345
								}
								goto l344
							l345:
								position, tokenIndex, depth = position345, tokenIndex345, depth345
							}
							{
								position346, tokenIndex346, depth346 := position, tokenIndex, depth
								{
									position348, tokenIndex348, depth348 := position, tokenIndex, depth
									if buffer[position] != rune('@') {
										goto l349
									}
									position++
									if !_rules[ruleLowercaseIdentifier]() {
										goto l349
									}
									goto l348
								l349:
									position, tokenIndex, depth = position348, tokenIndex348, depth348
									if buffer[position] != rune('@') {
										goto l350
									}
									position++
									goto l348
								l350:
									position, tokenIndex, depth = position348, tokenIndex348, depth348
									if buffer[position] != rune('.') {
										goto l346
									}
									position++
									if buffer[position] != rune('*') {
										goto l346
									}
									position++
								}
							l348:
								goto l347
							l346:
								position, tokenIndex, depth = position346, tokenIndex346, depth346
							}
						l347:
							{
								position351, tokenIndex351, depth351 := position, tokenIndex, depth
								if !_rules[ruleMETHOD]() {
									goto l351
								}
								goto l352
							l351:
								position, tokenIndex, depth = position351, tokenIndex351, depth351
							}
						l352:
							depth--
							add(ruleSourceSelector, position343)
						}
						break
					case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
						{
							position353 := position
							depth++
							if !_rules[ruleUppercaseIdentifier]() {
								goto l340
							}
							{
								add(ruleAction3, position)
							}
						l355:
							{
								position356, tokenIndex356, depth356 := position, tokenIndex, depth
								if buffer[position] != rune('.') {
									goto l356
								}
								position++
								if !_rules[ruleLowercaseIdentifier]() {
									goto l356
								}
								goto l355
							l356:
								position, tokenIndex, depth = position356, tokenIndex356, depth356
							}
							{
								position357, tokenIndex357, depth357 := position, tokenIndex, depth
								{
									position359, tokenIndex359, depth359 := position, tokenIndex, depth
									if buffer[position] != rune('@') {
										goto l360
									}
									position++
									if !_rules[ruleLowercaseIdentifier]() {
										goto l360
									}
									goto l359
								l360:
									position, tokenIndex, depth = position359, tokenIndex359, depth359
									if buffer[position] != rune('@') {
										goto l361
									}
									position++
									goto l359
								l361:
									position, tokenIndex, depth = position359, tokenIndex359, depth359
									if buffer[position] != rune('.') {
										goto l357
									}
									position++
									if buffer[position] != rune('*') {
										goto l357
									}
									position++
								}
							l359:
								goto l358
							l357:
								position, tokenIndex, depth = position357, tokenIndex357, depth357
							}
						l358:
							{
								position362, tokenIndex362, depth362 := position, tokenIndex, depth
								if !_rules[ruleMETHOD]() {
									goto l362
								}
								goto l363
							l362:
								position, tokenIndex, depth = position362, tokenIndex362, depth362
							}
						l363:
							depth--
							add(ruleAbsoluteSelector, position353)
						}
						break
					default:
						{
							position364 := position
							depth++
							if !_rules[ruleLowercaseIdentifier]() {
								goto l340
							}
							{
								add(ruleAction2, position)
							}
						l366:
							{
								position367, tokenIndex367, depth367 := position, tokenIndex, depth
								if buffer[position] != rune('.') {
									goto l367
								}
								position++
								if !_rules[ruleLowercaseIdentifier]() {
									goto l367
								}
								goto l366
							l367:
								position, tokenIndex, depth = position367, tokenIndex367, depth367
							}
							{
								position368, tokenIndex368, depth368 := position, tokenIndex, depth
								{
									position370, tokenIndex370, depth370 := position, tokenIndex, depth
									if buffer[position] != rune('@') {
										goto l371
									}
									position++
									if !_rules[ruleLowercaseIdentifier]() {
										goto l371
									}
									goto l370
								l371:
									position, tokenIndex, depth = position370, tokenIndex370, depth370
									if buffer[position] != rune('@') {
										goto l372
									}
									position++
									goto l370
								l372:
									position, tokenIndex, depth = position370, tokenIndex370, depth370
									if buffer[position] != rune('.') {
										goto l368
									}
									position++
									if buffer[position] != rune('*') {
										goto l368
									}
									position++
								}
							l370:
								goto l369
							l368:
								position, tokenIndex, depth = position368, tokenIndex368, depth368
							}
						l369:
							{
								position373, tokenIndex373, depth373 := position, tokenIndex, depth
								if !_rules[ruleMETHOD]() {
									goto l373
								}
								goto l374
							l373:
								position, tokenIndex, depth = position373, tokenIndex373, depth373
							}
						l374:
							depth--
							add(ruleRelativeSelector, position364)
						}
						break
					}
				}

				depth--
				add(ruleSelector, position341)
			}
			return true
		l340:
			position, tokenIndex, depth = position340, tokenIndex340, depth340
			return false
		},
		/* 73 RelativeSelector <- <(LowercaseIdentifier Action2 ('.' LowercaseIdentifier)* (('@' LowercaseIdentifier) / '@' / ('.' '*'))? METHOD?)> */
		nil,
		/* 74 AbsoluteSelector <- <(UppercaseIdentifier Action3 ('.' LowercaseIdentifier)* (('@' LowercaseIdentifier) / '@' / ('.' '*'))? METHOD?)> */
		nil,
		/* 75 SourceSelector <- <('$' ('.' LowercaseIdentifier)* (('@' LowercaseIdentifier) / '@' / ('.' '*'))? METHOD?)> */
		nil,
		/* 76 BoolExpr <- <((NOTOP _)? Condition (_ BOOLOP _ (NOTOP _)? _ Condition)*)> */
		func() bool {
			position378, tokenIndex378, depth378 := position, tokenIndex, depth
			{
				position379 := position
				depth++
				{
					position380, tokenIndex380, depth380 := position, tokenIndex, depth
					if !_rules[ruleNOTOP]() {
						goto l380
					}
					if !_rules[rule_]() {
						goto l380
					}
					goto l381
				l380:
					position, tokenIndex, depth = position380, tokenIndex380, depth380
				}
			l381:
				if !_rules[ruleCondition]() {
					goto l378
				}
			l382:
				{
					position383, tokenIndex383, depth383 := position, tokenIndex, depth
					if !_rules[rule_]() {
						goto l383
					}
					{
						position384 := position
						depth++
						{
							position385, tokenIndex385, depth385 := position, tokenIndex, depth
							if buffer[position] != rune('|') {
								goto l386
							}
							position++
							if buffer[position] != rune('|') {
								goto l386
							}
							position++
							goto l385
						l386:
							position, tokenIndex, depth = position385, tokenIndex385, depth385
							if buffer[position] != rune('&') {
								goto l383
							}
							position++
							if buffer[position] != rune('&') {
								goto l383
							}
							position++
						}
					l385:
						depth--
						add(ruleBOOLOP, position384)
					}
					if !_rules[rule_]() {
						goto l383
					}
					{
						position387, tokenIndex387, depth387 := position, tokenIndex, depth
						if !_rules[ruleNOTOP]() {
							goto l387
						}
						if !_rules[rule_]() {
							goto l387
						}
						goto l388
					l387:
						position, tokenIndex, depth = position387, tokenIndex387, depth387
					}
				l388:
					if !_rules[rule_]() {
						goto l383
					}
					if !_rules[ruleCondition]() {
						goto l383
					}
					goto l382
				l383:
					position, tokenIndex, depth = position383, tokenIndex383, depth383
				}
				depth--
				add(ruleBoolExpr, position379)
			}
			return true
		l378:
			position, tokenIndex, depth = position378, tokenIndex378, depth378
			return false
		},
		/* 77 Condition <- <((Expr _ COMPARATOR _ Condition) / (Expr _ MATCHER _ REGEX) / Expr)> */
		func() bool {
			position389, tokenIndex389, depth389 := position, tokenIndex, depth
			{
				position390 := position
				depth++
				{
					position391, tokenIndex391, depth391 := position, tokenIndex, depth
					if !_rules[ruleExpr]() {
						goto l392
					}
					if !_rules[rule_]() {
						goto l392
					}
					{
						position393 := position
						depth++
						{
							position394, tokenIndex394, depth394 := position, tokenIndex, depth
							if buffer[position] != rune('<') {
								goto l395
							}
							position++
							goto l394
						l395:
							position, tokenIndex, depth = position394, tokenIndex394, depth394
							if buffer[position] != rune('>') {
								goto l396
							}
							position++
							goto l394
						l396:
							position, tokenIndex, depth = position394, tokenIndex394, depth394
							{
								switch buffer[position] {
								case '!':
									if buffer[position] != rune('!') {
										goto l392
									}
									position++
									if buffer[position] != rune('=') {
										goto l392
									}
									position++
									break
								case '=':
									if buffer[position] != rune('=') {
										goto l392
									}
									position++
									if buffer[position] != rune('=') {
										goto l392
									}
									position++
									break
								case '>':
									if buffer[position] != rune('>') {
										goto l392
									}
									position++
									if buffer[position] != rune('=') {
										goto l392
									}
									position++
									break
								default:
									if buffer[position] != rune('<') {
										goto l392
									}
									position++
									if buffer[position] != rune('=') {
										goto l392
									}
									position++
									break
								}
							}

						}
					l394:
						depth--
						add(ruleCOMPARATOR, position393)
					}
					if !_rules[rule_]() {
						goto l392
					}
					if !_rules[ruleCondition]() {
						goto l392
					}
					goto l391
				l392:
					position, tokenIndex, depth = position391, tokenIndex391, depth391
					if !_rules[ruleExpr]() {
						goto l398
					}
					if !_rules[rule_]() {
						goto l398
					}
					{
						position399 := position
						depth++
						if buffer[position] != rune('~') {
							goto l398
						}
						position++
						depth--
						add(ruleMATCHER, position399)
					}
					if !_rules[rule_]() {
						goto l398
					}
					{
						position400 := position
						depth++
						if buffer[position] != rune('/') {
							goto l398
						}
						position++
					l401:
						{
							position402, tokenIndex402, depth402 := position, tokenIndex, depth
							{
								position403, tokenIndex403, depth403 := position, tokenIndex, depth
								if buffer[position] != rune('/') {
									goto l403
								}
								position++
								goto l402
							l403:
								position, tokenIndex, depth = position403, tokenIndex403, depth403
							}
							if !matchDot() {
								goto l402
							}
							goto l401
						l402:
							position, tokenIndex, depth = position402, tokenIndex402, depth402
						}
						if buffer[position] != rune('/') {
							goto l398
						}
						position++
						depth--
						add(ruleREGEX, position400)
					}
					goto l391
				l398:
					position, tokenIndex, depth = position391, tokenIndex391, depth391
					if !_rules[ruleExpr]() {
						goto l389
					}
				}
			l391:
				depth--
				add(ruleCondition, position390)
			}
			return true
		l389:
			position, tokenIndex, depth = position389, tokenIndex389, depth389
			return false
		},
		/* 78 Expr <- <(_ (Function / ((&('(') ('(' _ BoolExpr _ ')')) | (&('\t' | '\n' | '\r' | ' ' | '[' | '\u00a0') Query) | (&('"' | '$' | '\'' | '+' | '-' | '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z' | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') Value))) (_ ExprExtension)?)> */
		func() bool {
			position404, tokenIndex404, depth404 := position, tokenIndex, depth
			{
				position405 := position
				depth++
				if !_rules[rule_]() {
					goto l404
				}
				{
					position406, tokenIndex406, depth406 := position, tokenIndex, depth
					if !_rules[ruleFunction]() {
						goto l407
					}
					goto l406
				l407:
					position, tokenIndex, depth = position406, tokenIndex406, depth406
					{
						switch buffer[position] {
						case '(':
							if buffer[position] != rune('(') {
								goto l404
							}
							position++
							if !_rules[rule_]() {
								goto l404
							}
							if !_rules[ruleBoolExpr]() {
								goto l404
							}
							if !_rules[rule_]() {
								goto l404
							}
							if buffer[position] != rune(')') {
								goto l404
							}
							position++
							break
						case '\t', '\n', '\r', ' ', '[', '\u00a0':
							if !_rules[ruleQuery]() {
								goto l404
							}
							break
						default:
							if !_rules[ruleValue]() {
								goto l404
							}
							break
						}
					}

				}
			l406:
				{
					position409, tokenIndex409, depth409 := position, tokenIndex, depth
					if !_rules[rule_]() {
						goto l409
					}
					if !_rules[ruleExprExtension]() {
						goto l409
					}
					goto l410
				l409:
					position, tokenIndex, depth = position409, tokenIndex409, depth409
				}
			l410:
				depth--
				add(ruleExpr, position405)
			}
			return true
		l404:
			position, tokenIndex, depth = position404, tokenIndex404, depth404
			return false
		},
		/* 79 Value <- <(FLOAT / TIME / DATETIME / DATE / ((&('"' | '\'') STRING) | (&('+' | '-' | '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') INTEGER) | (&('$' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z' | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') Selector)))> */
		func() bool {
			position411, tokenIndex411, depth411 := position, tokenIndex, depth
			{
				position412 := position
				depth++
				{
					position413, tokenIndex413, depth413 := position, tokenIndex, depth
					{
						position415 := position
						depth++
						if !_rules[ruleINTEGER]() {
							goto l414
						}
						if buffer[position] != rune('.') {
							goto l414
						}
						position++
						{
							position416, tokenIndex416, depth416 := position, tokenIndex, depth
							if !_rules[ruleCARDINAL]() {
								goto l416
							}
							goto l417
						l416:
							position, tokenIndex, depth = position416, tokenIndex416, depth416
						}
					l417:
						{
							position418, tokenIndex418, depth418 := position, tokenIndex, depth
							{
								position420 := position
								depth++
								{
									position421, tokenIndex421, depth421 := position, tokenIndex, depth
									if buffer[position] != rune('e') {
										goto l422
									}
									position++
									goto l421
								l422:
									position, tokenIndex, depth = position421, tokenIndex421, depth421
									if buffer[position] != rune('E') {
										goto l418
									}
									position++
								}
							l421:
								if !_rules[ruleINTEGER]() {
									goto l418
								}
								depth--
								add(ruleEXPONENT, position420)
							}
							goto l419
						l418:
							position, tokenIndex, depth = position418, tokenIndex418, depth418
						}
					l419:
						depth--
						add(ruleFLOAT, position415)
					}
					goto l413
				l414:
					position, tokenIndex, depth = position413, tokenIndex413, depth413
					if !_rules[ruleTIME]() {
						goto l423
					}
					goto l413
				l423:
					position, tokenIndex, depth = position413, tokenIndex413, depth413
					{
						position425 := position
						depth++
						if !_rules[ruleDATE]() {
							goto l424
						}
						{
							position426, tokenIndex426, depth426 := position, tokenIndex, depth
							if buffer[position] != rune('t') {
								goto l427
							}
							position++
							goto l426
						l427:
							position, tokenIndex, depth = position426, tokenIndex426, depth426
							if buffer[position] != rune('T') {
								goto l424
							}
							position++
						}
					l426:
						if !_rules[ruleTIME]() {
							goto l424
						}
						depth--
						add(ruleDATETIME, position425)
					}
					goto l413
				l424:
					position, tokenIndex, depth = position413, tokenIndex413, depth413
					if !_rules[ruleDATE]() {
						goto l428
					}
					goto l413
				l428:
					position, tokenIndex, depth = position413, tokenIndex413, depth413
					{
						switch buffer[position] {
						case '"', '\'':
							{
								position430 := position
								depth++
								{
									position431, tokenIndex431, depth431 := position, tokenIndex, depth
									if buffer[position] != rune('"') {
										goto l432
									}
									position++
								l433:
									{
										position434, tokenIndex434, depth434 := position, tokenIndex, depth
										{
											position435, tokenIndex435, depth435 := position, tokenIndex, depth
											if buffer[position] != rune('"') {
												goto l435
											}
											position++
											goto l434
										l435:
											position, tokenIndex, depth = position435, tokenIndex435, depth435
										}
										if !matchDot() {
											goto l434
										}
										goto l433
									l434:
										position, tokenIndex, depth = position434, tokenIndex434, depth434
									}
									if buffer[position] != rune('"') {
										goto l432
									}
									position++
									goto l431
								l432:
									position, tokenIndex, depth = position431, tokenIndex431, depth431
									if buffer[position] != rune('\'') {
										goto l411
									}
									position++
								l436:
									{
										position437, tokenIndex437, depth437 := position, tokenIndex, depth
										{
											position438, tokenIndex438, depth438 := position, tokenIndex, depth
											if buffer[position] != rune('\'') {
												goto l438
											}
											position++
											goto l437
										l438:
											position, tokenIndex, depth = position438, tokenIndex438, depth438
										}
										if !matchDot() {
											goto l437
										}
										goto l436
									l437:
										position, tokenIndex, depth = position437, tokenIndex437, depth437
									}
									if buffer[position] != rune('\'') {
										goto l411
									}
									position++
								}
							l431:
								depth--
								add(ruleSTRING, position430)
							}
							break
						case '+', '-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
							if !_rules[ruleINTEGER]() {
								goto l411
							}
							break
						default:
							if !_rules[ruleSelector]() {
								goto l411
							}
							break
						}
					}

				}
			l413:
				depth--
				add(ruleValue, position412)
			}
			return true
		l411:
			position, tokenIndex, depth = position411, tokenIndex411, depth411
			return false
		},
		/* 80 ExprExtension <- <(OP _ (Function / Value / Query) (_ ExprExtension)?)> */
		func() bool {
			position439, tokenIndex439, depth439 := position, tokenIndex, depth
			{
				position440 := position
				depth++
				{
					position441 := position
					depth++
					{
						switch buffer[position] {
						case '/':
							if buffer[position] != rune('/') {
								goto l439
							}
							position++
							break
						case '*':
							if buffer[position] != rune('*') {
								goto l439
							}
							position++
							break
						case '-':
							if buffer[position] != rune('-') {
								goto l439
							}
							position++
							break
						default:
							if buffer[position] != rune('+') {
								goto l439
							}
							position++
							break
						}
					}

					depth--
					add(ruleOP, position441)
				}
				if !_rules[rule_]() {
					goto l439
				}
				{
					position443, tokenIndex443, depth443 := position, tokenIndex, depth
					if !_rules[ruleFunction]() {
						goto l444
					}
					goto l443
				l444:
					position, tokenIndex, depth = position443, tokenIndex443, depth443
					if !_rules[ruleValue]() {
						goto l445
					}
					goto l443
				l445:
					position, tokenIndex, depth = position443, tokenIndex443, depth443
					if !_rules[ruleQuery]() {
						goto l439
					}
				}
			l443:
				{
					position446, tokenIndex446, depth446 := position, tokenIndex, depth
					if !_rules[rule_]() {
						goto l446
					}
					if !_rules[ruleExprExtension]() {
						goto l446
					}
					goto l447
				l446:
					position, tokenIndex, depth = position446, tokenIndex446, depth446
				}
			l447:
				depth--
				add(ruleExprExtension, position440)
			}
			return true
		l439:
			position, tokenIndex, depth = position439, tokenIndex439, depth439
			return false
		},
		/* 81 Function <- <((UppercaseIdentifier _ ':')? _ Identifier _ '(' _ Args? ')' METHOD?)> */
		func() bool {
			position448, tokenIndex448, depth448 := position, tokenIndex, depth
			{
				position449 := position
				depth++
				{
					position450, tokenIndex450, depth450 := position, tokenIndex, depth
					if !_rules[ruleUppercaseIdentifier]() {
						goto l450
					}
					if !_rules[rule_]() {
						goto l450
					}
					if buffer[position] != rune(':') {
						goto l450
					}
					position++
					goto l451
				l450:
					position, tokenIndex, depth = position450, tokenIndex450, depth450
				}
			l451:
				if !_rules[rule_]() {
					goto l448
				}
				{
					position452 := position
					depth++
					{
						position453 := position
						depth++
						{
							position454, tokenIndex454, depth454 := position, tokenIndex, depth
							if c := buffer[position]; c < rune('a') || c > rune('z') {
								goto l455
							}
							position++
							goto l454
						l455:
							position, tokenIndex, depth = position454, tokenIndex454, depth454
							if c := buffer[position]; c < rune('A') || c > rune('Z') {
								goto l448
							}
							position++
						}
					l454:
					l456:
						{
							position457, tokenIndex457, depth457 := position, tokenIndex, depth
							{
								switch buffer[position] {
								case '_':
									if buffer[position] != rune('_') {
										goto l457
									}
									position++
									break
								case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
									if c := buffer[position]; c < rune('0') || c > rune('9') {
										goto l457
									}
									position++
									break
								case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
									if c := buffer[position]; c < rune('A') || c > rune('Z') {
										goto l457
									}
									position++
									break
								case '-':
									if buffer[position] != rune('-') {
										goto l457
									}
									position++
									break
								default:
									if c := buffer[position]; c < rune('a') || c > rune('z') {
										goto l457
									}
									position++
									break
								}
							}

							goto l456
						l457:
							position, tokenIndex, depth = position457, tokenIndex457, depth457
						}
						depth--
						add(rulePegText, position453)
					}
					depth--
					add(ruleIdentifier, position452)
				}
				if !_rules[rule_]() {
					goto l448
				}
				if buffer[position] != rune('(') {
					goto l448
				}
				position++
				if !_rules[rule_]() {
					goto l448
				}
				{
					position459, tokenIndex459, depth459 := position, tokenIndex, depth
					{
						position461 := position
						depth++
						if !_rules[ruleValue]() {
							goto l459
						}
					l462:
						{
							position463, tokenIndex463, depth463 := position, tokenIndex, depth
							if !_rules[rule_]() {
								goto l463
							}
							if buffer[position] != rune(',') {
								goto l463
							}
							position++
							if !_rules[rule_]() {
								goto l463
							}
							if !_rules[ruleValue]() {
								goto l463
							}
							goto l462
						l463:
							position, tokenIndex, depth = position463, tokenIndex463, depth463
						}
						depth--
						add(ruleArgs, position461)
					}
					goto l460
				l459:
					position, tokenIndex, depth = position459, tokenIndex459, depth459
				}
			l460:
				if buffer[position] != rune(')') {
					goto l448
				}
				position++
				{
					position464, tokenIndex464, depth464 := position, tokenIndex, depth
					if !_rules[ruleMETHOD]() {
						goto l464
					}
					goto l465
				l464:
					position, tokenIndex, depth = position464, tokenIndex464, depth464
				}
			l465:
				depth--
				add(ruleFunction, position449)
			}
			return true
		l448:
			position, tokenIndex, depth = position448, tokenIndex448, depth448
			return false
		},
		/* 82 Args <- <(Value (_ ',' _ Value)*)> */
		nil,
		/* 83 Query <- <(COMMENTS? _ '[' Action4 ((_ '(' _ FLOWR _ ')' METHOD?) / (Element (FLOWR / (_ '(' _ FLOWR _ ')' METHOD?))) / FLOWR)? Action5 _ ']')> */
		func() bool {
			position467, tokenIndex467, depth467 := position, tokenIndex, depth
			{
				position468 := position
				depth++
				{
					position469, tokenIndex469, depth469 := position, tokenIndex, depth
					if !_rules[ruleCOMMENTS]() {
						goto l469
					}
					goto l470
				l469:
					position, tokenIndex, depth = position469, tokenIndex469, depth469
				}
			l470:
				if !_rules[rule_]() {
					goto l467
				}
				if buffer[position] != rune('[') {
					goto l467
				}
				position++
				{
					add(ruleAction4, position)
				}
				{
					position472, tokenIndex472, depth472 := position, tokenIndex, depth
					{
						position474, tokenIndex474, depth474 := position, tokenIndex, depth
						if !_rules[rule_]() {
							goto l475
						}
						if buffer[position] != rune('(') {
							goto l475
						}
						position++
						if !_rules[rule_]() {
							goto l475
						}
						if !_rules[ruleFLOWR]() {
							goto l475
						}
						if !_rules[rule_]() {
							goto l475
						}
						if buffer[position] != rune(')') {
							goto l475
						}
						position++
						{
							position476, tokenIndex476, depth476 := position, tokenIndex, depth
							if !_rules[ruleMETHOD]() {
								goto l476
							}
							goto l477
						l476:
							position, tokenIndex, depth = position476, tokenIndex476, depth476
						}
					l477:
						goto l474
					l475:
						position, tokenIndex, depth = position474, tokenIndex474, depth474
						if !_rules[ruleElement]() {
							goto l478
						}
						{
							position479, tokenIndex479, depth479 := position, tokenIndex, depth
							if !_rules[ruleFLOWR]() {
								goto l480
							}
							goto l479
						l480:
							position, tokenIndex, depth = position479, tokenIndex479, depth479
							if !_rules[rule_]() {
								goto l478
							}
							if buffer[position] != rune('(') {
								goto l478
							}
							position++
							if !_rules[rule_]() {
								goto l478
							}
							if !_rules[ruleFLOWR]() {
								goto l478
							}
							if !_rules[rule_]() {
								goto l478
							}
							if buffer[position] != rune(')') {
								goto l478
							}
							position++
							{
								position481, tokenIndex481, depth481 := position, tokenIndex, depth
								if !_rules[ruleMETHOD]() {
									goto l481
								}
								goto l482
							l481:
								position, tokenIndex, depth = position481, tokenIndex481, depth481
							}
						l482:
						}
					l479:
						goto l474
					l478:
						position, tokenIndex, depth = position474, tokenIndex474, depth474
						if !_rules[ruleFLOWR]() {
							goto l472
						}
					}
				l474:
					goto l473
				l472:
					position, tokenIndex, depth = position472, tokenIndex472, depth472
				}
			l473:
				{
					add(ruleAction5, position)
				}
				if !_rules[rule_]() {
					goto l467
				}
				if buffer[position] != rune(']') {
					goto l467
				}
				position++
				depth--
				add(ruleQuery, position468)
			}
			return true
		l467:
			position, tokenIndex, depth = position467, tokenIndex467, depth467
			return false
		},
		/* 84 FLOWR <- <(ForeachExpr LetExpr* WhereExpr? DistinctExpr? OrderByExpr* SkipExpr? TakeExpr? LetExpr* (ReturnExpr / FLOWR))> */
		func() bool {
			position484, tokenIndex484, depth484 := position, tokenIndex, depth
			{
				position485 := position
				depth++
				{
					position486 := position
					depth++
					if !_rules[rule_]() {
						goto l484
					}
					{
						position487 := position
						depth++
						{
							position488, tokenIndex488, depth488 := position, tokenIndex, depth
							if buffer[position] != rune('f') {
								goto l489
							}
							position++
							goto l488
						l489:
							position, tokenIndex, depth = position488, tokenIndex488, depth488
							if buffer[position] != rune('F') {
								goto l484
							}
							position++
						}
					l488:
						{
							position490, tokenIndex490, depth490 := position, tokenIndex, depth
							if buffer[position] != rune('o') {
								goto l491
							}
							position++
							goto l490
						l491:
							position, tokenIndex, depth = position490, tokenIndex490, depth490
							if buffer[position] != rune('O') {
								goto l484
							}
							position++
						}
					l490:
						{
							position492, tokenIndex492, depth492 := position, tokenIndex, depth
							if buffer[position] != rune('r') {
								goto l493
							}
							position++
							goto l492
						l493:
							position, tokenIndex, depth = position492, tokenIndex492, depth492
							if buffer[position] != rune('R') {
								goto l484
							}
							position++
						}
					l492:
						depth--
						add(ruleFOR, position487)
					}
					{
						position494, tokenIndex494, depth494 := position, tokenIndex, depth
						if !_rules[rule_]() {
							goto l494
						}
						{
							position496 := position
							depth++
							{
								position497, tokenIndex497, depth497 := position, tokenIndex, depth
								if buffer[position] != rune('e') {
									goto l498
								}
								position++
								goto l497
							l498:
								position, tokenIndex, depth = position497, tokenIndex497, depth497
								if buffer[position] != rune('E') {
									goto l494
								}
								position++
							}
						l497:
							{
								position499, tokenIndex499, depth499 := position, tokenIndex, depth
								if buffer[position] != rune('a') {
									goto l500
								}
								position++
								goto l499
							l500:
								position, tokenIndex, depth = position499, tokenIndex499, depth499
								if buffer[position] != rune('A') {
									goto l494
								}
								position++
							}
						l499:
							{
								position501, tokenIndex501, depth501 := position, tokenIndex, depth
								if buffer[position] != rune('c') {
									goto l502
								}
								position++
								goto l501
							l502:
								position, tokenIndex, depth = position501, tokenIndex501, depth501
								if buffer[position] != rune('C') {
									goto l494
								}
								position++
							}
						l501:
							{
								position503, tokenIndex503, depth503 := position, tokenIndex, depth
								if buffer[position] != rune('h') {
									goto l504
								}
								position++
								goto l503
							l504:
								position, tokenIndex, depth = position503, tokenIndex503, depth503
								if buffer[position] != rune('H') {
									goto l494
								}
								position++
							}
						l503:
							depth--
							add(ruleEACH, position496)
						}
						goto l495
					l494:
						position, tokenIndex, depth = position494, tokenIndex494, depth494
					}
				l495:
					if !_rules[rule__]() {
						goto l484
					}
					if !_rules[ruleLowercaseIdentifier]() {
						goto l484
					}
					{
						add(ruleAction6, position)
					}
					{
						position506, tokenIndex506, depth506 := position, tokenIndex, depth
						if !_rules[rule__]() {
							goto l506
						}
						{
							position508 := position
							depth++
							{
								position509, tokenIndex509, depth509 := position, tokenIndex, depth
								if buffer[position] != rune('i') {
									goto l510
								}
								position++
								goto l509
							l510:
								position, tokenIndex, depth = position509, tokenIndex509, depth509
								if buffer[position] != rune('I') {
									goto l506
								}
								position++
							}
						l509:
							{
								position511, tokenIndex511, depth511 := position, tokenIndex, depth
								if buffer[position] != rune('n') {
									goto l512
								}
								position++
								goto l511
							l512:
								position, tokenIndex, depth = position511, tokenIndex511, depth511
								if buffer[position] != rune('N') {
									goto l506
								}
								position++
							}
						l511:
							depth--
							add(ruleIN, position508)
						}
						if !_rules[rule__]() {
							goto l506
						}
						{
							position513, tokenIndex513, depth513 := position, tokenIndex, depth
							if !_rules[ruleFunction]() {
								goto l514
							}
							goto l513
						l514:
							position, tokenIndex, depth = position513, tokenIndex513, depth513
							if !_rules[ruleSelector]() {
								goto l506
							}
						}
					l513:
						goto l507
					l506:
						position, tokenIndex, depth = position506, tokenIndex506, depth506
					}
				l507:
					depth--
					add(ruleForeachExpr, position486)
				}
			l515:
				{
					position516, tokenIndex516, depth516 := position, tokenIndex, depth
					if !_rules[ruleLetExpr]() {
						goto l516
					}
					goto l515
				l516:
					position, tokenIndex, depth = position516, tokenIndex516, depth516
				}
				{
					position517, tokenIndex517, depth517 := position, tokenIndex, depth
					{
						position519 := position
						depth++
						if !_rules[rule_]() {
							goto l517
						}
						{
							position520 := position
							depth++
							{
								position521, tokenIndex521, depth521 := position, tokenIndex, depth
								if buffer[position] != rune('w') {
									goto l522
								}
								position++
								goto l521
							l522:
								position, tokenIndex, depth = position521, tokenIndex521, depth521
								if buffer[position] != rune('W') {
									goto l517
								}
								position++
							}
						l521:
							{
								position523, tokenIndex523, depth523 := position, tokenIndex, depth
								if buffer[position] != rune('h') {
									goto l524
								}
								position++
								goto l523
							l524:
								position, tokenIndex, depth = position523, tokenIndex523, depth523
								if buffer[position] != rune('H') {
									goto l517
								}
								position++
							}
						l523:
							{
								position525, tokenIndex525, depth525 := position, tokenIndex, depth
								if buffer[position] != rune('e') {
									goto l526
								}
								position++
								goto l525
							l526:
								position, tokenIndex, depth = position525, tokenIndex525, depth525
								if buffer[position] != rune('E') {
									goto l517
								}
								position++
							}
						l525:
							{
								position527, tokenIndex527, depth527 := position, tokenIndex, depth
								if buffer[position] != rune('r') {
									goto l528
								}
								position++
								goto l527
							l528:
								position, tokenIndex, depth = position527, tokenIndex527, depth527
								if buffer[position] != rune('R') {
									goto l517
								}
								position++
							}
						l527:
							{
								position529, tokenIndex529, depth529 := position, tokenIndex, depth
								if buffer[position] != rune('e') {
									goto l530
								}
								position++
								goto l529
							l530:
								position, tokenIndex, depth = position529, tokenIndex529, depth529
								if buffer[position] != rune('E') {
									goto l517
								}
								position++
							}
						l529:
							depth--
							add(ruleWHERE, position520)
						}
						if !_rules[rule__]() {
							goto l517
						}
						if !_rules[ruleBoolExpr]() {
							goto l517
						}
						depth--
						add(ruleWhereExpr, position519)
					}
					goto l518
				l517:
					position, tokenIndex, depth = position517, tokenIndex517, depth517
				}
			l518:
				{
					position531, tokenIndex531, depth531 := position, tokenIndex, depth
					{
						position533 := position
						depth++
						if !_rules[rule_]() {
							goto l531
						}
						{
							position534 := position
							depth++
							{
								position535, tokenIndex535, depth535 := position, tokenIndex, depth
								if buffer[position] != rune('d') {
									goto l536
								}
								position++
								goto l535
							l536:
								position, tokenIndex, depth = position535, tokenIndex535, depth535
								if buffer[position] != rune('D') {
									goto l531
								}
								position++
							}
						l535:
							{
								position537, tokenIndex537, depth537 := position, tokenIndex, depth
								if buffer[position] != rune('i') {
									goto l538
								}
								position++
								goto l537
							l538:
								position, tokenIndex, depth = position537, tokenIndex537, depth537
								if buffer[position] != rune('I') {
									goto l531
								}
								position++
							}
						l537:
							{
								position539, tokenIndex539, depth539 := position, tokenIndex, depth
								if buffer[position] != rune('s') {
									goto l540
								}
								position++
								goto l539
							l540:
								position, tokenIndex, depth = position539, tokenIndex539, depth539
								if buffer[position] != rune('S') {
									goto l531
								}
								position++
							}
						l539:
							{
								position541, tokenIndex541, depth541 := position, tokenIndex, depth
								if buffer[position] != rune('t') {
									goto l542
								}
								position++
								goto l541
							l542:
								position, tokenIndex, depth = position541, tokenIndex541, depth541
								if buffer[position] != rune('T') {
									goto l531
								}
								position++
							}
						l541:
							{
								position543, tokenIndex543, depth543 := position, tokenIndex, depth
								if buffer[position] != rune('i') {
									goto l544
								}
								position++
								goto l543
							l544:
								position, tokenIndex, depth = position543, tokenIndex543, depth543
								if buffer[position] != rune('I') {
									goto l531
								}
								position++
							}
						l543:
							{
								position545, tokenIndex545, depth545 := position, tokenIndex, depth
								if buffer[position] != rune('n') {
									goto l546
								}
								position++
								goto l545
							l546:
								position, tokenIndex, depth = position545, tokenIndex545, depth545
								if buffer[position] != rune('N') {
									goto l531
								}
								position++
							}
						l545:
							{
								position547, tokenIndex547, depth547 := position, tokenIndex, depth
								if buffer[position] != rune('c') {
									goto l548
								}
								position++
								goto l547
							l548:
								position, tokenIndex, depth = position547, tokenIndex547, depth547
								if buffer[position] != rune('C') {
									goto l531
								}
								position++
							}
						l547:
							{
								position549, tokenIndex549, depth549 := position, tokenIndex, depth
								if buffer[position] != rune('t') {
									goto l550
								}
								position++
								goto l549
							l550:
								position, tokenIndex, depth = position549, tokenIndex549, depth549
								if buffer[position] != rune('T') {
									goto l531
								}
								position++
							}
						l549:
							depth--
							add(ruleDISTINCT, position534)
						}
						depth--
						add(ruleDistinctExpr, position533)
					}
					goto l532
				l531:
					position, tokenIndex, depth = position531, tokenIndex531, depth531
				}
			l532:
			l551:
				{
					position552, tokenIndex552, depth552 := position, tokenIndex, depth
					{
						position553 := position
						depth++
						if !_rules[rule_]() {
							goto l552
						}
						{
							position554 := position
							depth++
							{
								position555, tokenIndex555, depth555 := position, tokenIndex, depth
								if buffer[position] != rune('o') {
									goto l556
								}
								position++
								goto l555
							l556:
								position, tokenIndex, depth = position555, tokenIndex555, depth555
								if buffer[position] != rune('O') {
									goto l552
								}
								position++
							}
						l555:
							{
								position557, tokenIndex557, depth557 := position, tokenIndex, depth
								if buffer[position] != rune('r') {
									goto l558
								}
								position++
								goto l557
							l558:
								position, tokenIndex, depth = position557, tokenIndex557, depth557
								if buffer[position] != rune('R') {
									goto l552
								}
								position++
							}
						l557:
							{
								position559, tokenIndex559, depth559 := position, tokenIndex, depth
								if buffer[position] != rune('d') {
									goto l560
								}
								position++
								goto l559
							l560:
								position, tokenIndex, depth = position559, tokenIndex559, depth559
								if buffer[position] != rune('D') {
									goto l552
								}
								position++
							}
						l559:
							{
								position561, tokenIndex561, depth561 := position, tokenIndex, depth
								if buffer[position] != rune('e') {
									goto l562
								}
								position++
								goto l561
							l562:
								position, tokenIndex, depth = position561, tokenIndex561, depth561
								if buffer[position] != rune('E') {
									goto l552
								}
								position++
							}
						l561:
							{
								position563, tokenIndex563, depth563 := position, tokenIndex, depth
								if buffer[position] != rune('r') {
									goto l564
								}
								position++
								goto l563
							l564:
								position, tokenIndex, depth = position563, tokenIndex563, depth563
								if buffer[position] != rune('R') {
									goto l552
								}
								position++
							}
						l563:
							depth--
							add(ruleORDER, position554)
						}
						if !_rules[rule_]() {
							goto l552
						}
						{
							position565 := position
							depth++
							{
								position566, tokenIndex566, depth566 := position, tokenIndex, depth
								if buffer[position] != rune('b') {
									goto l567
								}
								position++
								goto l566
							l567:
								position, tokenIndex, depth = position566, tokenIndex566, depth566
								if buffer[position] != rune('B') {
									goto l552
								}
								position++
							}
						l566:
							{
								position568, tokenIndex568, depth568 := position, tokenIndex, depth
								if buffer[position] != rune('y') {
									goto l569
								}
								position++
								goto l568
							l569:
								position, tokenIndex, depth = position568, tokenIndex568, depth568
								if buffer[position] != rune('Y') {
									goto l552
								}
								position++
							}
						l568:
							depth--
							add(ruleBY, position565)
						}
						if !_rules[rule__]() {
							goto l552
						}
						if !_rules[ruleExpr]() {
							goto l552
						}
						{
							position570, tokenIndex570, depth570 := position, tokenIndex, depth
							{
								position572, tokenIndex572, depth572 := position, tokenIndex, depth
								if !_rules[rule__]() {
									goto l573
								}
								{
									position574 := position
									depth++
									{
										position575, tokenIndex575, depth575 := position, tokenIndex, depth
										if buffer[position] != rune('a') {
											goto l576
										}
										position++
										goto l575
									l576:
										position, tokenIndex, depth = position575, tokenIndex575, depth575
										if buffer[position] != rune('A') {
											goto l573
										}
										position++
									}
								l575:
									{
										position577, tokenIndex577, depth577 := position, tokenIndex, depth
										if buffer[position] != rune('s') {
											goto l578
										}
										position++
										goto l577
									l578:
										position, tokenIndex, depth = position577, tokenIndex577, depth577
										if buffer[position] != rune('S') {
											goto l573
										}
										position++
									}
								l577:
									{
										position579, tokenIndex579, depth579 := position, tokenIndex, depth
										if buffer[position] != rune('c') {
											goto l580
										}
										position++
										goto l579
									l580:
										position, tokenIndex, depth = position579, tokenIndex579, depth579
										if buffer[position] != rune('C') {
											goto l573
										}
										position++
									}
								l579:
									{
										position581, tokenIndex581, depth581 := position, tokenIndex, depth
										if buffer[position] != rune('e') {
											goto l582
										}
										position++
										goto l581
									l582:
										position, tokenIndex, depth = position581, tokenIndex581, depth581
										if buffer[position] != rune('E') {
											goto l573
										}
										position++
									}
								l581:
									{
										position583, tokenIndex583, depth583 := position, tokenIndex, depth
										if buffer[position] != rune('n') {
											goto l584
										}
										position++
										goto l583
									l584:
										position, tokenIndex, depth = position583, tokenIndex583, depth583
										if buffer[position] != rune('N') {
											goto l573
										}
										position++
									}
								l583:
									{
										position585, tokenIndex585, depth585 := position, tokenIndex, depth
										if buffer[position] != rune('d') {
											goto l586
										}
										position++
										goto l585
									l586:
										position, tokenIndex, depth = position585, tokenIndex585, depth585
										if buffer[position] != rune('D') {
											goto l573
										}
										position++
									}
								l585:
									{
										position587, tokenIndex587, depth587 := position, tokenIndex, depth
										if buffer[position] != rune('i') {
											goto l588
										}
										position++
										goto l587
									l588:
										position, tokenIndex, depth = position587, tokenIndex587, depth587
										if buffer[position] != rune('I') {
											goto l573
										}
										position++
									}
								l587:
									{
										position589, tokenIndex589, depth589 := position, tokenIndex, depth
										if buffer[position] != rune('n') {
											goto l590
										}
										position++
										goto l589
									l590:
										position, tokenIndex, depth = position589, tokenIndex589, depth589
										if buffer[position] != rune('N') {
											goto l573
										}
										position++
									}
								l589:
									{
										position591, tokenIndex591, depth591 := position, tokenIndex, depth
										if buffer[position] != rune('g') {
											goto l592
										}
										position++
										goto l591
									l592:
										position, tokenIndex, depth = position591, tokenIndex591, depth591
										if buffer[position] != rune('G') {
											goto l573
										}
										position++
									}
								l591:
									depth--
									add(ruleASCENDING, position574)
								}
								goto l572
							l573:
								position, tokenIndex, depth = position572, tokenIndex572, depth572
								if !_rules[rule__]() {
									goto l570
								}
								{
									position593 := position
									depth++
									{
										position594, tokenIndex594, depth594 := position, tokenIndex, depth
										if buffer[position] != rune('d') {
											goto l595
										}
										position++
										goto l594
									l595:
										position, tokenIndex, depth = position594, tokenIndex594, depth594
										if buffer[position] != rune('D') {
											goto l570
										}
										position++
									}
								l594:
									{
										position596, tokenIndex596, depth596 := position, tokenIndex, depth
										if buffer[position] != rune('e') {
											goto l597
										}
										position++
										goto l596
									l597:
										position, tokenIndex, depth = position596, tokenIndex596, depth596
										if buffer[position] != rune('E') {
											goto l570
										}
										position++
									}
								l596:
									{
										position598, tokenIndex598, depth598 := position, tokenIndex, depth
										if buffer[position] != rune('s') {
											goto l599
										}
										position++
										goto l598
									l599:
										position, tokenIndex, depth = position598, tokenIndex598, depth598
										if buffer[position] != rune('S') {
											goto l570
										}
										position++
									}
								l598:
									{
										position600, tokenIndex600, depth600 := position, tokenIndex, depth
										if buffer[position] != rune('c') {
											goto l601
										}
										position++
										goto l600
									l601:
										position, tokenIndex, depth = position600, tokenIndex600, depth600
										if buffer[position] != rune('C') {
											goto l570
										}
										position++
									}
								l600:
									{
										position602, tokenIndex602, depth602 := position, tokenIndex, depth
										if buffer[position] != rune('e') {
											goto l603
										}
										position++
										goto l602
									l603:
										position, tokenIndex, depth = position602, tokenIndex602, depth602
										if buffer[position] != rune('E') {
											goto l570
										}
										position++
									}
								l602:
									{
										position604, tokenIndex604, depth604 := position, tokenIndex, depth
										if buffer[position] != rune('n') {
											goto l605
										}
										position++
										goto l604
									l605:
										position, tokenIndex, depth = position604, tokenIndex604, depth604
										if buffer[position] != rune('N') {
											goto l570
										}
										position++
									}
								l604:
									{
										position606, tokenIndex606, depth606 := position, tokenIndex, depth
										if buffer[position] != rune('d') {
											goto l607
										}
										position++
										goto l606
									l607:
										position, tokenIndex, depth = position606, tokenIndex606, depth606
										if buffer[position] != rune('D') {
											goto l570
										}
										position++
									}
								l606:
									{
										position608, tokenIndex608, depth608 := position, tokenIndex, depth
										if buffer[position] != rune('i') {
											goto l609
										}
										position++
										goto l608
									l609:
										position, tokenIndex, depth = position608, tokenIndex608, depth608
										if buffer[position] != rune('I') {
											goto l570
										}
										position++
									}
								l608:
									{
										position610, tokenIndex610, depth610 := position, tokenIndex, depth
										if buffer[position] != rune('n') {
											goto l611
										}
										position++
										goto l610
									l611:
										position, tokenIndex, depth = position610, tokenIndex610, depth610
										if buffer[position] != rune('N') {
											goto l570
										}
										position++
									}
								l610:
									{
										position612, tokenIndex612, depth612 := position, tokenIndex, depth
										if buffer[position] != rune('g') {
											goto l613
										}
										position++
										goto l612
									l613:
										position, tokenIndex, depth = position612, tokenIndex612, depth612
										if buffer[position] != rune('G') {
											goto l570
										}
										position++
									}
								l612:
									depth--
									add(ruleDESCENDING, position593)
								}
							}
						l572:
							goto l571
						l570:
							position, tokenIndex, depth = position570, tokenIndex570, depth570
						}
					l571:
						depth--
						add(ruleOrderByExpr, position553)
					}
					goto l551
				l552:
					position, tokenIndex, depth = position552, tokenIndex552, depth552
				}
				{
					position614, tokenIndex614, depth614 := position, tokenIndex, depth
					{
						position616 := position
						depth++
						if !_rules[rule_]() {
							goto l614
						}
						{
							position617 := position
							depth++
							{
								position618, tokenIndex618, depth618 := position, tokenIndex, depth
								if buffer[position] != rune('s') {
									goto l619
								}
								position++
								goto l618
							l619:
								position, tokenIndex, depth = position618, tokenIndex618, depth618
								if buffer[position] != rune('S') {
									goto l614
								}
								position++
							}
						l618:
							{
								position620, tokenIndex620, depth620 := position, tokenIndex, depth
								if buffer[position] != rune('k') {
									goto l621
								}
								position++
								goto l620
							l621:
								position, tokenIndex, depth = position620, tokenIndex620, depth620
								if buffer[position] != rune('K') {
									goto l614
								}
								position++
							}
						l620:
							{
								position622, tokenIndex622, depth622 := position, tokenIndex, depth
								if buffer[position] != rune('i') {
									goto l623
								}
								position++
								goto l622
							l623:
								position, tokenIndex, depth = position622, tokenIndex622, depth622
								if buffer[position] != rune('I') {
									goto l614
								}
								position++
							}
						l622:
							{
								position624, tokenIndex624, depth624 := position, tokenIndex, depth
								if buffer[position] != rune('p') {
									goto l625
								}
								position++
								goto l624
							l625:
								position, tokenIndex, depth = position624, tokenIndex624, depth624
								if buffer[position] != rune('P') {
									goto l614
								}
								position++
							}
						l624:
							depth--
							add(ruleSKIP, position617)
						}
						if !_rules[rule__]() {
							goto l614
						}
						if !_rules[ruleExpr]() {
							goto l614
						}
						depth--
						add(ruleSkipExpr, position616)
					}
					goto l615
				l614:
					position, tokenIndex, depth = position614, tokenIndex614, depth614
				}
			l615:
				{
					position626, tokenIndex626, depth626 := position, tokenIndex, depth
					{
						position628 := position
						depth++
						if !_rules[rule_]() {
							goto l626
						}
						{
							position629 := position
							depth++
							{
								position630, tokenIndex630, depth630 := position, tokenIndex, depth
								if buffer[position] != rune('t') {
									goto l631
								}
								position++
								goto l630
							l631:
								position, tokenIndex, depth = position630, tokenIndex630, depth630
								if buffer[position] != rune('T') {
									goto l626
								}
								position++
							}
						l630:
							{
								position632, tokenIndex632, depth632 := position, tokenIndex, depth
								if buffer[position] != rune('a') {
									goto l633
								}
								position++
								goto l632
							l633:
								position, tokenIndex, depth = position632, tokenIndex632, depth632
								if buffer[position] != rune('A') {
									goto l626
								}
								position++
							}
						l632:
							{
								position634, tokenIndex634, depth634 := position, tokenIndex, depth
								if buffer[position] != rune('k') {
									goto l635
								}
								position++
								goto l634
							l635:
								position, tokenIndex, depth = position634, tokenIndex634, depth634
								if buffer[position] != rune('K') {
									goto l626
								}
								position++
							}
						l634:
							{
								position636, tokenIndex636, depth636 := position, tokenIndex, depth
								if buffer[position] != rune('e') {
									goto l637
								}
								position++
								goto l636
							l637:
								position, tokenIndex, depth = position636, tokenIndex636, depth636
								if buffer[position] != rune('E') {
									goto l626
								}
								position++
							}
						l636:
							depth--
							add(ruleTAKE, position629)
						}
						if !_rules[rule__]() {
							goto l626
						}
						if !_rules[ruleExpr]() {
							goto l626
						}
						depth--
						add(ruleTakeExpr, position628)
					}
					goto l627
				l626:
					position, tokenIndex, depth = position626, tokenIndex626, depth626
				}
			l627:
			l638:
				{
					position639, tokenIndex639, depth639 := position, tokenIndex, depth
					if !_rules[ruleLetExpr]() {
						goto l639
					}
					goto l638
				l639:
					position, tokenIndex, depth = position639, tokenIndex639, depth639
				}
				{
					position640, tokenIndex640, depth640 := position, tokenIndex, depth
					{
						position642 := position
						depth++
						if !_rules[rule_]() {
							goto l641
						}
						{
							position643 := position
							depth++
							{
								position644, tokenIndex644, depth644 := position, tokenIndex, depth
								if buffer[position] != rune('r') {
									goto l645
								}
								position++
								goto l644
							l645:
								position, tokenIndex, depth = position644, tokenIndex644, depth644
								if buffer[position] != rune('R') {
									goto l641
								}
								position++
							}
						l644:
							{
								position646, tokenIndex646, depth646 := position, tokenIndex, depth
								if buffer[position] != rune('e') {
									goto l647
								}
								position++
								goto l646
							l647:
								position, tokenIndex, depth = position646, tokenIndex646, depth646
								if buffer[position] != rune('E') {
									goto l641
								}
								position++
							}
						l646:
							{
								position648, tokenIndex648, depth648 := position, tokenIndex, depth
								if buffer[position] != rune('t') {
									goto l649
								}
								position++
								goto l648
							l649:
								position, tokenIndex, depth = position648, tokenIndex648, depth648
								if buffer[position] != rune('T') {
									goto l641
								}
								position++
							}
						l648:
							{
								position650, tokenIndex650, depth650 := position, tokenIndex, depth
								if buffer[position] != rune('u') {
									goto l651
								}
								position++
								goto l650
							l651:
								position, tokenIndex, depth = position650, tokenIndex650, depth650
								if buffer[position] != rune('U') {
									goto l641
								}
								position++
							}
						l650:
							{
								position652, tokenIndex652, depth652 := position, tokenIndex, depth
								if buffer[position] != rune('r') {
									goto l653
								}
								position++
								goto l652
							l653:
								position, tokenIndex, depth = position652, tokenIndex652, depth652
								if buffer[position] != rune('R') {
									goto l641
								}
								position++
							}
						l652:
							{
								position654, tokenIndex654, depth654 := position, tokenIndex, depth
								if buffer[position] != rune('n') {
									goto l655
								}
								position++
								goto l654
							l655:
								position, tokenIndex, depth = position654, tokenIndex654, depth654
								if buffer[position] != rune('N') {
									goto l641
								}
								position++
							}
						l654:
							depth--
							add(ruleRETURN, position643)
						}
						if !_rules[rule__]() {
							goto l641
						}
						{
							position656, tokenIndex656, depth656 := position, tokenIndex, depth
							if !_rules[ruleElement]() {
								goto l657
							}
							goto l656
						l657:
							position, tokenIndex, depth = position656, tokenIndex656, depth656
							if !_rules[ruleExpr]() {
								goto l641
							}
						}
					l656:
						depth--
						add(ruleReturnExpr, position642)
					}
					goto l640
				l641:
					position, tokenIndex, depth = position640, tokenIndex640, depth640
					if !_rules[ruleFLOWR]() {
						goto l484
					}
				}
			l640:
				depth--
				add(ruleFLOWR, position485)
			}
			return true
		l484:
			position, tokenIndex, depth = position484, tokenIndex484, depth484
			return false
		},
		/* 85 ForeachExpr <- <(_ FOR (_ EACH)? __ LowercaseIdentifier Action6 (__ IN __ (Function / Selector))?)> */
		nil,
		/* 86 WhereExpr <- <(_ WHERE __ BoolExpr)> */
		nil,
		/* 87 DistinctExpr <- <(_ DISTINCT)> */
		nil,
		/* 88 OrderByExpr <- <(_ ORDER _ BY __ Expr ((__ ASCENDING) / (__ DESCENDING))?)> */
		nil,
		/* 89 TakeExpr <- <(_ TAKE __ Expr)> */
		nil,
		/* 90 SkipExpr <- <(_ SKIP __ Expr)> */
		nil,
		/* 91 LetExpr <- <(_ LET __ LowercaseIdentifier Action7 _ '=' _ Expr)> */
		func() bool {
			position664, tokenIndex664, depth664 := position, tokenIndex, depth
			{
				position665 := position
				depth++
				if !_rules[rule_]() {
					goto l664
				}
				{
					position666 := position
					depth++
					{
						position667, tokenIndex667, depth667 := position, tokenIndex, depth
						if buffer[position] != rune('l') {
							goto l668
						}
						position++
						goto l667
					l668:
						position, tokenIndex, depth = position667, tokenIndex667, depth667
						if buffer[position] != rune('L') {
							goto l664
						}
						position++
					}
				l667:
					{
						position669, tokenIndex669, depth669 := position, tokenIndex, depth
						if buffer[position] != rune('e') {
							goto l670
						}
						position++
						goto l669
					l670:
						position, tokenIndex, depth = position669, tokenIndex669, depth669
						if buffer[position] != rune('E') {
							goto l664
						}
						position++
					}
				l669:
					{
						position671, tokenIndex671, depth671 := position, tokenIndex, depth
						if buffer[position] != rune('t') {
							goto l672
						}
						position++
						goto l671
					l672:
						position, tokenIndex, depth = position671, tokenIndex671, depth671
						if buffer[position] != rune('T') {
							goto l664
						}
						position++
					}
				l671:
					depth--
					add(ruleLET, position666)
				}
				if !_rules[rule__]() {
					goto l664
				}
				if !_rules[ruleLowercaseIdentifier]() {
					goto l664
				}
				{
					add(ruleAction7, position)
				}
				if !_rules[rule_]() {
					goto l664
				}
				if buffer[position] != rune('=') {
					goto l664
				}
				position++
				if !_rules[rule_]() {
					goto l664
				}
				if !_rules[ruleExpr]() {
					goto l664
				}
				depth--
				add(ruleLetExpr, position665)
			}
			return true
		l664:
			position, tokenIndex, depth = position664, tokenIndex664, depth664
			return false
		},
		/* 92 ReturnExpr <- <(_ RETURN __ (Element / Expr))> */
		nil,
		/* 94 Action0 <- <{
		   // p.models.Print()
		   // p.vars.Print()
		 }> */
		nil,
		nil,
		/* 96 Action1 <- <{ p.models.Push(text) }> */
		nil,
		/* 97 Action2 <- <{ p.vars.Check(text) }> */
		nil,
		/* 98 Action3 <- <{ p.models.Check(text) }> */
		nil,
		/* 99 Action4 <- <{ p.env.Push(p.vars, p.models) }> */
		nil,
		/* 100 Action5 <- <{ p.vars, p.models = p.env.Pop() }> */
		nil,
		/* 101 Action6 <- <{ p.vars.Push(text) }> */
		nil,
		/* 102 Action7 <- <{ p.vars.Push(text) }> */
		nil,
	}
	p.rules = _rules
}
