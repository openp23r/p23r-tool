// TestParserExamples
// 	Given real examples
// 		When the example from the P23R handbook is given
// 			It should be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserExamples(t *testing.T) {

	Convey("Given real examples", t, func() {

		Convey("When the example from the P23R handbook is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
# Selection.dats

Zunächst werden die Antragstellerdaten dem Modell Applicant zugeordnet.

	source = http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/enterpriseCore1-0
	selection {
		@xmlns { "http://leitstelle.p23r.de/NS/P23R/ruleGroups/Starter/Selection1-0" }

Und dann ein Profile erstellt

		[
			for each applicant in $.enterpriseCore.profile
			take 1
			return profile {
				@name { applicant.company.name }
				[
					for each location in applicant.headquarters.addresses
					take -2
					return addresses {
						@validFrom { location.validFrom }
						@lastModified { location.lastModified }
						@houseNumber { location.housenumber }
						@street { location.street }
						@city { location.city }
						@zipCode { location.zipCode }
						@country { location.country }
					}
				]
			}
		]
	}
`), ShouldBeNil)
			})

		})

		Convey("When the example from ADPSW is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`Recommendation.dats

Pfad:     <
<Rule Package Name>>/<
  <Release>>/<
    <Rule Group>>/
Präsenz:  verbindlich
Referenz: T-BRS Kapitel 5 / Tabelle 32
Inhalt:   P23R Selection Skript zur Empfehlung einer Benachrichtigungsregelgruppe

	recommendationResult {
		@namespace { "http://leitstelle.p23r.de/NS/p23r/nrl/RecommendationResult1-1" }
			@recommended { "true" }
	}
`), ShouldBeNil)
			})

		})

	})

}
