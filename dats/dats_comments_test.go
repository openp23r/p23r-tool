// TestParserComments
// 	Given a comment in selection expression for the processor
// 		When a pure single line comment is given
// 			It should be accepted
// 		When multiple comment block is given
// 			It should be accepted
// 		When a pure single line comment starting with a single letter is given
// 			It should be accepted
// 		When initial comments are given
// 			It should be accepted
// 		When final comments are given
// 			It should be accepted
// 		When comments before elements are given
// 			It should be accepted
// 		When comments before attributes are given
// 			It should be accepted
// 		When comments before model definitions are given
// 			It should be accepted
// 		When comments before queries are given
// 			It should be accepted
// 		When comments before tag end are given
// 			It should not be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserComments(t *testing.T) {

	Convey("Given a comment in selection expression for the processor", t, func() {

		Convey("When a pure single line comment is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`This is a single, one line comment`), ShouldBeNil)
			})

		})

		Convey("When multiple comment block is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
This is comment spanning
multiple lines.

This is a second comment block.
`), ShouldBeNil)
			})

		})

		Convey("When a pure single line comment starting with a single letter is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`I am a single comment`), ShouldBeNil)
			})

		})

		Convey("When initial comments are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
This is an initial comment surrounded by blank lines.
 
	result {
	}
`), ShouldBeNil)
			})

		})

		Convey("When initial comment without empty lines at start of file is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`This is an initial comment followed by blank lines.
 
	result {
	}
`), ShouldBeNil)
			})

		})

		Convey("When multiple initial comments without empty lines at start of file are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`This is an initial comment followed by a second comment.

I'm comment two.
 
	result {
	}
`), ShouldBeNil)
			})

		})

		Convey("When final comments are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
	result {
	}
This is an final comment.
`), ShouldBeNil)
			})

		})

		Convey("When comments before elements are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
comments before elements without blank lines
		tt {}
	}
`), ShouldBeNil)
			})

		})

		Convey("When comments before attributes are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
	result {
comments before elements without blank lines
		@a {1}
	}
`), ShouldBeNil)
			})

		})

		Convey("When comments before model definitions are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
comments before model definitions without blank lines
	model Xyz=http://www.pp.de/NS/22/hs
`), ShouldBeNil)
			})

		})

		Convey("When comments before queries are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
	result {
comments before queries without blank lines
		[]
	}
`), ShouldBeNil)
			})

		})

		Convey("When comments before tag end are given", func() {

			Convey("It should not be accepted", func() {
				So(ParseDats(`
	result {
comments before queries without blank lines
	}
`), ShouldNotBeNil)
			})

		})

	})

}
