// TestParserQuerySort
// 	Given local queries including orderby in a selection expression for the processor
// 		When a range of values descending is given
// 			It should be accepted
// 	Given local queries including distinct in a selection expression for the processor
// 		When only distinct values from values in outer selections are given
// 			It should be accepted
// 		When only distinct values from values in inner selections are given
// 			It should be accepted
// 		When only distinct values from element sequences are given
// 			It should be accepted
// 		When a function result from a query is given
// 			It should be accepted
// 		When iterating using numbers
// 			It should not be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserQuerySort(t *testing.T) {

	Convey("Given local queries including orderby in a selection expression for the processor", t, func() {

		Convey("When a range of values descending is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in P23R:range(1,3)
							orderby x descending
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

	})

	Convey("Given local queries including distinct in a selection expression for the processor", t, func() {

		Convey("When only distinct values from values in outer selections are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					result {
						[
							for x in P23R:range(1,3)
							distinct
							for y in P23R:range(1,2)
							return x + y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When only distinct values from values in inner selections are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					result {
						[
							for x in P23R:range(1,3)
							for y in P23R:range(1,2)
							distinct
							return x + y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When only distinct values from element sequences are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.in.*
							distinct
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a function result from a query is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in range(3,3)
							return range(1,2)
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When iterating using numbers", func() {

			Convey("It should not be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in 1
							return x
						]
					}
				`), ShouldNotBeNil)
			})

		})

	})

}
