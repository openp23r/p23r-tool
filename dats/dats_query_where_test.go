// TestParserQueryWhere
// 	Given local queries including where in a selection expression for the processor
// 		When a string value is compared
// 			It should be accepted
// 		When an or condition operator is given
// 			It should be accepted
// 		When a not operator is given
// 			It should be accepted
// 		When a regex operator is given
// 			It should be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserQueryWhere(t *testing.T) {

	Convey("Given local queries including where in a selection expression for the processor", t, func() {

		Convey("When a string value is compared", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							where x@ == "xyz"
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When an or condition operator is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							where x@ == "xyz" || x@ == "uvw"
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a not operator is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							where ! x@ == "xyz"
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a regex operator is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							where x@ ~ /[qx]/
							return x
						]
					}
				`), ShouldBeNil)
			})

		})

	})

}
