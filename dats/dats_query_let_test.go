// TestParserQueryLet
// 	Given local queries including let in a selection expression for the processor
// 		When a single let is given
// 			It should be accepted
// 		When a single let with string concatination is given
// 			It should be accepted
// 		When a single let with number addition is given
// 			It should be accepted
// 		When a single let with string remove is given
// 			It should be accepted
// 		When a single let with number subtraction is given
// 			It should be accepted
// 		When a single let with string multiplication (1) is given
// 			It should be accepted
// 		When a single let with string multiplication (2) is given
// 			It should be accepted
//
// 		When a single let with number multiplication (3) is given
// 			It should be accepted
// 		When a single let with number division is given
// 			It should be accepted
// 		When a single let with enclosed expression is given
// 			It should be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserQueryLet(t *testing.T) {

	Convey("Given local queries including let in a selection expression for the processor", t, func() {

		Convey("When a single let is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							let y = x@
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with string concatination is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							let y = x@ + x@
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with number addition is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = x@ + x@
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with string remove is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							let y = x@ - "rst"
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with number subtraction is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = x@ - 1
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with string multiplication (1) is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.d
							let y = 3 * x@
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with string multiplication (2) is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[(
							for x in $.a.d
							let y = x@ * 3
							return y
						).Join()]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with number multiplication (3) is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = 3 * x@
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with number division is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = x@ / 2
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

		Convey("When a single let with enclosed expression is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
				 
					source = http://www.pp.de/NS/rule0817/source
					result {
						[
							for x in $.a.e
							let y = (x@ * 2) + 3
							return y
						]
					}
				`), ShouldBeNil)
			})

		})

	})

}
