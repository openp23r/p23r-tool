package dats

import (
	"fmt"
	"io/ioutil"
	"strconv"
)

// catchError catches errors triggered by panic and
// returns back the error message as usual
func catchError(g func()) (err error) {
	defer func() {
		if x := recover(); x != nil {
			err = fmt.Errorf("%v", x)
		}
	}()
	g()
	return nil
}

// ParseDatsFile reads a file and check its syntax and semantic against the
// P23R Selection grammer
func ParseDatsFile(filename string) error {
	expression, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	return ParseDats(string(expression))
}

// ParseDats get a string and check its syntax and semantic against the
// P23R Selection grammer
func ParseDats(expression string) error {
	dats := &Dats{Buffer: expression}
	dats.Init()

	if err := dats.Parse(); err != nil {
		return err
	}

	if err := catchError(dats.Execute); err != nil {
		return err
	}

	return nil
}

// TokenInfo is
type TokenInfo struct {
	Rule  string
	Text  string
	Depth uint32
}

// TokenDetails will
func TokenDetails(p *Dats) <-chan TokenInfo {
	s := make(chan TokenInfo, 16)
	tokens := p.tokenTree.Tokens()
	go func() {
		for token := range tokens {
			x := token.getToken32()
			s <- TokenInfo{rul3s[x.pegRule], strconv.Quote(string(([]rune(p.buffer)[token.begin:token.end]))), token.next}
		}
		close(s)
	}()
	return s
}
