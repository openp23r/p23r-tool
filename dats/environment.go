package dats

import "fmt"

// EnvironmentVars stores all variable names for the actual context
type EnvironmentVars struct {
	stack []string
}

// EnvironmentModels stores all model names for the actual context
type EnvironmentModels struct {
	stack []string
}

// Environment is used to remember the whole context for semantic parsing
type Environment struct {
	Vars   []EnvironmentVars
	Models []EnvironmentModels
}

// Push adds new context to stack
func (env *Environment) Push(vars EnvironmentVars, models EnvironmentModels) {
	env.Vars = append(env.Vars, vars)
	env.Models = append(env.Models, models)
}

// Pop removes top context from stack and returns the heads of variables and models
func (env *Environment) Pop() (vars EnvironmentVars, models EnvironmentModels) {
	vars = env.Vars[len(env.Vars)-1]
	models = env.Models[len(env.Models)-1]
	env.Vars = env.Vars[0 : len(env.Vars)-1]
	env.Models = env.Models[0 : len(env.Models)-1]
	return vars, models
}

// Push adds variable name to stack
func (vars *EnvironmentVars) Push(id string) {
	vars.stack = append(vars.stack, id)
}

// Check tests if a variable is in the stack
func (vars *EnvironmentVars) Check(id string) {
	for _, x := range vars.stack {
		if x == id {
			return
		}
	}
	panic("SEMANTIC ERROR: Variable '" + id + "' is not defined in context")
}

// Length of the stack with variable names
func (vars *EnvironmentVars) Length() int {
	return len(vars.stack)
}

// Print all variable names
func (vars *EnvironmentVars) Print() {
	for _, x := range vars.stack {
		fmt.Println(x)
	}
}

// Push adds model name to stack
func (models *EnvironmentModels) Push(id string) {
	models.stack = append(models.stack, id)
}

// Check tests if a model name is in the stack
func (models *EnvironmentModels) Check(id string) {
	for _, x := range models.stack {
		if x == id {
			return
		}
	}
	panic("SEMANTIC ERROR: Model '" + id + "' is not defined in context")
}

// Length is
func (models *EnvironmentModels) Length() int {
	return len(models.stack)
}

// Print all model names
func (models *EnvironmentModels) Print() {
	for _, x := range models.stack {
		fmt.Println(x)
	}
}
