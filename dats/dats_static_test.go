// TestParserStatic
// 	Given a selection expression evaluating static content for the processor
// 		When a single element is given
// 			It should be accepted
// 		When a simply nested element is given
// 			It should be accepted
// 		When a single element with text content is given
// 			It should be accepted
// 		When a resulting element value from string expressions is given
// 			It should be accepted
// 		When an empty attribute is given
// 			It should not be accepted
// 		When a single element and an integer attribute are given
// 			It should be accepted
// 		When a single element and a floating attribute are given
// 			It should be accepted
// 		When a single element and a time attribute are given
// 			It should be accepted
// 		When a single element and a datetime attribute are given
// 			It should be accepted
// 		When a single element and a date attribute are given
// 			It should be accepted
// 		When a single element and a string attribute are given
// 			It should be accepted
// 		When a single element and multiple attribute values are given
// 			It should be accepted
// 		When a single element and multiple attributes are given
// 			It should be accepted
// 		When nested elements are given
// 			It should be accepted
// 		When nested elements and attributes are given
// 			It should be accepted
// 		When deeper nested elements are given
// 			It should be accepted
// 		When an element with multiple static values are given
// 			It should be accepted
// 		When an element with multiple static values even in a line are given
// 			It should be accepted
// 		When resulting values from standard function calls are given
// 			It should be accepted
// 		When resulting attribute values from string expressions are given
// 			It should be accepted
// 		When resulting values from mixed expressions are given
// 			It should be accepted

package dats

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParserStatic(t *testing.T) {

	Convey("Given a selection expression evaluating static content for the processor", t, func() {

		Convey("When a single element is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
	result {
	}
`), ShouldBeNil)
			})

		})

		Convey("When a simply nested element is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		a {}
	}
`), ShouldBeNil)
			})

		})

		Convey("When a single element with text content is given", func() {
			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result { "Hello World" }
`), ShouldBeNil)
			})

		})

		Convey("When a resulting element value from string expressions is given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		a { "abc" + "def" }
	}
`), ShouldBeNil)
			})

		})

		Convey("When an empty attribute is given", func() {

			Convey("It should not be accepted", func() {
				So(ParseDats(`
 
		result {
			@a {  }
		}
`), ShouldNotBeNil)
			})

		})

		Convey("When a single element and an integer attribute are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@count { 16 }
	}
`), ShouldBeNil)
			})

		})

		Convey("When a single element and a floating attribute are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@count { 16.5e5 }
	}
`), ShouldBeNil)
			})

		})

		Convey("When a single element and a time attribute are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@count { 16:03 }
	}
`), ShouldBeNil)
			})

		})

		Convey("When a single element and a datetime attribute are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@count { 2014-01-31t16:03:45 }
	}
`), ShouldBeNil)
			})

		})

		Convey("When a single element and a date attribute are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@count { 2014-08-14 }
	}
`), ShouldBeNil)
			})

		})

		Convey("When a single element and a string attribute are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@xmlns {"http://www.website.de/NS/schema.xsd"}
	}
`), ShouldBeNil)
			})

		})

		Convey("When a single element and multiple attribute values are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@mixed { 2014-08-14 12 "Max Mustermann" }
	}
`), ShouldBeNil)
			})

		})

		Convey("When a single element and multiple attributes are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@a1 { 2014-08-14 12 }
		@a2 { "Max Mustermann" }
	}
`), ShouldBeNil)
			})

		})

		Convey("When nested elements are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	outer {
		inner {
			"Max Mustermann"
		}
	}
`), ShouldBeNil)
			})

		})

		Convey("When nested elements and attributes are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	outer {
		inner {
			@attr {
				2014-08-14
				12
				"Max Mustermann"
			}
		}
	}
`), ShouldBeNil)
			})

		})

		Convey("When deeper nested elements are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	outer {
		inner {
			t {
				@attr {
					2014-08-14
					12
					"Max Mustermann"
				}
			}
		}
	}
`), ShouldBeNil)
			})

		})

		Convey("When an element with multiple static values are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	elem {
		2014-08-14
		12
		"Max Mustermann"
	}
`), ShouldBeNil)
			})

		})

		Convey("When an element with multiple static values even in a line are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@xmlns {"http://www.website.de/NS/schema.xsd"}
		1 2.1 (-456) 1.23e4 1.23e-4
	}
`), ShouldBeNil)
			})

		})

		Convey("When resulting values from standard function calls are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`

	result {
	@xmlns {"http://www.website.de/NS/schema.xsd"}
	P23R:protocolWrite(1,2,3)
	}
`), ShouldBeNil)
			})

		})

		Convey("When resulting attribute values from string expressions are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
	@a { "abc" + "def" }
	}
`), ShouldBeNil)
			})

		})

		Convey("When resulting values from mixed expressions are given", func() {

			Convey("It should be accepted", func() {
				So(ParseDats(`
 
	result {
		@a { "abc" + 6 }
	}
`), ShouldBeNil)
			})

		})

	})

}
