// Package xsd provides a function to validate XML documents against a XSD schema
package xsd

import (
	"io/ioutil"
	"os"
	"unsafe"

	"github.com/jbussdieker/golibxml"
	"github.com/krolaw/xsd"
	"gitlab.com/openp23r/p23r-common/log"
)

// SchemaDependence describes additional schema files required for validation
type SchemaDependence struct {
	Filename string
	Schema   string
}

// Validate parse the given XML document and validate it with the given XSD schema.
func Validate(xmlDoc, xsdSchema string, dependencies []*SchemaDependence) error {

	// Create temporarly local schemata on which the given schema depends
	for _, x := range dependencies {
		if _, err := os.Stat(x.Filename); err == nil {
			log.Error.Println("could not validate as file", x.Filename, "still exist")
			return err
		}
		if err := ioutil.WriteFile(x.Filename, []byte(x.Schema), 0600); err != nil {
			log.Error.Println("could not create local copy of schema", x.Filename)
		}
		defer func(x *SchemaDependence) {
			if err := os.Remove(x.Filename); err != nil {
				log.Error.Println("could not remove local copy of schema", x.Filename)
			}
		}(x)
	}

	// Create schema based parser
	schema, err := xsd.ParseSchema([]byte(xsdSchema))
	if err != nil {
		return err
	}

	// Parse and validate XML document
	//
	// TODO: Catch error messages
	doc := golibxml.ParseDoc(xmlDoc)
	if doc == nil {
		return err
	}
	defer doc.Free()

	// golibxml._Ctype_xmlDocPtr can't be cast to xsd.DocPtr, even though they are both
	// essentially _Ctype_xmlDocPtr. Using unsafe gets around this.
	//
	// TODO: Catch error messages
	if err := schema.Validate(xsd.DocPtr(unsafe.Pointer(doc.Ptr))); err != nil {
		return err
	}
	return nil
}

// ValidateFile validates the XML content of the file against the given schema.
func ValidateFile(filename, xsdSchema string, dependencies []*SchemaDependence) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	return Validate(string(data), xsdSchema, dependencies)
}

// ValidateSchemaFile check if the content of the file is valid schema
func ValidateSchemaFile(filename string) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	_, err = xsd.ParseSchema([]byte(data))
	return err
}
