package manifest

// TestCaseManifestSchema can be used to validate the manifest of a
// test case. Additionally, the schema CommonNrl1-1 must be locally
// available.
const TestSetManifestSchema = `<?xml version="1.0" encoding="utf-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1" xmlns:tsm="http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1" xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" elementFormDefault="qualified" version="1.1" >
<xs:annotation><xs:documentation>
### Specification of the Test Set Manifest,#### _p23r.nrl.TestSetManifest_,This specification describes the syntax of the test set manifest, which is the core of a test set included in a test package. This specification is normative for P23R solutions.,##### Notes,_none_,##### Specification of the model,The test set manifest is part of the core specifications for the P23R and belongs to the technical notification rule language (T-BRS). Therefore it is maintained by the p23r core team.
</xs:documentation></xs:annotation>

  <xs:import namespace="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" schemaLocation="CommonNrl1-1.xsd" />

  <xs:element name="testSetManifest" type="tsm:TestSetManifest" />

  <xs:complexType name="TestSetManifest"  >
<xs:annotation><xs:documentation>
The manifest describes the overall meta information required in the publishing process
of a test set, including the general informations, responsibilities and descriptions.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="release" type="cc:Release" />
      <xs:element name="subjects" minOccurs="0" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="creators" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="auditors" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="auditBys" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="descriptions" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="licenses" minOccurs="0" maxOccurs="unbounded" type="cc:Description" />
    </xs:sequence>
    <xs:attribute name="id" use="required" >
<xs:annotation><xs:documentation>
This is the unique id of the test set using an UUID. This id has to be updated
if a new release of the test set is published.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:pattern value="[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="name" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the unique name of the test set following the P23R NDRs. The test set's name
should never be updated even if a new release is published.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="title" type="xs:string" >
<xs:annotation><xs:documentation>
The _title_ is a short description of the intention of the test set.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="releasedAt" type="xs:dateTime" >
<xs:annotation><xs:documentation>
This is the date and time when the current test set was approved.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="context" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
The _context_ describes shortly the subject or given scenario to be tested. This text will be shown
in the test protocol as what will be tested in the sense of the behaviour driven test principles.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

</xs:schema>
`
