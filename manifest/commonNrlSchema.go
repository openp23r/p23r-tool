package manifest

// CommonNrl1_0Schema will be included by other schemata
const Common1_0Schema = `<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	targetNamespace="http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0"
	xmlns="http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0"
	elementFormDefault="qualified"
	version = "1.0"
>
	<xsd:element name="common" type= "Common" />
	<xsd:complexType name="Common">
	</xsd:complexType>
	<xsd:complexType name="PackageDependencies">
		<xsd:annotation>
			 <xsd:documentation xml:lang="en"><![CDATA[

			 	 This part describes which other packages and
			 	 P23R specifications this package depends on.


			 ]]></xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="rulePackages"
					minOccurs="0"
					maxOccurs="unbounded"
					type="PackageSet"
				>
				<xsd:annotation>
					 <xsd:documentation xml:lang="en"><![CDATA[

					 	 These are all notification rule packages, on
					 	 which the actual one depends.


					 ]]></xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="modelPackages"
					minOccurs="0"
					maxOccurs="unbounded"
					type="PackageSet"
				>
				<xsd:annotation>
					 <xsd:documentation xml:lang="en"><![CDATA[

					 	 These are all data model packages, on which
					 	 the actual one depends.


					 ]]></xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="p23rSpecifications"
					maxOccurs="unbounded"
					type="Release"
				>
				<xsd:annotation>
					 <xsd:documentation xml:lang="en"><![CDATA[

					 	 The package requires a P23R implementation
					 	 compatible with the given P23R specifications.


					 ]]></xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="PackageSet">
		<xsd:annotation>
			 <xsd:documentation xml:lang="en"><![CDATA[

			 	 This part describes a package with all its relevant
			 	 releases.


			 ]]></xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="releases"
					maxOccurs="unbounded"
					type="Release"
				>
				<xsd:annotation>
					 <xsd:documentation xml:lang="en"><![CDATA[

					 	 This a list of releases for the packages,
					 	 which should be part of the set.


					 ]]></xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
		<xsd:attribute name="name" use="required" type="xsd:string">
			<xsd:annotation>
				 <xsd:documentation xml:lang="en"><![CDATA[

				 	 This is a name of the package.


				 ]]></xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:complexType>
	<xsd:simpleType name="Release">
		<xsd:annotation>
			 <xsd:documentation xml:lang="en"><![CDATA[

			 	 This defines a single release number. A release
			 	 number can be given by a combination of a major
			 	 and minor number or only by the major number,
			 	 e.g. "1.2" or "1".


			 ]]></xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:complexType name="File">
		<xsd:sequence>
			<xsd:element name="hash"
				type="xsd:base64Binary"
			>
				<xsd:annotation>
					 <xsd:documentation xml:lang="en"><![CDATA[

					 	 The checksum of the file using a SHA254 hash
					 	 code.


					 ]]></xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
		<xsd:attribute name="location" use="required" type="xsd:anyURI">
			<xsd:annotation>
				 <xsd:documentation xml:lang="en"><![CDATA[

				 	 This is the location of the file usually given
				 	 be a relative URI, e.g.
				 	 'file:///«package name«/selection.dats'.


				 ]]></xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:complexType>
	<xsd:complexType name="Parameter" mixed="true">
		<xsd:annotation>
			 <xsd:documentation xml:lang="en"><![CDATA[

			 	 Each parameter has a unique name (id) and a value.


			 ]]></xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="name" use="required" type="xsd:string">
		</xsd:attribute>
	</xsd:complexType>
	<xsd:complexType name="Description" mixed="true">
		<xsd:annotation>
			 <xsd:documentation xml:lang="en"><![CDATA[

			 	 There could be multiple descriptions to support
			 	 various languages. If no matching language is found
			 	 the english description should be used as default.


			 ]]></xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="lang" use="required" type="xsd:string">
			<xsd:annotation>
				 <xsd:documentation xml:lang="en"><![CDATA[

				 	 The language must be a valid ISO3066 language
				 	 code.


				 ]]></xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:complexType>
	<xsd:complexType name="Contact">
		<xsd:annotation>
			 <xsd:documentation xml:lang="en"><![CDATA[

			 	 This part describes all available contact
			 	 information.



			 ]]></xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="email" type="xsd:anyURI">
			<xsd:annotation>
				 <xsd:documentation xml:lang="en"><![CDATA[

				 	 This is an URI with an email address.


				 ]]></xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="postalAddress" type="xsd:string">
			<xsd:annotation>
				 <xsd:documentation xml:lang="en"><![CDATA[

				 	 This is the postal address of the person
				 	 or organisation to contact. The address should
				 	 be written in one line in free form.


				 ]]></xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="location" type="xsd:string">
			<xsd:annotation>
				 <xsd:documentation xml:lang="en"><![CDATA[

				 	 This is the address where the person or
				 	 organisation is physically located. The address
				 	 should be written in one line in free form.


				 ]]></xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="telephone" type="xsd:string">
			<xsd:annotation>
				 <xsd:documentation xml:lang="en"><![CDATA[

				 	 This is the telephone number of the person or
				 	 organisation to contact.


				 ]]></xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="website" type="xsd:anyURI">
			<xsd:annotation>
				 <xsd:documentation xml:lang="en"><![CDATA[

				 	 This is the website of the person or
				 	 organisation to contact. The page should be a
				 	 support or contact page.


				 ]]></xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:complexType>
</xsd:schema>`

// CommonNrl1_1Schema will be included by other schemata
const CommonNrl1_1Schema = `<?xml version="1.0" encoding="utf-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" elementFormDefault="qualified" version="1.1" >
<xs:annotation><xs:documentation>
### Specification of the common data types for P23R models,#### _p23r.nrl.CommonNrl_,This specification describes the syntax of the Common components, which are used in the notification rule language.,##### Notes,*_2011/08/01_ Dependencies splitted in data model and notification rule package.,##### Specification,These are the common components used in the notification rule language.
</xs:documentation></xs:annotation>

  <xs:complexType name="PackageDependencies"  >
<xs:annotation><xs:documentation>
This part describes which other packages and P23R specifications this package depends on.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="rulePackages" minOccurs="0" maxOccurs="unbounded" type="cc:PackageSet" />
      <xs:element name="modelPackages" minOccurs="0" maxOccurs="unbounded" type="cc:PackageSet" />
      <xs:element name="p23rSpecifications" maxOccurs="unbounded" type="cc:Release" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PackageSet"  >
<xs:annotation><xs:documentation>
This part describes a package with all its relevant releases.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="releases" maxOccurs="unbounded" type="cc:Release" />
    </xs:sequence>
    <xs:attribute name="name" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is a name of the package.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:annotation><xs:documentation>
This defines a single release number. A release number can be given by a combination of a major, minor and patch number or only by part of it, e.g. "1.2.3", "1.2" or "1". Further you can add a label using a hyphen followed by alphas, e.g. "1.2.3-alpha". The release number follows generally the rules of Semantic Versioning as specified on http://semver.org .
</xs:documentation></xs:annotation>
<xs:simpleType name="Release" >
    <xs:restriction base="xs:string">
      <xs:pattern value="(\d+)(\.\d+)?(\.\d+)?(-[a-zA-Z]+)?" />
    </xs:restriction>
  </xs:simpleType>

  <xs:annotation><xs:documentation>
An UUID for unique identification.
</xs:documentation></xs:annotation>
<xs:simpleType name="UUID" >
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}" />
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="File"  >

    <xs:sequence>
      <xs:element name="hash" type="xs:base64Binary" />
    </xs:sequence>
    <xs:attribute name="location" use="required" type="xs:anyURI" >
<xs:annotation><xs:documentation>
This is the location of a file usually given be a local, relative URI,
e.g. '«package name«/selection.dats', which points to an internal file, or a
external URI prefixed by a protocol, e.g. 'https://«control centre»/«package name».zip',
which points to an external file.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:complexType name="Parameter" mixed="true"  >
<xs:annotation><xs:documentation>
Each parameter has a unique name (id) and a value.
</xs:documentation></xs:annotation>

    <xs:attribute name="name" use="required" type="xs:string" >

</xs:attribute>  </xs:complexType>

  <xs:complexType name="Description" mixed="true"  >
<xs:annotation><xs:documentation>
There could be multiple descriptions to support various languages. If no matching language is found
the english description should be used as default.
</xs:documentation></xs:annotation>

    <xs:attribute name="lang" use="required" >
<xs:annotation><xs:documentation>
The language must be a valid ISO 369-1 language code.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:pattern value="[a-z]{2}(-[A-Z]{2})?" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>

  <xs:complexType name="Contact"  >
<xs:annotation><xs:documentation>
This part describes all available contact information.
</xs:documentation></xs:annotation>

    <xs:attribute name="email" >
<xs:annotation><xs:documentation>
This is an URI with an email address.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:anyURI">
          <xs:pattern value="mailto:.*" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="postalAddress" type="xs:string" >
<xs:annotation><xs:documentation>
This is the postal address of the person or organisation to contact. The address should
be written in one line in free form.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="location" type="xs:string" >
<xs:annotation><xs:documentation>
This is the address where the person or organisation is physically located. The address
should be written in one line in free form.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="telephone" type="xs:string" >
<xs:annotation><xs:documentation>
This is the telephone number of the person or organisation to contact.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="website" type="xs:anyURI" >
<xs:annotation><xs:documentation>
This is the website of the person or organisation to contact. The page should be a
support or contact page.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:complexType name="SourceLanguage"  >
<xs:annotation><xs:documentation>
This is the relationship between a type of a source file and its compiler. The compiler
could be used to prove is a source file matches the files or part of it, which belong
to the generated files as decribed by the T-BRS.
</xs:documentation></xs:annotation>

    <xs:attribute name="name" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
The name is the suffix of the source file and specifies its language respectively type.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="compiler" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the name of the tool to translate the source code into its technical artefacts.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:complexType name="Policy"  >
<xs:annotation><xs:documentation>
Apolicy typically defines a compliance rule, which is implemented or not. A real
example is the policy "the recommendations could return the value 'mandatory' under some circumstances".
</xs:documentation></xs:annotation>

    <xs:attribute name="label" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the predefined, unique label describing the compliance rule or policy.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="isImplemented" use="required" type="xs:boolean" >
<xs:annotation><xs:documentation>
This boolean value states if for example a rule group or parts of it implements the policy.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

</xs:schema>`
