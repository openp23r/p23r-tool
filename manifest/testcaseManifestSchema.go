package manifest

// TestCaseManifestSchema can be used to validate the manifest of a
// test case. Additionally, the schema CommonNrl1-1 must be locally
// available.
const TestCaseManifestSchema = `<?xml version="1.0" encoding="utf-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://leitstelle.p23r.de/NS/p23r/nrl/TestCaseManifest1-0" xmlns:tcm="http://leitstelle.p23r.de/NS/p23r/nrl/TestCaseManifest1-0" xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0" elementFormDefault="qualified" version="1.0" >
<xs:annotation><xs:documentation>
### Specification of the Test Case Manifest,#### _p23r.nrl.TestCaseManifest_,This specification describes the syntax of the test case manifest, which is the core of a test case included in a test set. This specification is normative for P23R solutions.,##### Notes,_none_,##### Specification,The test case manifest is part of the core specifications for the P23R and belongs to the technical notification rule language (T-BRS). Therefore it is maintained by the p23r core team.
</xs:documentation></xs:annotation>

  <xs:import namespace="http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0" schemaLocation="Common1-0.xsd" />

  <xs:element name="testCaseManifest" type="tcm:TestCaseManifest" />

  <xs:complexType name="TestCaseManifest"  >
<xs:annotation><xs:documentation>
The manifest describes the overall meta information required in the publishing process
of a test case, including the general informations, responsibilities and descriptions.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="release" type="cc:Release" />
      <xs:element name="subjects" minOccurs="0" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="creators" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="behaviours" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="descriptions" maxOccurs="unbounded" type="cc:Description" />
    </xs:sequence>
    <xs:attribute name="id" use="required" >
<xs:annotation><xs:documentation>
This is the unique id of the test case using an UUID. This id has to be updated
if a new release of the test case is published.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:pattern value="[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="name" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the unique name of the test case following the P23R NDRs. The test case's name
should never be updated even if a new release is published.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="title" type="xs:string" >
<xs:annotation><xs:documentation>
The _title_ is a short description of the intention of the test case.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="releasedAt" use="required" type="xs:dateTime" >
<xs:annotation><xs:documentation>
This is the date and time when the current test set was created or last updated.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="ruleName" type="xs:string" >
<xs:annotation><xs:documentation>
The _ruleName_ is the unique name of the notification rule, which will be tested by the test case.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="ruleGroupName" type="xs:string" >
<xs:annotation><xs:documentation>
The _ruleGroupName_ is the unique name of the notification rule group, which will be tested by the test case.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="expect" default="matchable" >
<xs:annotation><xs:documentation>
The _expect_ describes if all resulting notifications match the expected notification set. It is also
possible to expect that an error is thrown and the generation of a notification will be aborted.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="matchable"/>
          <xs:enumeration value="unmatchable"/>
          <xs:enumeration value="error"/>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>

</xs:schema>
`
