package manifest

// RuleManifestSchema can be used to validate the manifest of a
// rule . Additionally, the schema CommonNrl1-1 must be locally
// available.
const RuleManifestSchema = `<?xml version="1.0" encoding="utf-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://leitstelle.p23r.de/NS/p23r/nrl/RuleManifest1-1" xmlns:rm="http://leitstelle.p23r.de/NS/p23r/nrl/RuleManifest1-1" xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" elementFormDefault="qualified" version="1.1" >
<xs:annotation><xs:documentation>
### Specification of the Rule Manifest,#### _p23r.nrl.RuleManifest_,This specification describes the syntax of the the content of the file '_RuleName_/Manifest.xml'.,##### Notes,30.07.11	name eingefuegt:,*activateAt und deactivateAt in validFrom und
* validUntil umbenannt (Gueltigkeitsbereich)
* createBefore in createDaysBefore umbenannt.
* Reihenfolge einiger Elemente vereinheitlicht.,##### Specification,This model describes the format of a manifest with the
meta information for a notification rule.
</xs:documentation></xs:annotation>

  <xs:import namespace="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" schemaLocation="CommonNrl1-1.xsd" />

  <xs:element name="ruleManifest" type="rm:RuleManifest" />

  <xs:complexType name="RuleManifest"  >
<xs:annotation><xs:documentation>
The manifest describes the meta information of
a notification rule.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="release" type="cc:Release" />
      <xs:element name="signer" minOccurs="0" type="rm:RequirementsOnApprove" />
      <xs:element name="subjects" minOccurs="0" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="submitAtDailyPeriods" minOccurs="0" maxOccurs="unbounded" type="xs:time" />
      <xs:element name="submitAtWeeklyPeriods" minOccurs="0" maxOccurs="unbounded" >
<xs:annotation><xs:documentation>
This element can be used to schedule a rule,
which has to be generated each week. The given
number uses ISO weekday numbers {1|2|3|4|5|6|7}
to specify the weekday from Monday to Sunday.
The submission has to be finished before
midnight.
</xs:documentation></xs:annotation>

        <xs:simpleType>
          <xs:restriction base="xs:int">
            <xs:minInclusive value="1" />
            <xs:maxInclusive value="7" />
          </xs:restriction>
        </xs:simpleType>
      </xs:element>
      <xs:element name="submitAtMonthlyPeriods" minOccurs="0" maxOccurs="unbounded" type="xs:gDay" />
      <xs:element name="submitAtYearlyPeriods" minOccurs="0" maxOccurs="unbounded" type="xs:gMonthDay" />
      <xs:element name="submitAts" minOccurs="0" maxOccurs="unbounded" type="xs:dateTime" />
      <xs:element name="creators" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="descriptions" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="targets" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="hints" minOccurs="0" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="legalBackgrounds" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="generation" type="rm:Generation" />
    </xs:sequence>
    <xs:attribute name="id" use="required" >
<xs:annotation><xs:documentation>
Unique id using an UUID. This id has to be
updated if a new release of the rule is
published.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:pattern value="[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="name" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
Unique name of the rule following the P23R
NDRs. The name should not change if a new
release is published.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="title" type="xs:string" >
<xs:annotation><xs:documentation>
The title is a short description of the
intention of the notification to be generated.
This title should de used as logging
information in the protocol while generating
the notification. The title could be
overwritten by the sender of the initial
message.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="releasedAt" type="xs:dateTime" >
<xs:annotation><xs:documentation>
The system date and time at which the notification rule was last updated.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="validFrom" use="required" type="xs:dateTime" >
<xs:annotation><xs:documentation>
The date from which on this notification rule
could be used for notifications generated for
that period. This notification rule could
replace an older release of this notification
rule. The validFrom date of this
notification rule and the validUntil date
from an older notification rule could
overlap.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="validUntil" type="xs:dateTime" >
<xs:annotation><xs:documentation>
The date from which on this notification rule
should be no longer used for notifications
to be generated at points of time after
this date.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="createDaysBefore" type="xs:duration" >
<xs:annotation><xs:documentation>
The notification should be generated the given
days before the notification has to be
submitted. This should give the approver enough
time to check the notification.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="criticalSubmitDelay" type="xs:duration" >
<xs:annotation><xs:documentation>
The notification must not be delayed by more
than the amount of hours given by the property
criticalSubmitDelay, taking the property
submitAt... as the official submission time.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:complexType name="RequirementsOnApprove"  >
<xs:annotation><xs:documentation>
This section describes the requirements for the person who
has permission to approve and sign the final
notification.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="hints" minOccurs="0" maxOccurs="unbounded" type="cc:Description" />
    </xs:sequence>
    <xs:attribute name="registeredAt" use="required" >
<xs:annotation><xs:documentation>
This value describes from which source the
approver's id and its permission can be
verified by the notification receiver.,This is an enumeration with the following
values:,'ebr.de': The signing person must be registered in the electronic business register of Germany
'ebr.eu': The signing person must be registered in the electronic business register of the EU
'registered': The signing person must be registered at the notification receiver's system.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="ebr.de"/>
          <xs:enumeration value="ebr.eu"/>
          <xs:enumeration value="registered"/>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="qualifiedSignatureRequired" use="required" type="xs:boolean" >
<xs:annotation><xs:documentation>
This section indicates to the P23R client if
the notification has be signed by a qualified
signature.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:complexType name="Generation"  >
<xs:annotation><xs:documentation>
This section describes how the generation of the
notification and the notification profile is
sequenced.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="steps" minOccurs="0" maxOccurs="unbounded" type="rm:Step" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="Step"  >
<xs:annotation><xs:documentation>
Each step is selected by the notification type of
the last resulting XML document and defines the
followup transformation scripts to generate the
next notification as well as notification profile.
</xs:documentation></xs:annotation>

    <xs:attribute name="name" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
The unique name of this step, e.g. for logging.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="title" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is a short description of this step, e.g. for logging.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="notificationType" type="xs:string" >
<xs:annotation><xs:documentation>
The namespace, which must match with the one
of the last resulting notification document.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="notificationTransformation" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the name of the XSLT script to be used
to generate the next (intermediate)
notification.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="profileTransformation" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the name of the XSLT script to be used
to generate the next (intermediate)
notification profile
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

</xs:schema>
`
