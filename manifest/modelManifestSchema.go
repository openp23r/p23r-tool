package manifest

// ModelManifestSchema can be used to validate the manifest of a
// model. Additionally, the schema CommonNrl1-1 must be locally
// available.
const ModelManifestSchema = `<?xml version="1.0" encoding="utf-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1" xmlns:mm="http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1" xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" elementFormDefault="qualified" version="1.1" >
<xs:annotation><xs:documentation>
### Specification of the Model Manifest,#### _p23r.nrl.ModelManifest_,This specification describes the content of the file '&lt;&lt;Model Name&gt;&gt;/&lt;&lt;release&gt;&gt;/Manifest.xml'.,##### Notes,*01.08.11 tag in subjects umbenannt.,*Reihenfolge vereinheitlicht.,##### Specification of the model,This model describes the format of a manifest with the,meta information for a data model.
</xs:documentation></xs:annotation>

  <xs:import namespace="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" schemaLocation="CommonNrl1-1.xsd" />

  <xs:element name="modelManifest" type="mm:ModelManifest" />

  <xs:complexType name="ModelManifest"  >
<xs:annotation><xs:documentation>
The manifest describes the meta information of,apartial data model belonging to thePivot model,of a P23R tenant instance.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="release" type="cc:Release" />
      <xs:element name="subjects" minOccurs="0" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="creators" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="descriptions" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="targets" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="licenses" minOccurs="0" maxOccurs="unbounded" type="cc:Description" />
    </xs:sequence>
    <xs:attribute name="id" use="required" >
<xs:annotation><xs:documentation>
Unique id using an UUID. This id has to be,updated if a new release of the notification,model is published.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:pattern value="[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="name" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the unique name of the partial data,model following the P23R NDRs. The name should,not change even if a new release is published.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="title" type="xs:string" >
<xs:annotation><xs:documentation>
The title is a short description of the,intention of the partial data model.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="releasedAt" use="required" type="xs:dateTime" >
<xs:annotation><xs:documentation>
The system date and time at which the model was last updated.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="namespace" use="required" type="xs:anyURI" >
<xs:annotation><xs:documentation>
This is the namespace of this data model.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="schema" use="required" type="xs:anyURI" >
<xs:annotation><xs:documentation>
This is the XML schema file given as a URI for this data,model. There could be more schema files in the,directory, which could be included or imported,by this one.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

</xs:schema>
`
