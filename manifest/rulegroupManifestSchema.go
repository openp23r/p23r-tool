package manifest

// RuleGroupManifestSchema can be used to validate the manifest of a
// rule group. Additionally, the schema CommonNrl1-1 must be locally
// available.
const RuleGroupManifestSchema = `<?xml version="1.0" encoding="utf-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1" xmlns:rgm="http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1" xmlns:cc="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" elementFormDefault="qualified" version="1.1" >
<xs:annotation><xs:documentation>
### Specification of the Rule Group Manifest,#### _p23r.nrl.RuleGroupManifest_,This specification describes the content of the file '&lt;&lt;Rule Group Name&gt;&gt;/&lt;&lt;release&gt;&gt;/Manifest.xml'.,##### Notes,30.07.11	name eingefügt
activateAt und deactivateAt in validFrom und
validUntil umbenannt (Gültigkeitsbereich)
Reihenfolge einiger Elemente vereinheitlicht.,##### Specification,This model describes the format of a manifest with the
meta information for a notification rule group.
</xs:documentation></xs:annotation>

  <xs:import namespace="http://leitstelle.p23r.de/NS/p23r/nrl/CommonNrl1-1" schemaLocation="CommonNrl1-1.xsd" />

  <xs:element name="ruleGroupManifest" type="rgm:RuleGroupManifest" />

  <xs:complexType name="RuleGroupManifest"  >
<xs:annotation><xs:documentation>
The manifest describes the meta information of
a notification rule group.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="release" type="cc:Release" />
      <xs:element name="sourceLanguages" minOccurs="0" maxOccurs="unbounded" type="cc:SourceLanguage" />
      <xs:element name="subjects" minOccurs="0" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="conflicts" minOccurs="0" type="rgm:Conflicts" />
      <xs:element name="recommendationNSs" minOccurs="0" maxOccurs="unbounded" type="xs:anyURI" />
      <xs:element name="configurationRequireNSs" minOccurs="0" maxOccurs="unbounded" type="xs:anyURI" />
      <xs:element name="creators" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="auditors" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="auditBys" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="descriptions" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="targets" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="hints" minOccurs="0" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="legalBackgrounds" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="licenses" minOccurs="0" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="variants" minOccurs="0" maxOccurs="unbounded" type="rgm:Variant" />
      <xs:element name="policies" minOccurs="0" maxOccurs="unbounded" type="cc:Policy" />
    </xs:sequence>
    <xs:attribute name="id" use="required" >
<xs:annotation><xs:documentation>
This is a unique id of the notification rule
group using an UUID. This id has to be updated
if a new release of the notification rule group
is published.
</xs:documentation></xs:annotation>

      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:pattern value="[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="name" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the unique name of the notification
rule group following the P23R NDRs. The name
should not updated even if a new release is
published.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="title" type="xs:string" >
<xs:annotation><xs:documentation>
The title is a short description of the
intention of the notification rule group.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="releasedAt" type="xs:dateTime" >
<xs:annotation><xs:documentation>
This is the date and time when the current rule group was officially approved in the development portal.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="validFrom" use="required" type="xs:dateTime" >
<xs:annotation><xs:documentation>
The date from which on this rule group could be
used for notifications generated for that
period. This notification rule group could
replace an older release of this notification
rule group. The validFrom date of this
notification rule group and the validUntil date
from an older notification rule group could
overlap.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="validUntil" type="xs:dateTime" >
<xs:annotation><xs:documentation>
The date from which on this notification rule
group should be no longer used for
notifications to be generated at points of time
after this date.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:complexType name="Variant"  >
<xs:annotation><xs:documentation>
This section specifies a variant for a rule group.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="descriptions" maxOccurs="unbounded" type="cc:Description" />
      <xs:element name="choices" maxOccurs="unbounded" type="rgm:Choice" />
    </xs:sequence>
    <xs:attribute name="id" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is an unique identifier of this rule
variant.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:complexType name="Choice"  >
<xs:annotation><xs:documentation>
This section describes a choice.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="rules" maxOccurs="unbounded" type="xs:string" />
      <xs:element name="descriptions" maxOccurs="unbounded" type="cc:Description" />
    </xs:sequence>
    <xs:attribute name="id" use="required" type="xs:string" >
<xs:annotation><xs:documentation>
This is the unique id for the choice of this
variant.
</xs:documentation></xs:annotation>

</xs:attribute>    <xs:attribute name="isDefault" use="required" type="xs:boolean" >
<xs:annotation><xs:documentation>
If this is the default choice it should be true
and shown to the domain adminstrator of the
P23R tenant instance as preselected.
</xs:documentation></xs:annotation>

</xs:attribute>  </xs:complexType>

  <xs:complexType name="Conflicts"  >
<xs:annotation><xs:documentation>
Specifies with which other id this will get in
conflict.
</xs:documentation></xs:annotation>

    <xs:sequence>
      <xs:element name="ids" maxOccurs="unbounded" type="xs:string" />
    </xs:sequence>
  </xs:complexType>

</xs:schema>
`
