package manifest

import (
	"io/ioutil"
	"path/filepath"

	"github.com/jteeuwen/go-pkg-xmlx"
	"gopkg.in/yaml.v2"

	"gitlab.com/openp23r/p23r-common/log"
	"gitlab.com/openp23r/p23r-common/markdown"
)

// the constants for all kind of identified manifest types
const (
	NoManifestID           = -1
	UnknownManifestID      = 0
	RuleManifestID         = 1
	RuleGroupManifestID    = 2
	ModelManifestID        = 3
	TestCaseManifestID     = 4
	TestSetManifestID      = 5
	RulePackageManifestID  = 6
	ModelPackageManifestID = 7
	TestPackageManifestID  = 8
)

type namespace struct {
	Xmlns string
}

// GetManifestType returns the kind of manifest
func GetManifestType(path string, verbose bool) int {
	data, err := ioutil.ReadFile(filepath.Join(path, "Manifest.xml"))
	if err != nil {
		if verbose {
			log.Info.Println("Could not read Manifest file", filepath.Join(path, "Manifest.xml"))
		}
		data, err := ioutil.ReadFile(filepath.Join(path, "sources", "Manifest.lityaml"))
		if err != nil {
			return NoManifestID
		}
		var ns namespace
		err = yaml.Unmarshal(markdown.Filter([]byte(data)), &ns)
		if err != nil {
			log.Info.Println("Could not parse Manifest file", filepath.Join(path, "sources", "Manifest.lityaml", err.Error()))
			return UnknownManifestID
		}
		switch ns.Xmlns {
		case "http://leitstelle.p23r.de/NS/p23r/nrl/RuleManifest1-1":
			return RuleManifestID
		case "http://leitstelle.p23r.de/NS/p23r/nrl/RuleGroupManifest1-1":
			return RuleGroupManifestID
		case "http://leitstelle.p23r.de/NS/p23r/nrl/ModelManifest1-1":
			return ModelManifestID
		case "http://leitstelle.p23r.de/NS/p23r/nrl/TestCaseManifest1-0":
			return TestCaseManifestID
		case "http://leitstelle.p23r.de/NS/p23r/nrl/TestSetManifest1-1":
			return TestSetManifestID
		case "http://leitstelle.p23r.de/NS/p23r/nrl/TestPackageManifest1-0":
			return TestPackageManifestID
		case "http://leitstelle.p23r.de/NS/p23r/nrl/RulePackageManifest1-1":
			return RulePackageManifestID
		case "http://leitstelle.p23r.de/NS/p23r/nrl/ModelPackageManifest1-0":
			return ModelPackageManifestID
		}
		return UnknownManifestID
	}
	doc := xmlx.New()
	err = doc.LoadString(string(data), nil)
	if err != nil {
		if verbose {
			log.Info.Println("Could not read Manifest file", filepath.Join(path, "Manifest.xml"))
		}
		return UnknownManifestID
	}
	node := doc.SelectNode("*", "ruleManifest")
	if node != nil {
		return RuleManifestID
	}
	node = doc.SelectNode("*", "ruleGroupManifest")
	if node != nil {
		return RuleGroupManifestID
	}
	node = doc.SelectNode("*", "modelManifest")
	if node != nil {
		return ModelManifestID
	}
	node = doc.SelectNode("*", "testCaseManifest")
	if node != nil {
		return TestCaseManifestID
	}
	node = doc.SelectNode("*", "testSetManifest")
	if node != nil {
		return TestSetManifestID
	}
	node = doc.SelectNode("*", "testPackageManifest")
	if node != nil {
		return TestPackageManifestID
	}
	node = doc.SelectNode("*", "rulePackageManifest")
	if node != nil {
		return RulePackageManifestID
	}
	node = doc.SelectNode("*", "modelPackageManifest")
	if node != nil {
		return ModelPackageManifestID
	}

	return UnknownManifestID
}
